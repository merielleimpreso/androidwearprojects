package com.aeustech.inactivity;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

/**
 * Created by aeustech on 3/3/15.
 */
public  class InactivityWearReceiver extends BroadcastReceiver implements SensorEventListener, MessageApi.MessageListener
{

    static Context xContext;
    static SensorEventListener xListener;
    static double threshold = 5;
    static double accelerometerPreviousValue = 0;

    static InactivityWearActivity xActivity;
    Random randomizer;
    static SharedPreferences prefs;
    static SharedPreferences.Editor prefsEditor;
    static Sensor accelerometer;
    static SensorManager sensorManager;
    long timeStarted;//First detection of greather than threshold. Stored in TimeStarted prefs.
    long timePrevious;//succeeding detections greater than threshold
    long intervalThreshold = 10000;//(millis) time in between detections before timeStarted is reset
    long totalTimeThreshold = 30000;//in millis, threshold time difference timeStarted and last timePrevious. If greater than this,
                           //alarm is cancelled
    long[] totalTimeThresholds = new long[]{60000,30000, 40000, 90000};//Divide by 1k = total time in minutes
    static boolean userHasMoved;
    static boolean userActiveEnough;
    double percentage;
    long totalTimeDifference =0;

    private GoogleApiClient mGoogleApiClient;
    private static String TAG = "Inactivity_Wear";

    @Override
    public void onReceive(Context context, Intent intent)
    {

        String action = intent.getStringExtra("Extra");
        int temp;

        prefs = context.getSharedPreferences("com.aeustech.inactivity", Context.MODE_PRIVATE);
        prefsEditor = prefs.edit();
        totalTimeThreshold = totalTimeThresholds[prefs.getInt("MessagePackageIndex",0)];

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        String prevDate = prefs.getString("PreviousDate", formattedDate);


        if(!formattedDate.equals(prevDate))
        {
            resetScores();
            prefsEditor.putString("PreviousDate", formattedDate);
        }


        Log.d("Reciever", "Recieved something" +action);
        if(action.equals("Accept"))
        {

                temp = prefs.getInt("ChallengesAccepted", 0);
                temp++;
                prefsEditor.putInt("ChallengesAccepted", temp);

                int storedID = prefs.getInt("MessagePackageIndex", 0);
                String toSay = "";
                switch(storedID)
                {
                    case 0:
                        toSay = "You have fed Pudgy "+ temp+" times";
                        break;
                    case 1:
                        toSay = "You appeased Chip "+ temp+" times";
                        break;
                    case 2:
                        toSay = "You heeded Doctor Doki "+ temp+" times";
                        break;
                    case 3:
                        toSay = "You followed Master Maru's teachings "+ temp+" times";
                        break;
                    case 4:
                        toSay = "You rewarded "+ temp+" times";
                        break;
                }
                Toast.makeText(xContext, toSay, Toast.LENGTH_SHORT).show();
                //Do accept stuff here
                prefsEditor.commit();
        }

       else if(action.equals("Reset"))
        {
                //Reset ChallengesThrown and ChallengesAccepted
        }


        else //if(action.equals("Alarm"))
        {
                if(!userHasMoved || !userActiveEnough)
                {
                    Log.d("Notif","Alarm Called " );
                    sendNotif("New Message!", getRandomMessage(context));
                    temp = prefs.getInt("ChallengesThrown", 0);
                    temp++;
                    prefsEditor.putInt("ChallengesThrown", temp);
                    Toast.makeText(xContext, "ChallengesThrown: "+temp, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    //When alarm is cancelled because user is physically active, assume challenge accepted.
                    Log.d("Notif", "Alarm Cancelled");
                    temp = prefs.getInt("ChallengesAccepted", 0);
                    temp++;
                    prefsEditor.putInt("ChallengesAccepted", temp);
                    Toast.makeText(xContext, "You Accepted", Toast.LENGTH_SHORT).show();
                }

                prefsEditor.commit();
                userHasMoved = false;
                totalTimeDifference+=0;

        }



    }

    @Override
    public final void onAccuracyChanged(Sensor triggeringSensor, int value)
    {

    }

    @Override
    public final void onSensorChanged(SensorEvent sensorEvent)
    {


        if(prefs != null && prefs.getBoolean("IsActive", true))
        {
            double x = sensorEvent.values[0];
            double y = sensorEvent.values[1];
            double z = sensorEvent.values[2];
            double value = Math.sqrt(x+y+z);


            if(value> accelerometerPreviousValue)
            {
                accelerometerPreviousValue = value;

            }

            //if(!userHasMoved)
            //Log.d("Sensor", "Changed. Magnitude = "+value);

            if(value > threshold)
            {
                //Log.d("Sensor", "Changed. Magnitude = "+value);
                if(!userHasMoved)//First motion detection
                {
                    Log.d("Sensor", "First detection.");
                    timeStarted = System.currentTimeMillis();
                    timePrevious = timeStarted;
                    userHasMoved = true;

                }
                else//Succeedingi motion detections
                {
                    long tempTime = System.currentTimeMillis();
                    long timeDifference = tempTime - timePrevious;

                    if(timeDifference <= intervalThreshold)//If new detection is within 10 seconds of previous
                    {

                        timeDifference = tempTime - timeStarted;

                        Log.d("Sensor", "Succeeding detection. Current Total: "+ timeDifference/1000);
                        /*
                        if(timeDifference >= totalTimeThreshold)
                        {
                            sendNotif("You moved quite a lot. What a surprise.","Alarm Cancelled");
                        }
                        */
                        timePrevious = tempTime;

                    }
                    else
                    {
                        timeDifference = tempTime - timeStarted;
                        totalTimeDifference+=timeDifference;

                        if(totalTimeDifference >= totalTimeThreshold)
                        {
                            sendNotif("You moved quite a lot. What a surprise.","Alarm Cancelled");
                        }
                        else
                        {
                            //sendNotif("You didn't move enough. No surprise there.","Herp Derp");
                            userHasMoved = false;
                        }

                    }
                }

            }
        }
        else
        {
            Log.d("Receiving ", "Sensor values");
            //turnOffSensors();
        }





    }

    void sendNotif(String title, String message)
    {
        Intent mainIntent = new Intent(xContext, InactivityWearActivity.class);
        PendingIntent mainPendingIntent = PendingIntent.getActivity(xContext,0,mainIntent,0);

        Intent acceptIntent = new Intent(xContext, InactivityWearReceiver.class);
        acceptIntent.putExtra("Extra","Accept");
        PendingIntent acceptPendingIntent = PendingIntent.getBroadcast(xContext, 1, acceptIntent, 0);
        int drawableID = R.drawable.bird_active;
        int smallIconID = R.drawable.bird_notif;

        int storedID = prefs.getInt("MessagePackageIndex", 0);

        switch(storedID)
        {
            case 0:
                drawableID = R.drawable.bird_active;
                smallIconID = R.drawable.bird_notif;
                break;

            case 1:
                drawableID = R.drawable.lazy_idle1;
                smallIconID = R.drawable.lazy_idle1;
                break;

            case 2:
                drawableID = R.drawable.heart_active;
                smallIconID = R.drawable.heart_active;
                break;

            case 3:
                drawableID = R.drawable.fit_work1;
                smallIconID = R.drawable.fit_work1;
                break;

            case 4:
                drawableID = R.drawable.jun;
                smallIconID = R.drawable.jun;
                break;
        }

        Bitmap notifBG = BitmapFactory.decodeResource(xContext.getResources(), drawableID);
        Notification notif = new NotificationCompat.Builder(xContext)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(smallIconID)
                .setVibrate(new long[]{0, 500, 100, 500, 100, 500})
                .setAutoCancel(true)
                .setLocalOnly(false)
                .setContentIntent(mainPendingIntent)
                .addAction(drawableID, "Reward Coach", acceptPendingIntent)
                .extend(new NotificationCompat.WearableExtender().setBackground(notifBG))


                //.addAction(R.drawable.ic_launcher,"Snooze",mainPendingIntent)
                .build();
//        NotificationManager nm = (NotificationManager) mContext.getSystemService(mContext.NOTIFICATION_SERVICE);
//        nm.notify(0, notif);

        // Get an instance of the NotificationManager service
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(xContext);

        // Issue the notification with notification manager.
        notificationManager.notify(0, notif);
    }

    String getRandomMessage(Context context)
    {

        prefs = context.getSharedPreferences("com.aeustech.inactivity", Context.MODE_PRIVATE);
        prefsEditor = prefs.edit();
        String defaultMessages = "Get Up!/Stand Up!/Take A Break!/Flap Those Wings!";
        String[] messages;
        String temp = prefs.getString("Messages", defaultMessages);
        messages =  temp.split("\\/");
        randomizer = new Random();
        int index = randomizer.nextInt(messages.length);
        prefsEditor.putString("ActivityDescriptionIndex", messages[index]);
        return messages[index];
    }

    static void initializeSensors(SensorEventListener listener)
    {
        Log.d("Turn On: ", "Sensors");
        xListener = listener;
        sensorManager = (SensorManager)  xContext.getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(xListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);


    }

    static void useContext(Context context)
    {
        xContext = context;
    }

    static void useActivity(InactivityWearActivity activity)
    {
        xActivity = activity;
    }

   static void setSensorThreshold(float newThreshold)
    {
        threshold = newThreshold;

    }

    static void turnOffSensors()
    {
        Log.d("Turn OFf: ", "Sensors");
        sensorManager.unregisterListener(xListener);


    }

    void resetScores()
    {
        prefs = xContext.getSharedPreferences("com.aeustech.inactivity", Context.MODE_PRIVATE);
        prefsEditor = prefs.edit();
        prefsEditor.putInt("ChallengesThrown", 0);
        prefsEditor.putInt("ChallengesAccepted", 0);
    }

    static void initializePrefs()
    {
        prefs = xContext.getSharedPreferences("com.aeustech.inactivity", Context.MODE_PRIVATE);
        prefsEditor = prefs.edit();


    }

    @Override
    public void onMessageReceived(final MessageEvent messageEvent)
    {

        //Toast.makeText(xContext, "MEssage: "+messageEvent.getPath(), Toast.LENGTH_SHORT).show();
        Log.d("AdoptionCall", "Message Event: " +messageEvent.getPath().toString());



                String handHeldMessage = messageEvent.getPath();
                String[] data = handHeldMessage.split("/");

                if (data.length > 1) {

                    if (data[0].equalsIgnoreCase("0"))
                    {
                        //Toast.makeText(xContext, "Adopted Burd", Toast.LENGTH_SHORT).show();
                        buyPackageAtIndex(0);
                    }

                    if (data[0].equalsIgnoreCase("1"))
                    {
                        //Toast.makeText(xContext, "Adopted Chip", Toast.LENGTH_SHORT).show();
                        buyPackageAtIndex(1);
                    }

                    if (data[0].equalsIgnoreCase("2"))
                    {
                        //Toast.makeText(xContext, "Adopted Dr. Doki", Toast.LENGTH_SHORT).show();
                        buyPackageAtIndex(2);
                    }

                    if (data[0].equalsIgnoreCase("3"))
                    {
                        //Toast.makeText(xContext, "Adopted Master Maru", Toast.LENGTH_SHORT).show();
                        buyPackageAtIndex(3);
                    }

                    if(data[0].equalsIgnoreCase("setPackages"))
                    {
                        int x = 1;
                        while (x < data.length )
                        {
                            if(data[x].equalsIgnoreCase("1"))
                            {
                                prefsEditor.putBoolean("Package_"+(x-1), true);
                            }
                            else
                            {
                                prefsEditor.putBoolean("Package_"+(x-1), false);
                            }
                            x++;
                        }
                        prefsEditor.commit();

                        //Intent LaunchIntent = xContext.getPackageManager().getLaunchIntentForPackage("com.aeustech.inactivity");
                        //xContext.startActivity(LaunchIntent);

                    }

                    ((InactivityWearActivity) xContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            ((InactivityWearActivity) xContext).sync();
                        }
                    });








                }


    }

    public void buyPackageAtIndex(int index)
    {
        //Toast.makeText(xContext, "Adopted Coach# "+index, Toast.LENGTH_SHORT).show();
        Log.d("Bought: ",""+index);
        prefsEditor.putBoolean("Package_"+index, true);
        prefsEditor.commit();
    }

    private void initializeApiClient()
    {
        //  Needed for communication between watch and device.
        mGoogleApiClient = new GoogleApiClient.Builder(xContext)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {

                        addListener();
                        //  "onConnected: null" is normal.
                        //  There's nothing in our bundle.
                    }
                    @Override
                    public void onConnectionSuspended(int cause) {
                        Log.d(TAG, "onConnectionSuspended: " + cause);
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        Log.d(TAG, "onConnectionFailed: " + result);
                    }
                })
                .addApi(Wearable.API)
                .build();

        mGoogleApiClient.connect();
    }



    public void addListener() {
        Log.d(TAG, "onConnection: ");
        Wearable.MessageApi.addListener(mGoogleApiClient, this);
    }



}
