package com.aeustech.inactivity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.wearable.view.DismissOverlayView;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class InactivityWearActivity extends Activity implements GoogleApiClient.ConnectionCallbacks{


    Context context;
    SharedPreferences prefs;
    SharedPreferences.Editor prefsEditor;

    LayoutInflater inflater;
    InactivityPagerAdapter pagerAdapter;
    ViewPager pager;

    ImageButton toggleButton,
                btnUpArrow,
                btnDownArrow,
                btnHideShop,
                btnGoToShop,
                btnSelectCoach,
                btnGoToShop2;



    AlarmManager alarmManager;
    PendingIntent pendingIntent;

    int alarmInterval;
    int intervalIndex;//For interval page

    String[] packageNames = new String[5];
    int[] intervals = new int[5];
    double[] happinessPercentages = new double[5];

    ArrayList<String> messages = new ArrayList<String>();
    int messagePackageIndex;
    int tempMessagePackageIndex;

    TextView textCurrentIntensity,
             descriptionText;



    ImageView userActivityIndicator,
              userAvatarPreview,
              userAvatarDescription;


    Random randomizer;

    boolean isActive = false;//If app is running or not

    //ExpandableListAdapter listAdapter;
    //ExpandableListView listView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    HashMap<String, String> activityDescriptions;//use checkBoxName as index
    //List<String> checkedCheckBoxes;//for runtime. Edited when checkbox is checked/unchecked.
                                     //stored as 1 string in prefs
    String[] previousCheckBoxes;//for initial checking of boxes. Gets contents from saved prefs.
    double userActivityIntensity; // = ChallengesAccepted/ChallengesThrown. Gets ChallengesAccepted
                                  // and ChallengesThrown from prefs.
    InactivityWearReceiver receiver;

    ScrollView scrollView;
    FrameLayout descriptionFrame,
                buyFrame;

    // Variables used for watch-device communication
    private GoogleApiClient mGoogleApiClient;


    int currentPage = 0;
    ViewGroup viewGroupLayout;
    FrameLayout frmMain;
    boolean isRound;
    public DismissOverlayView mDismissOverlayView;

/* Pref Ids*/
    //Prefs ids
    //IsActive : (boolean) if app was active on previous close
    //PreviousDate: (String) date of previous app opening. If current date is different, scores reset
    //CustomInterval: (int) value of last saved custom interval, defaulted at INTERVAL_NORMAL
    //IntervalIndex: (int) Index of saved intensity level. Intensity levels are stored in intervals[] array.
    //CheckBoxNames: (String) combined getText() of all checkboxes from previous save.
    //Package_(index): (boolean) if package of index (index) is already bought/unlocked.
    //ChallengesThrown: (int) challenges/alarms thrown. Used to solve indicatorBeast's state
    //ChallengesAccepted: (int) challenges/alarms accepted. Used to solve indicatorBeast's state
    //ActivityDescriptionIndex: (String) index of latest message. Index is the message itself. (HashMap)
    //MessagePackageIndex: (String) package index of the HashMap contents needed


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                        //15, 35
        intervals = new int[] {15, 35, 40, 10, 10};
        packageNames = new String [] {"Pudgy Birb", "Chip", "Doctor Doki", "Master Maru", "IAPP3"};
        happinessPercentages = new double [] {30, 20, 50, 75, 10};


        context = this;
        Log.d("Test", "Entered");


       //initializeStub();
        initializeLayout();
        initializeAPI();

        if(receiver == null)
        {
            InactivityWearReceiver.useContext(context);
            receiver = new InactivityWearReceiver();
            receiver.useContext(context);
            //receiver.initializeSensors(receiver); //Causes Misfire of sensors.
            receiver.initializePrefs();

        }

        sendMessage("syncPackages/");


    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    public void buttonClicked(View v)
    {

        switch(v.getId())
        {
            case R.id.btnStart:
                isActive = !isActive;
                setAlarm(isActive);
                savePrefs();
                updateIntensityLabels();
                updateIndicatorBeast(messagePackageIndex);
                break;

            case R.id.btnLowerIntensity://Intervals/Intensity automatically change upon button click, no more accept
                intervalIndex--;
                maintainIndexInBounds();
                updateIntensityLabels();
                savePrefs();
                break;

            case R.id.btnIncreaseIntensity:
                intervalIndex++;
                maintainIndexInBounds();
                updateIntensityLabels();
                savePrefs();
                break;



            case R.id.btnAcceptChallenge:
                descriptionFrame.setVisibility(View.GONE);
                prefsEditor.putString("ActivityDescriptionIndex", "");

                Log.d("PRESSED: ", ""+prefs.getInt("ChallengesThrown", 0));

                int temp = prefs.getInt("ChallengesAccepted", 0);
                temp++;

                prefsEditor.putInt("ChallengesAccepted", temp);
                Toast.makeText(context, "You Accepted. Score: "+temp, Toast.LENGTH_SHORT).show();
                //Do accept stuff here
                prefsEditor.commit();
                break;

            case R.id.btnUpArrow:

                tempMessagePackageIndex++;
                if(tempMessagePackageIndex > 3)
                {
                    tempMessagePackageIndex = 0;
                }
                Log.d("UP ARROW", tempMessagePackageIndex+"");
                initializeMessages();
                updateAvatarPreview(tempMessagePackageIndex);

                break;

            case R.id.btnDownArrow:

                tempMessagePackageIndex--;

                if(tempMessagePackageIndex < 0)
                {
                    tempMessagePackageIndex = 3;
                }
                Log.d("Down ARROW", tempMessagePackageIndex+"");
                initializeMessages();
                updateAvatarPreview(tempMessagePackageIndex);

                break;


            case R.id.btnSelectCoach:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Are you sure you want to set "+packageNames[tempMessagePackageIndex]+" as your coach?")
                       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                           public void onClick(DialogInterface dialog, int id) {

                               Boolean temp;
                               if(tempMessagePackageIndex >  1)
                               {
                                   temp = false;
                               }
                               else
                               {
                                    temp = true;
                               }
                               Log.d("Set ", "Package_"+ tempMessagePackageIndex +"|||"+prefs.getBoolean("Package_" +tempMessagePackageIndex, temp));
                               if (prefs.getBoolean("Package_" +tempMessagePackageIndex, temp)) {
                                   setCoach(tempMessagePackageIndex);
                               } else {
                                   showBuyScreen(true);
                               }

                           }


                       })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                builder.show();

                break;

            case R.id.btnCloseShop:
                showBuyScreen(false);
                break;

            case R.id.btnOpenPhone:
                //Send MEssage To Phone
               // Toast.makeText(this, "Called to Phone To show Store.(Kuno abi pa lang) 1", Toast.LENGTH_SHORT).show();
                sendMessage("goToBuyScreen");
                showBuyScreen(false);
                break;

            case R.id.btnOpenPhone2:
                //Send MEssage To Phone
                //Toast.makeText(this, "Called to Phone To show Store.(Kuno abi pa lang) 2", Toast.LENGTH_SHORT).show();
                sendMessage("goToBuyScreen");

                break;



        }


    }

    private class InactivityPagerAdapter extends PagerAdapter {
        ArrayList<View> views = new ArrayList<View>();

        TextView timeText;

        public void preloadPacks() {
            views.clear();


            views.add((View) inflater.inflate(R.layout.page1_main_menu, null));
           // views.add((View) inflater.inflate(R.layout.page2_remind_interval, null));
            views.add((View) inflater.inflate(R.layout.page3_activity_packs, null));
            Log.d("TAG", "PRELOADED PACKS");

            /*

            btnIncreaseMinutes = (ImageButton) views.get(1).findViewById(R.id.btnIncreaseMinutes);
            btnDecreaseMinutes = (ImageButton) views.get(1).findViewById(R.id.btnDecreaseMinutes);
            textInterval = (TextView) views.get(1).findViewById(R.id.textIntervalValue);

            textIntensityLabel = (TextView) views.get(1).findViewById(R.id.textIntensityLabel);

            */
            descriptionFrame = (FrameLayout)findViewById(R.id.descriptionFrame);
            descriptionText = (TextView) findViewById(R.id.descriptionText);

            textCurrentIntensity = (TextView) views.get(0).findViewById(R.id.textCurrentIntensityLabel);
            userActivityIndicator = (ImageView) views.get(0).findViewById(R.id.userActivityIndicator);
            toggleButton = (ImageButton) views.get(0).findViewById(R.id.btnStart);

            userAvatarPreview = (ImageView) views.get(1).findViewById(R.id.avatarPreview);
            userAvatarDescription = (ImageView) views.get(1).findViewById(R.id.coachDescription);
            scrollView = (ScrollView) views.get(1).findViewById(R.id.scrollView);
            buyFrame = (FrameLayout) views.get(1).findViewById(R.id.buyScreen);
            btnGoToShop = (ImageButton) views.get(1).findViewById(R.id.btnOpenPhone);
            btnGoToShop2 = (ImageButton) views.get(1).findViewById(R.id.btnOpenPhone2);
            btnHideShop = (ImageButton) views.get(1).findViewById(R.id.btnCloseShop);

            btnUpArrow = (ImageButton) views.get(1).findViewById(R.id.btnUpArrow);
            btnDownArrow = (ImageButton) views.get(1).findViewById(R.id.btnDownArrow);
            btnSelectCoach = (ImageButton) views.get(1).findViewById(R.id.btnSelectCoach);


            scrollView.setOnTouchListener(new DragListener());


            //TO DO: get score from wear
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view==object);
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            View v = views.get(position);
            collection.addView(v, 0);




            return v;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }



    }

    private class DragListener implements View.OnTouchListener
    {
        /*
        public boolean onDrag(View v, DragEvent dragEvent)
        {


            return true;
        }
        */
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {


            //Log.d("Dragged", "Pos: "+ scrollView.getScrollY() );

            if(scrollView.getScrollY() > 80)
            {
                btnUpArrow.setVisibility(View.GONE);
                btnDownArrow.setVisibility(View.GONE);
                btnSelectCoach.setVisibility(View.GONE);
            }
            else
            {
                btnUpArrow.setVisibility(View.VISIBLE);
                btnDownArrow.setVisibility(View.VISIBLE);
                btnSelectCoach.setVisibility(View.VISIBLE);
            }
            return false;//False para d ya pag punggan ang scrolling. Kun i true dan wai ga giho
                        //ang scroll

        }
    }


    public void initializePager()
    {

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new InactivityPagerAdapter();
        pager.setAdapter(pagerAdapter);
        pagerAdapter.preloadPacks();

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {



            }

            @Override
            public void onPageSelected(int position) {

                tempMessagePackageIndex = messagePackageIndex;
                updateAvatarPreview(messagePackageIndex);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initializePagerAdapter() {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewGroupLayout = (ViewGroup) findViewById(android.R.id.content);

        pagerAdapter = new InactivityPagerAdapter();
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);
        pagerAdapter.preloadPacks();
        pager.setOffscreenPageLimit(pagerAdapter.getCount());

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Check if scrolled to page 1

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
                tempMessagePackageIndex = messagePackageIndex;
                updateAvatarPreview(messagePackageIndex);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });



    }

    private String zeroPad(int value) {
        String temp = value < 10 ? "0" + value : value + "";
        return temp;
    }

    private void initializeAlarm()
    {
        alarmInterval = intervals[messagePackageIndex];//Temporary
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent("com.aeustech.inactivity");
        intent.putExtra("Extra", "Alarm");

        pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        //Alarm for resetting ChallengesThrown and ChallengesAccepted prefs
        /*
        Intent resetIntent = new Intent("Extra");
        resetIntent.putExtra("Extra", "Reset");
        PendingIntent pendingResetIntent = PendingIntent.getBroadcast(context, 0, resetIntent, 0);
        Calendar alarmTime = Calendar.getInstance();
        alarmTime.setTimeInMillis(System.currentTimeMillis());//Assume 0:00 am
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime.getTimeInMillis(),   60 * 1000, pendingResetIntent);
        */

    }

    private void initializeLayout() {
        // Stub layouts
        mDismissOverlayView = new DismissOverlayView(this);

        WatchViewStub stub = new WatchViewStub(this);
        stub.setRoundLayout(R.layout.round_activity_inactivity_wear);
        stub.setRectLayout(R.layout.rect_activity_inactivity_wear);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub watchViewStub) {
                //LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, "--- on layout inflated");

                // Detect layout shape
                frmMain = (FrameLayout) findViewById(R.id.frmMainRect);
                if (frmMain == null) {

                    frmMain = (FrameLayout) findViewById(R.id.frmMainRound);
                    isRound = true;

                    ViewGroup parent = (ViewGroup) mDismissOverlayView.getParent();
                    if (parent != null) {
                        parent.removeView(mDismissOverlayView);
                    }
                    frmMain.addView(mDismissOverlayView, new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.MATCH_PARENT));
                } else {

                    frmMain.addView(mDismissOverlayView, new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.MATCH_PARENT));
                    isRound = false;
                }

                initializePagerAdapter();
                initializePrefs();
                initializeListView();
                updateIntensityLabels();

            }
        });
        addContentView(stub, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        stub.inflate();
    }



    public void setAlarm(boolean willAlarm)
    {
        if(willAlarm)
        {

           // receiver.initializeSensors(receiver);
            alarmManager.cancel(pendingIntent);
            initializeAlarm();
            Calendar alarmTime = Calendar.getInstance();
            alarmTime.setTimeInMillis(System.currentTimeMillis());//Time now in milliseconds. Also used as starting time
            alarmTime.add(Calendar.SECOND, alarmInterval * 60);//PLUS wait time, so if next reminder is 15 secs, time now + 15 secs
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime.getTimeInMillis(), alarmInterval * 60 * 1000, pendingIntent);

            //Toast.makeText(this, "alarm set at " + alarmInterval * 60 + " intervals", Toast.LENGTH_SHORT).show();
        }
        else
        {
            receiver.initializeSensors(receiver);
            receiver.turnOffSensors();
            alarmManager.cancel(pendingIntent);

            //Toast.makeText(this, "alarm canceled", Toast.LENGTH_SHORT).show();
        }
    }

    void maintainIndexInBounds()
    {


        if(intervalIndex< 0)
        {
            intervalIndex = 4;

        }

        if(intervalIndex >4 )
        {
            intervalIndex = 0;
        }

        handleCustomIntensity();
    }

    void updateIntensityLabels()
    {
        /*
        intervals = new int[] {INTERVAL_LOW, INTERVAL_NORMAL, INTERVAL_HIGH, INTERVAL_EXTREME, INTERVAL_CUSTOM};
        alarmInterval = intervals[intervalIndex];

        if(alarmInterval < 0)
        {
            alarmInterval = 0;
        }

        textInterval.setText(Integer.toString(alarmInterval));
        textCurrentIntensity.setText(intervalDescriptions[intervalIndex]);
        textIntensityLabel.setText(textCurrentIntensity.getText());
         */

        if(isActive)
        {
            toggleButton.setImageResource(R.drawable.button_clock);
        }
        else
        {
            toggleButton.setImageResource(R.drawable.button_sleep);
        }

    }

    void handleCustomIntensity()
    {
        //Enable or disable controls n shit

        if(intervalIndex < 4)
        {
            //Hide editor buttons

        }

        else
        {
            //Show editor buttons

        }


    }


    void savePrefs()
    {
        prefsEditor.putBoolean("IsActive", isActive);
        prefsEditor.putInt("IntervalIndex", intervalIndex);
        prefsEditor.putInt("MessagePackageIndex", messagePackageIndex);


        String messagestoSave="";

        for(String str : messages)
        {
            messagestoSave+=str+"/";
        }

        Log.d("Messages", messages.toString());
        prefsEditor.putString("Messages", messagestoSave);

        prefsEditor.commit();
    }

    void initializePrefs()
    {
        //test names.
        //String defaultCheckBoxNames = "FP1 Activity 1/FP1 Activity 2/FP1 Activity 3/FP1 Activity 4/FP1 Activity 5/FP1 Activity 6/FP1 Activity 7";
        String defaultMessages = "Get Up!/Stand Up!/Take A Break!/Flap Those Wings!";

        prefs = context.getSharedPreferences("com.aeustech.inactivity", Context.MODE_PRIVATE);
        prefsEditor = prefs.edit();

        isActive = prefs.getBoolean("IsActive", false);
        intervalIndex = prefs.getInt("IntervalIndex", 0);
        messagePackageIndex = prefs.getInt("MessagePackageIndex",0);
        tempMessagePackageIndex = messagePackageIndex;


        updateIndicatorBeast( messagePackageIndex);



        String tempString = prefs.getString("Messages", defaultMessages);
        //Log.d("Loaded ", tempString);
       String[] tempMessages = tempString.split("\\/");
        messages = new ArrayList<String>();

        for(String str : tempMessages)
        {
            messages.add(str);
        }
        Log.d("MESSAGES", ""+tempMessages);
        /*
        checkedCheckBoxes = new ArrayList<String>();//update currently checked, to sync with previous

        for(String str : previousCheckBoxes)
        {
            if(!checkedCheckBoxes.contains(str))
             checkedCheckBoxes.add(str);
        }
        */
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        String prevDate = prefs.getString("PreviousDate", formattedDate);
        Log.d("DATE", formattedDate);

        if(!formattedDate.equals(prevDate))
        {
            resetScores();
            prefsEditor.putString("PreviousDate", formattedDate);
        }


        prefsEditor.commit();


       // Log.d("READ", "READ "+previousCheckBoxes.length+ "  \""+previousCheckBoxes[0]+"\"");

    }

    void initializeListView()
    {
        //listView = (ExpandableListView) pagerAdapter.views.get(2).findViewById(R.id.listView);
        prepareListData();
        //listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        //listView.setAdapter(listAdapter);
        initializeMessages();
        initializeAlarm();

        //((CheckBox)pagerAdapter.views.get(1).findViewById(messagePackageIndex)).setChecked(true);
    }

    void prepareListData()
    {

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();//HAshmap format: (Name of HEader, List of children)
        activityDescriptions = new HashMap<String, String>();


        listDataHeader.add("0");//normal
        listDataHeader.add("1");//low
        listDataHeader.add("2");//In app purchase
        listDataHeader.add("3");//In app purchase
        listDataHeader.add("In-App Package 3");

        List<String> bird = new ArrayList<String>();
        bird.add("Get Up!");
        bird.add("Stand Up!");
        bird.add("Take a Break!");
        bird.add("Flap those wings!");
        bird.add("Bird Activity 5");
        bird.add("Bird Activity 6");
        bird.add("Bird Activity 7");

        activityDescriptions.put("Get Up!", "FP1 Activity1 Description");
        activityDescriptions.put("Stand Up!", "FP1 Activity2 Description");
        activityDescriptions.put("Take a Break!", "FP1 Activity3 Description");
        activityDescriptions.put("Flap those wings!", "FP1 Activity4 Description");
        activityDescriptions.put("Bird Activity 5", "FP1 Activity5 Description");
        activityDescriptions.put("Bird Activity 6", "FP1 Activity6 Description");
        activityDescriptions.put("Bird Activity 7", "FP1 Activity7 Description");

        List<String> lazy = new ArrayList<String>();
        lazy.add("It's Alright");
        lazy.add("Feels Good Man");
        lazy.add("Lie Down A Bit");
        lazy.add("It's Okay To Rest");
        lazy.add("Lazy Activity 5");
        lazy.add("Lazy Activity 6");
        lazy.add("Lazy Activity 7");

        activityDescriptions.put("It's Alright", "FP2 Activity1 Description");
        activityDescriptions.put("Feels Good Man", "FP2 Activity2 Description");
        activityDescriptions.put("Lie Down A Bit", "FP2 Activity3 Description");
        activityDescriptions.put("It's Okay To Rest", "FP2 Activity4 Description");
        activityDescriptions.put("Lazy Activity 5", "FP2 Activity5 Description");
        activityDescriptions.put("Lazy Activity 6", "FP2 Activity6 Description");
        activityDescriptions.put("Lazy Activity 7", "FP2 Activity7 Description");

        List<String> love = new ArrayList<String>();
        love.add("Move Baby <3");
        love.add("Take A Break <3");
        love.add("Hug A Friend");
        love.add("Keep your <3 pumpin'");
        love.add("Love Activity 5");
        love.add("Love Activity 6");
        love.add("Love Activity 7");

        activityDescriptions.put("Move Baby <3", "IAPP1 Activity 1 Description");
        activityDescriptions.put("Take A Break <3", "IAPP1 Activity 2 Description");
        activityDescriptions.put("Hug A Friend", "IAPP1 Activity 3 Description");
        activityDescriptions.put("Keep your <3 pumpin'", "IAPP1 Activity 4 Description");
        activityDescriptions.put("Love Activity 5", "IAPP1 Activity 5 Description");
        activityDescriptions.put("Love Activity 6", "IAPP1 Activity 6 Description");
        activityDescriptions.put("Love Activity 7", "IAPP1 Activity 7 Description");

        List<String> fit = new ArrayList<String>();
        fit.add("Get Moving!");
        fit.add("Stay Active!");
        fit.add("Get Up & Move!");
        fit.add("Do More Excercise!");
        fit.add("Fit Activity 5");
        fit.add("Fit Activity 6");
        fit.add("Fit Activity 7");

        activityDescriptions.put("Get Moving!", "IAPP2 Activity 1 Description");
        activityDescriptions.put("Stay Active!", "IAPP2 Activity 2 Description");
        activityDescriptions.put("Get Up & Move!", "IAPP2 Activity 3 Description");
        activityDescriptions.put("Do More Excercise!", "IAPP2 Activity 4 Description");
        activityDescriptions.put("Fit Activity 5", "IAPP2 Activity 5 Description");
        activityDescriptions.put("Fit Activity 6", "IAPP2 Activity 6 Description");
        activityDescriptions.put("Fit Activity 7", "IAPP2 Activity 7 Description");

        List<String> iAPPkg3 = new ArrayList<String>();
        iAPPkg3.add("IAPP3 Activity 1");
        iAPPkg3.add("IAPP3 Activity 2");
        iAPPkg3.add("IAPP3 Activity 3");
        iAPPkg3.add("IAPP3 Activity 4");
        iAPPkg3.add("IAPP3 Activity 5");
        iAPPkg3.add("IAPP3 Activity 6");
        iAPPkg3.add("IAPP3 Activity 7");

        activityDescriptions.put("IAPP3 Activity 1", "IAPP3 Activity 1 Description");
        activityDescriptions.put("IAPP3 Activity 2", "IAPP3 Activity 2 Description");
        activityDescriptions.put("IAPP3 Activity 3", "IAPP3 Activity 3 Description");
        activityDescriptions.put("IAPP3 Activity 4", "IAPP3 Activity 4 Description");
        activityDescriptions.put("IAPP3 Activity 5", "IAPP3 Activity 5 Description");
        activityDescriptions.put("IAPP3 Activity 6", "IAPP3 Activity 6 Description");
        activityDescriptions.put("IAPP3 Activity 7", "IAPP3 Activity 7 Description");

        listDataChild.put(listDataHeader.get(0), bird);
        listDataChild.put(listDataHeader.get(1), lazy);
        listDataChild.put(listDataHeader.get(2), love);
        listDataChild.put(listDataHeader.get(3), fit);
        listDataChild.put(listDataHeader.get(4), iAPPkg3);



        if(!prefs.getBoolean("Package_0", false))
        {
            initializePackages();
        }


        String desc = prefs.getString("ActivityDescriptionIndex", "");

        if(!desc.equals(""))
        {

            Log.d("Desc: ", desc);
            descriptionText.setText(activityDescriptions.get(desc));
            //descriptionFrame.setVisibility(View.VISIBLE);
        }


    }

    String getRandomMessage()
    {
         randomizer = new Random();
            Log.d("SIZE", ""+messages.size());
         int index = randomizer.nextInt(messages.size());

        return messages.get(index);
    }

    void initializeMessages()
    {

       // messages = new ArrayList<String>();
        List<String> tempList = listDataChild.get(messagePackageIndex+"");
        messages = (ArrayList<String>)tempList;

        /*
        int currentPackageIndex = listAdapter.getGroupCount()-1;//reads from last package, to first

        Log.d("currentPackageIndex: ", currentPackageIndex+"");
        listAdapter.setCheckBoxes(previousCheckBoxes);

        for(String msg : previousCheckBoxes)
        {
           if( !messages.contains(msg))
           {
               messages.add(msg);

           }
            else
           {
               Log.d("Double: ", msg);
           }


        }
        */
        Log.d("MEssages Size: ", tempList+"");



    }

    public void buyPackageAtIndex(int index)
    {
        Toast.makeText(context, "Adopted Coach# "+index, Toast.LENGTH_SHORT).show();
        prefsEditor.putBoolean("Package_"+index, true);
        prefsEditor.commit();
    }

    public void updateIndicatorBeast(int index)
    {
        //Update activity intensity indicator
        //if(userActivityIntensity== blahblah)
        //userActivityIndicator.setImageResource();

        int challengesThrown = prefs.getInt("ChallengesThrown", 0);
        int challengesAccepted = prefs.getInt("ChallengesAccepted", 0);
        AnimationDrawable animationDrawable;
        Log.d("Index", index+"");

        if(challengesThrown > 0 && challengesAccepted > 0)
        {
            userActivityIntensity = challengesAccepted/challengesThrown;
        }
        else
        {
            userActivityIntensity = happinessPercentages[messagePackageIndex];
        }


            if(index == 0)
            {
                if(isActive)
                {
                    if(userActivityIntensity >= happinessPercentages[messagePackageIndex])
                    {
                        userActivityIndicator.setBackgroundResource(R.drawable.bird_on_anim);

                        //Animate
                    }
                    else//Angry or Disappointed
                    {

                        userActivityIndicator.setBackgroundResource(R.drawable.bird_angry_anim);
                    }

                }
                else
                {
                    userActivityIndicator.setBackgroundResource(R.drawable.bird_off_anim);
                }


                userAvatarPreview.setImageResource(R.drawable.bird_on1);

            }

            if(index == 1)
            {
                if(isActive)
                {
                    if(userActivityIntensity >= happinessPercentages[messagePackageIndex])
                    {
                        userActivityIndicator.setBackgroundResource(R.drawable.lazy_on_anim);
                    }
                    else//Angry or Disappointed. Does lazy coach even get disappointed???
                    {

                        userActivityIndicator.setBackgroundResource(R.drawable.lazy_on_anim);
                        //Animate
                    }

                }
                else
                    userActivityIndicator.setBackgroundResource(R.drawable.lazy_off_anim);

                userAvatarPreview.setImageResource(R.drawable.lazy_on1);
            }

            if(index == 2)
            {
                if(isActive)
                {
                    if(userActivityIntensity >= happinessPercentages[messagePackageIndex])
                    {
                        userActivityIndicator.setBackgroundResource(R.drawable.heart_on_anim);
                        //Animate
                    }
                    else//Angry or Disappointed
                    {
                        userActivityIndicator.setBackgroundResource(R.drawable.heart_angry_anim);
                    }
                }
                else
                {
                    userActivityIndicator.setBackgroundResource(R.drawable.heart_off_anim);
                    //Animate
                }


                userAvatarPreview.setBackgroundResource(R.drawable.heart_active);
            }

            if(index == 3)
            {
                if(isActive)
                {
                    if(userActivityIntensity >= happinessPercentages[messagePackageIndex])
                    {
                        userActivityIndicator.setBackgroundResource(R.drawable.fit_on_anim);
                        //Animate
                    }
                    else//Disappointed or angry
                    {
                        userActivityIndicator.setBackgroundResource(R.drawable.fit_angry_anim);
                        //Animate
                    }
                }

                else
                {
                    userActivityIndicator.setBackgroundResource(R.drawable.fit_off_anim);
                    //Animate
                }


                userAvatarPreview.setBackgroundResource(R.drawable.fit_idle1);
            }

            if(index == 4)
            {
                if(isActive)
                {
                    if(userActivityIntensity >= happinessPercentages[messagePackageIndex])
                    {
                        userActivityIndicator.setImageResource(R.drawable.jun);
                    }
                    else//Angry disappointed
                    {
                        userActivityIndicator.setImageResource(R.drawable.jun);
                    }
                }

                else
                {
                    userActivityIndicator.setImageResource(R.drawable.jun);
                }


                userAvatarPreview.setImageResource(R.drawable.jun);
            }


        animationDrawable = (AnimationDrawable) userActivityIndicator.getBackground();
        animationDrawable.start();





    }

    public void updateAvatarPreview(int index)
    {
        if(index == 0)
        {
            userAvatarPreview.setImageResource(R.drawable.bird_on1);
            userAvatarDescription.setImageResource(R.drawable.info_bird);

        }

        if(index == 1)
        {
            userAvatarPreview.setImageResource(R.drawable.lazy_on1);
            userAvatarDescription.setImageResource(R.drawable.info_lazy);
        }

        if(index == 2)
        {
            userAvatarPreview.setImageResource(R.drawable.heart_on1);
            userAvatarDescription.setImageResource(R.drawable.info_heart);
        }

        if(index == 3)
        {
            userAvatarPreview.setImageResource(R.drawable.fit_idle1);
            userAvatarDescription.setImageResource(R.drawable.info_fit);
        }

        if(index == 4)
        {
            userAvatarPreview.setImageResource(R.drawable.jun);
            userAvatarDescription.setImageResource(R.drawable.info_lazy);
        }

        Log.d("ButtonVisibility", "Package_"+index+"  "+prefs.getBoolean("Package_"+index, false));
        if(!prefs.getBoolean("Package_"+index, false))
        {
            btnGoToShop2.setVisibility(View.VISIBLE);
        }
        else
        {
            btnGoToShop2.setVisibility(View.GONE);
        }

    }
    void resetScores()
    {
        Log.d("SCores: ", "REset");
        prefsEditor.putInt("ChallengesThrown", 0);
        prefsEditor.putInt("ChallengesAccepted", 0);
    }

    void setCoach(int index)
    {
        intervalIndex = index;
        messagePackageIndex = index;

        isActive = true;
        setAlarm(isActive);

        initializeMessages();
        resetScores();
        updateIntensityLabels();
        updateIndicatorBeast(messagePackageIndex);
        savePrefs();
        pager.setCurrentItem(0);



    }

    void showBuyScreen(boolean willShow)
    {
            if(willShow)
            {
                buyFrame.setVisibility(View.VISIBLE);
            }
            else
            {
                buyFrame.setVisibility(View.GONE);
            }
    }

    void initializePackages()
    {
        int numberOfFreePackages = 2;//Free packages
        int x = 0;

        while(x<listDataHeader.size())
        {


            if(x<numberOfFreePackages)
            {
                prefsEditor.putBoolean("Package_"+x, true);
                Log.d("Package_"+x+": ",""+prefs.getBoolean("Package_"+x, true));
            }
            else
            {
                prefsEditor.putBoolean("Package_"+x, false);
                Log.d("Package_"+x+": ",""+prefs.getBoolean("Package_"+x, false));
            }

            x++;


        }

        prefsEditor.commit();
        Log.d("Initialized", "Packages");
    }



    private void sendMessage(final String msg){

        new AsyncTask<Void, Void, List<Node>>() {

            @Override
            protected List<Node> doInBackground(Void... params) {
                return getNodes();
            }

            @Override
            protected void onPostExecute(List<Node> nodeList) {


                Log.d("Nodes: ", "" + nodeList.size());

                for (final Node node : nodeList) {

                    PendingResult<MessageApi.SendMessageResult> result = Wearable.MessageApi.sendMessage(
                            mGoogleApiClient,
                            node.getId(),
                            msg,
                            null
                    );
                    Log.d("WEAR", "Sent " + msg);
                    result.setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                            //  The message is done sending.
                            //  This doesn't mean it worked, though.

                            //peerNode = node;    //  Save the node that worked so we don't have to loop again.
                        }
                    });
                }
            }
        }.execute();


    }

    private List<Node> getNodes() {
        List<Node> nodes = new ArrayList<Node>();
        NodeApi.GetConnectedNodesResult rawNodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
        for (Node node : rawNodes.getNodes()) {
            nodes.add(node);
        }
        return nodes;
    }


    @Override
    public void onConnected(Bundle bundle) {
       // LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, "connected to Google Play Services on Wear!");
        Wearable.MessageApi.addListener(mGoogleApiClient, receiver).setResultCallback(resultCallback);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private void initializeAPI() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {

                    }
                })
                .addApi(Wearable.API)
                .build();

        mGoogleApiClient.connect();
    }

    private ResultCallback<Status> resultCallback =  new ResultCallback<Status>() {
        @Override
        public void onResult(Status status) {

            new AsyncTask<Void, Void, Void>(){
                @Override
                protected Void doInBackground(Void... params) {
//                    sendHandheldMessage();
                    return null;
                }
            }.execute();
        }
    };



    public void sync()
    {
        updateAvatarPreview(tempMessagePackageIndex);
        Log.d("Sync", "Sync");
    }



}
