package com.aeustech.inactivity;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Terence John Lampasa on 3/9/15.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {


    private Context _context;
    private List<String> _listDataHeader;//Header titles
    private HashMap<String, List<String>> _listDataChild;
    String[] checkBoxList;
    //child data in format of header title, child title


    public ExpandableListAdapter(Context context, List<String> listDataHeader, HashMap<String, List<String>> listChildData)
    {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;

    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent)
    {
        /*
        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_packs_item, null);
        }


        CheckBox listChild = (CheckBox) convertView
                .findViewById(R.id.lblListItem);

        SharedPreferences prefs = _context.getSharedPreferences("com.aeustech.inactivity", Context.MODE_PRIVATE);
        boolean packageIsUnlocked = prefs.getBoolean("Package_"+groupPosition, false);

        if(packageIsUnlocked || groupPosition < 2 || childPosition == 0)//group pos = 2 since 2 free packages. [Free, Free, Paid, Paid, Paid]
        {
            listChild.setText(childText);
            listChild.setChecked(isChecked(childText));
            listChild.setTag("Unlocked");
        }
        else
        {
            listChild.setTag("Locked");
        }
        */
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_packs_header, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    public void setCheckBoxes(String[] previousBoxes)
    {
        checkBoxList = new String[previousBoxes.length];
        checkBoxList = previousBoxes;
        Log.d("listLength: ", checkBoxList.length+"");
    }

    boolean isChecked(String newText)
    {
        for(String box: checkBoxList)
        {
            if(newText.equals(box))
            {
                return true;
            }
            else
            {

            }
        }
        //Log.d("TEXTBOX: ", "false");
        return false;
    }

}
