package com.aeustech.inactivity;

import android.content.ComponentName;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

/**
 * Created by aeustech on 3/31/15.
 */
public class InactivityListeningService extends WearableListenerService{


    @Override
    public void onMessageReceived(MessageEvent messageEvent)
    {





        if(messageEvent.getPath().indexOf("goToBuyScreen") != -1)
        {

            Intent startIntent = new Intent("android.intent.action.MAIN");
            startIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


            startIntent.setComponent(ComponentName.unflattenFromString("com.aeustech.inactivity/com.aeustech.inactivity.InactivityActivity"));
            startIntent.addCategory("android.intent.category.LAUNCHER");

            startActivity(startIntent);

            ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 75);
            toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 1000);

            Intent broadcastIntent = new Intent("com.aeustech.inactivity.broadcast");
            broadcastIntent.putExtra("data", messageEvent.getPath()+"/");
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);

        }

        if(messageEvent.getPath().indexOf("syncPackages") != -1)
        {
            Intent broadcastIntent = new Intent("com.aeustech.inactivity.broadcast");
            broadcastIntent.putExtra("data", messageEvent.getPath()+"/");
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);

        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }



}
