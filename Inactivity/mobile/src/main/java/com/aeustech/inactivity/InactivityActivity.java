package com.aeustech.inactivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.ArrayList;
import java.util.List;


public class InactivityActivity extends Activity{

    Context context;
    LayoutInflater inflater;
    InactivityPagerAdapter pagerAdapter;
    ViewPager pager;
    int numberOfPackages = 4;
    int currentPage = 0;
    BroadcastReceiver resultReceiver;

    private static String TAG = "Inactivity_Phone";

    //Variables for Buying
    Interface apiBindings;
    String product = "0";

    public boolean boughtFromWatch = false;
    public boolean checkingPrevPurchases = false;
    public boolean consumeNC = true;
    public boolean prevPurchasesFound = false;


    SharedPreferences prefs;
    SharedPreferences.Editor prefsEditor;

    ImageButton btnAdoptionBird,
                btnAdoptionLazy,
                btnAdoptionHeart,
                btnAdoptionFit;

    ImageView   adoptionBird,
                adoptionLazy,
                adoptionHeart,
                adoptionFit;

    Button btnOtherApps;
    String itemID = "0/0";

    // Variables used for watch-device communication
    private GoogleApiClient mGoogleApiClient;


    @Override
    protected void onPause()
    {
        super.onPause();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onActivityResult","Yay");
        if(apiBindings.iabHelper == null) {
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }

        if (!apiBindings.iabHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inactivity);

        context = this;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        resultReceiver = initializeBroadcastReceiver();

        initializeApiClient();
        apiBindings = new Interface();
        apiBindings.context = this;

        StaticObjects.iabHelper = apiBindings.gplayInitIAB();
        //StaticObjects.gameHelper = apiBindings.gplayInitGameHelper();

        LocalBroadcastManager.getInstance(context).registerReceiver(resultReceiver, new IntentFilter("com.aeustech.inactivity.broadcast"));

        initializePager();
        checkIfNewInstall();

    }


    public void buttonClicked(View v)
    {
        Intent appStoreIntent;

        switch(v.getId())
        {
            case R.id.btn_otherapps:
                if(currentPage != 3) {
                    pager.setCurrentItem(3);
                    btnOtherApps.setText("How To Use");
                }
                else
                {
                    pager.setCurrentItem(0);
                    btnOtherApps.setText("Other Apps");
                }
                break;

            case R.id.btnAdoptionBird:
                //Toast.makeText(context, "Adopted Burd", Toast.LENGTH_SHORT).show();
                apiBindings.gplayIABBuyItem("bird");//put id here or sumthin
                itemID = "0/0";
                prefsEditor.putBoolean("Package_0", true );
                tellWatchConnectedState(itemID);
                prefsEditor.commit();
                syncPackagesWithWear();
                break;

            case R.id.btnAdoptionLazy:
               // Toast.makeText(context, "Adopted Lazy", Toast.LENGTH_SHORT).show();
                apiBindings.gplayIABBuyItem("lazy");//put id here or sumthin
                itemID = "1/1";
                tellWatchConnectedState(itemID);
                prefsEditor.putBoolean("Package_1", true );
                prefsEditor.commit();
                syncPackagesWithWear();
                break;

            case R.id.btnAdoptionDoki:
                //Toast.makeText(context, "Adopted Doki", Toast.LENGTH_SHORT).show();
                apiBindings.gplayIABBuyItem("doki");//put id here or sumthin
                itemID = "2/2";
                tellWatchConnectedState(itemID);
                checkingPrevPurchases = false;
                //prefsEditor.putBoolean("Package_2", true );
                //prefsEditor.commit();
                //syncPackagesWithWear();
                break;

            case R.id.btnAdoptionFit:
                //Toast.makeText(context, "Adopted Fit", Toast.LENGTH_SHORT).show();
                apiBindings.gplayIABBuyItem("fit");//put id here or sumthin
                itemID = "3/3";
                tellWatchConnectedState(itemID);
                checkingPrevPurchases = false;
                //prefsEditor.putBoolean("Package_3", true );
                //prefsEditor.commit();
                //syncPackagesWithWear();
                break;



            case R.id.btn_wtc:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.weartipcalculator"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                //flurryLogEvent("LAUNCH_WIT_PLAY_STORE");
                break;

            case R.id.btn_hm:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.aeustech.hydrateme"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);
                Log.d("Hydrate", "Me");
                break;
                //flurryLogEvent("LAUNCH_HM_PLAY_STORE");

            case R.id.btn_wrc:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.wearrotarycalculator"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                //flurryLogEvent("LAUNCH_WIT_PLAY_STORE");
                break;

            case R.id.btn_wit:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.wearintervaltimer"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                //flurryLogEvent("LAUNCH_WIT_PLAY_STORE");
                break;

            case R.id.btn_cm:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.colormatch"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                //flurryLogEvent("LAUNCH_CM_PLAY_STORE");
                break;

            case R.id.btn_omgwe:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.ohmygravitywear"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                //flurryLogEvent("LAUNCH_OMGW_PLAY_STORE");
                break;

            case R.id.btn_mb:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.matchblox"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                //flurryLogEvent("LAUNCH_MB_PLAY_STORE");
                break;

            case R.id.btn_omg:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.ohmygravity"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                //flurryLogEvent("LAUNCH_OMGH_PLAY_STORE");
                break;

            case R.id.btn_shapopo:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.shapopo"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                //flurryLogEvent("LAUNCH_SPP_PLAY_STORE");
                break;

            case R.id.btn_htc:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.hitthecan"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                //flurryLogEvent("LAUNCH_SPP_PLAY_STORE");
                break;

            case R.id.btn_et:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.wearexpensetracker"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                //flurryLogEvent("LAUNCH_SPP_PLAY_STORE");
                break;

            case R.id.btn_rate:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.aeustech.inactivity"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);


                break;

            case R.id.btn_restore:
                checkingPrevPurchases = false;
                apiBindings.gplayRestorePurchase();

                break;
            case R.id.debugger:
                //prefsEditor.putBoolean("Package_2",false);
                //prefsEditor.putBoolean("Package_3",false);
                ///syncPackagesWithWear();
                break;


        }

        prefsEditor.commit();
        updateAdoptionButtons();

    }

    public void initializePager()
    {

        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new InactivityPagerAdapter();
        pager.setAdapter(pagerAdapter);
        pagerAdapter.preloadPacks();

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {



            }

            @Override
            public void onPageSelected(int position) {

                currentPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private class InactivityPagerAdapter extends PagerAdapter {
        ArrayList<View> views = new ArrayList<View>();

        TextView timeText;

        public void preloadPacks() {
            views.clear();


            views.add((View) inflater.inflate(R.layout.page1_tutorial, null));
            views.add((View) inflater.inflate(R.layout.page2_how_to_select, null));
            views.add((View) inflater.inflate(R.layout.page3_pet_store, null));
            views.add((View) inflater.inflate(R.layout.page4_other_apps, null));

            Log.d("TAG", "PRELOADED PACKS");
            adoptionBird = (ImageView) views.get(2).findViewById(R.id.birdA);
            adoptionLazy = (ImageView) views.get(2).findViewById(R.id.lazyA);
            adoptionHeart = (ImageView) views.get(2).findViewById(R.id.heartA);
            adoptionFit = (ImageView) views.get(2).findViewById(R.id.fitA);

            btnAdoptionBird = (ImageButton) views.get(2).findViewById(R.id.btnAdoptionBird);
            btnAdoptionLazy = (ImageButton) views.get(2).findViewById(R.id.btnAdoptionLazy);
            btnAdoptionHeart = (ImageButton) views.get(2).findViewById(R.id.btnAdoptionDoki);
            btnAdoptionFit = (ImageButton) views.get(2).findViewById(R.id.btnAdoptionFit);
            btnOtherApps = (Button) findViewById(R.id.btn_otherapps);

            //TO DO: get score from wear
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view==object);
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            View v = views.get(position);
            collection.addView(v, 0);

            return v;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_inactivity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private BroadcastReceiver initializeBroadcastReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {


                String[] data = intent.getStringExtra("data").split("/");
                Log.d("Received in Activity", data[0]);
                if (data[0].equalsIgnoreCase("goToBuyScreen")) {
                    Log.d("Entered Go To Buy", "Screen");
                    pager.setCurrentItem(2);
                }
                if (data[0].equalsIgnoreCase("syncPackages")) {

                    syncPackagesWithWear();
                }
            }
        };
    }

    private void tellWatchConnectedState(final String message){

        new AsyncTask<Void, Void, List<Node>>(){

            @Override
            protected List<Node> doInBackground(Void... params) {
                return getNodes();
            }

            @Override
            protected void onPostExecute(List<Node> nodeList) {
                for(Node node : nodeList) {
                    Log.d(TAG, "Phone telling " + node.getId() + " i am " + message);

                    PendingResult<MessageApi.SendMessageResult> result = Wearable.MessageApi.sendMessage(
                            mGoogleApiClient,
                            node.getId(),
                            message,
                            null
                    );

                    result.setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                            Log.d(TAG, "Phone: " + sendMessageResult.getStatus().getStatusMessage());
                        }
                    });
                }
            }
        }.execute();

    }

    private List<Node> getNodes() {
        List<Node> nodes = new ArrayList<Node>();
        NodeApi.GetConnectedNodesResult rawNodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
        for (Node node : rawNodes.getNodes()) {
            nodes.add(node);
        }
        return nodes;
    }

    private void initializeApiClient()
    {
        //  Needed for communication between watch and device.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {


                        //  "onConnected: null" is normal.
                        //  There's nothing in our bundle.
                    }
                    @Override
                    public void onConnectionSuspended(int cause) {
                        Log.d(TAG, "onConnectionSuspended: " + cause);
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        Log.d(TAG, "onConnectionFailed: " + result);
                    }
                })
                .addApi(Wearable.API)
                .build();

        mGoogleApiClient.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
        apiBindings.gplayDestroyIAB();
    }

    public void cancelBuy() {
        Log.d("PHONE", "Cancel Buy");
        tellWatchConnectedState("cancel/buy/");
    }

    public void finishedBuy(String item) {

        //itemList = itemList.replace(".", "/");
        tellWatchConnectedState("itemList/"+item);

        String[] data = item.split("/");
        String index = item;
        String coachName = item;

        switch(item)
        {
            case "doki":
                index = "2";
                coachName = "Doctor Doki";
                break;

            case "fit":
                index = "3";
                coachName = "Master Maru";
                break;
        }

        Log.d("Item ID (FinishedBuy)", "Package " + index);
        //Toast.makeText(context, "Purchased "+item, Toast.LENGTH_SHORT).show();
        prefsEditor.putBoolean("Package_"+index, true );

        prefsEditor.commit();

        updateAdoptionButtons();
        syncPackagesWithWear();
        //txtUnlockMessage.setText("Thank you for your support!");
        //txtUnlockMessage.setVisibility(View.VISIBLE);

        if(!checkingPrevPurchases) {
            //Toast.makeText(context, "All modes are unlocked.\nPlease check your watch.", Toast.LENGTH_SHORT).show();
            showAlert("Coach " + coachName +" has been unlocked.\nPlease check your watch.");
        }
        else
        {

        }

    }

    public void checkIfNewInstall()
    {



        prefs = context.getSharedPreferences("com.aeustech.inactivity", Context.MODE_PRIVATE);
        prefsEditor = prefs.edit();

        if(!prefs.getBoolean("Package_0", false) && !checkingPrevPurchases)//dapat true ni, kay free ang package_0 (bird)
        {                                       // if false meaning wai pa kaagi initialize
            initializePackages();
        }


        updateAdoptionButtons();
       syncPackagesWithWear();

    }

    void initializePackages()
    {
        int numberOfFreePackages = 2;//Free packages
        int x = 0;

        while(x<numberOfPackages)
        {


            if(x<numberOfFreePackages)
            {
                prefsEditor.putBoolean("Package_"+x, true);
                Log.d("Package_"+x+": ",""+prefs.getBoolean("Package_"+x, true));
            }
            else
            {
                prefsEditor.putBoolean("Package_"+x, false);
                Log.d("Package_"+x+": ",""+prefs.getBoolean("Package_"+x, false));
            }

            x++;


        }

        prefsEditor.commit();
        Log.d("Initialized", "Packages");


    }

    void updateAdoptionButtons()
    {
        int x = 0;

        while(x<numberOfPackages)
        {



            if(prefs.getBoolean("Package_" + x, false))
            {
                Log.d("Package_"+x+": ",""+prefs.getBoolean("Package_"+x, false));

                //a = adopted, b = unadopted

                if(x==0)
                {
                    adoptionBird.setImageResource(R.drawable.iat_companion_profile_bird_01a);
                    btnAdoptionBird.setVisibility(View.GONE);
                }

                if(x==1)
                {
                    adoptionLazy.setImageResource(R.drawable.iat_companion_profile_lazy_01a);
                    btnAdoptionLazy.setVisibility(View.GONE);
                }

                if(x==2)
                {
                    adoptionHeart.setImageResource(R.drawable.iat_companion_profile_heart_01a);
                    btnAdoptionHeart.setVisibility(View.GONE);
                }

                if(x==3)
                {
                    adoptionFit.setImageResource(R.drawable.iat_companion_profile_fit_01a);
                    btnAdoptionFit.setVisibility(View.GONE);
                }



            }
            else
            {
                if(x==0)
                {
                    adoptionBird.setImageResource(R.drawable.iat_companion_profile_bird_01b);
                    btnAdoptionBird.setVisibility(View.VISIBLE);
                }

                if(x==1)
                {
                    adoptionLazy.setImageResource(R.drawable.iat_companion_profile_lazy_01b);
                    btnAdoptionLazy.setVisibility(View.VISIBLE);
                }

                if(x==2)
                {
                    adoptionHeart.setImageResource(R.drawable.iat_companion_profile_heart_01b);
                    btnAdoptionHeart.setVisibility(View.VISIBLE);
                }

                if(x==3)
                {
                    adoptionFit.setImageResource(R.drawable.iat_companion_profile_fit_01b);
                    btnAdoptionFit.setVisibility(View.VISIBLE);
                }
            }

            x++;




        }
    }


    void syncPackagesWithWear()
    {
        int x = 0;
        String msg = "setPackages/";
        while(x < numberOfPackages)
        {
            if(prefs.getBoolean("Package_"+x, false))
            {
                msg+="1/";
            }
            else
            {
                msg+="0/";
            }
            x++;
        }

        Log.d("Message", msg);
        tellWatchConnectedState(msg);
    }


    public void showAlert(String message) {
        final String msg = message;
        new AlertDialog.Builder(context)
                .setTitle("Inactivity Coach")
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                }).show();
    }




}
