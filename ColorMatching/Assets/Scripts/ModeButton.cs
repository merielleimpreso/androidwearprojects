﻿using UnityEngine;
using System.Collections;



public class ModeButton : MonoBehaviour {

	public GameType modeType;
	public SpriteRenderer spriteDependency;//Color sprite which corresponds to this
	public SpriteRenderer mySpriteRenderer;
	public SpriteRenderer myTextSpriteRenderer;
	Sprite defaultSprite;
	Sprite textDefaultSprite;
	public Sprite selectedSprite;
	public Sprite textSelectedSprite;
	public GameObject lockIndicator;


	Master master;

	void Start () 
	{
		master = GameObject.Find("Master").GetComponent<Master>();
		defaultSprite = mySpriteRenderer.sprite;
		textDefaultSprite = myTextSpriteRenderer.sprite;
		Debug.Log (modeType.ToString()+"_unlocked");
		if(PlayerPrefs.GetInt(modeType.ToString()+"_unlocked") == 0)
		{

			mySpriteRenderer.enabled =false;
		}
	}
	

	void Update () 
	{
		if(master.gameType == modeType)
		{
			mySpriteRenderer.sprite = selectedSprite;
			myTextSpriteRenderer.sprite = textSelectedSprite;
		}
		else
		{
			mySpriteRenderer.sprite = defaultSprite;
			myTextSpriteRenderer.sprite = textDefaultSprite;
		}

		if(PlayerPrefs.GetInt(modeType.ToString()+"_unlocked") == 1 )
		{
			if(lockIndicator)
				lockIndicator.SetActive(false);

			mySpriteRenderer.enabled =true;
		}

		if(PlayerPrefs.GetInt(modeType.ToString()+"_unlocked") == 0 )
		{
			if(lockIndicator)
				lockIndicator.SetActive(true);
			mySpriteRenderer.enabled = false;
		}
	}

	public void Activate()
	{
		if(PlayerPrefs.GetInt(modeType.ToString()+"_unlocked") == 1 || modeType == GameType.Classic || !lockIndicator)
		{
			master.SetMode(modeType, spriteDependency.sprite);
		}

		//master.StartGame();

		//transform.parent.gameObject.SetActive(false);

	}
}
