using UnityEngine;
using System.Collections;

public enum ButtonType
{
	OpenModeMenu,
	OpenMainMenu,
	StartGame,
	TogglePause,
	Restart,
	Quit,
	UnlockButton,
	Notice,
	ShowScores
}

public class GenericButton : MonoBehaviour {

	public ButtonType buttonType;
	public GameObject modeMenu;
	public GameObject mainMenu;
	public GameObject gameOver;
	public GameObject pauseScreen;

	Master master;

	void Start () 
	{
		if(PlayerPrefs.GetInt("Timed_unlocked") == 1 && buttonType == ButtonType.Notice)
		{
			//gameObject.SetActive(false);
		}

	}
	

	void Update () 
	{
		if(buttonType == ButtonType.TogglePause)
		{
			gameObject.SetActive(master.isStarted);

			if(master.isEnded)
				gameObject.SetActive(false);
		}


	
	}

	public void Activate()
	{
		if(buttonType == ButtonType.OpenModeMenu && (master.isEnded || !master.isStarted ) && master.gameOverWait <= 0)
		{
			modeMenu.SetActive(true);
			mainMenu.SetActive(false);
			gameOver.SetActive(false);
			Time.timeScale = 1;
			master.DisableButtonPressAnimation();

			//master.isEnded = false;
			master.ClearScore();
		}

		if(buttonType == ButtonType.StartGame && master.gameOverWait <= 0)
		{
			modeMenu.SetActive(false);
			mainMenu.SetActive(false);
			gameOver.SetActive(false);
			Time.timeScale = 1;
			master.StartGame();
			//master.ClearScore();
			
		}

		if(buttonType == ButtonType.OpenMainMenu)
		{
			modeMenu.SetActive(false);
			mainMenu.SetActive(true);
			gameOver.SetActive(false);
			pauseScreen.SetActive(false);
			Time.timeScale = 1;
			master.DisableButtonPressAnimation();

			master.isStarted = false;
			master.isPaused = false;
			master.isEnded = false;
			master.lastInSequence = false;
			master.ClearMemoryModeArray();
			//master.isEnded = false;
			master.CleanTimerBar();
			master.ClearScore();
			master.ResetRotation();

		}

		if(buttonType == ButtonType.TogglePause)
		{
			master.isPaused = !master.isPaused;

			pauseScreen.SetActive(master.isPaused);
			Debug.Log ("PAuse");
			if(!master.isPaused)
			{
				Time.timeScale = 1;
				if(master.gameType != GameType.Memory)
					master.scoreText.text = master.Score().ToString();
				else
					master.scoreText.text = master.CurrentColorIndex().ToString() + "/" + master.ColorIndexesToMemorizeLength().ToString();
			}
			else
				Time.timeScale = 0;
		}


		if(buttonType == ButtonType.Quit)
		{
			Debug.Log("Quit");
			Application.Quit();

		}

		if(buttonType == ButtonType.UnlockButton)
		{
			master.CompanionGoToBuy();
		}

		if(buttonType == ButtonType.Restart)
		{
			master.Restart();

			modeMenu.SetActive(false);
			mainMenu.SetActive(false);
			gameOver.SetActive(false);
			pauseScreen.SetActive(false);
		}

		if(buttonType == ButtonType.Notice)
		{
			gameObject.SetActive(false);
		}

		if(buttonType == ButtonType.ShowScores)
		{
			master.ShowCompanionScores();
			Debug.Log ("Show Scores");
		}

	}

	public void Initialize(Master masterX)
	{
		//Debug.Log (transform.name);
		master = masterX;

		if(buttonType == ButtonType.TogglePause)
			pauseScreen.SetActive(master.isPaused);
		//modeMenu = GameObject.Find("ModeMenu");
		//mainMenu = GameObject.Find ("MainMenu");
		//gameOver = GameObject.Find("GameOver");
		
		//if(master.isStarted)
		//	modeMenu.SetActive(false);
		
		//if(gameOver.transform != transform.parent)
		//	gameOver.SetActive(false);
		//gameOver = GameObject.Find("GameOver");
	}
}
