﻿/*
 * This class will contain code needed to interface with platform-specific native code (iOS, Android, etc.).
 * This class will determine what platform the app is running on and call the correct native functions. 
 */

using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

#if UNITY_ANDROID

class AndroidPlugin {
	public AndroidJavaObject pluginObject;
}

#endif

public class NativeCodeBridge : MonoBehaviour {
	
	[DllImport ("__Internal")]
	private static extern void showIADBanner(string show);
	
	[DllImport ("__Internal")]
	private static extern void initIAds();
	
	[DllImport ("__Internal")]
	private static extern void restorePurchases();
	
	[DllImport ("__Internal")]
	private static extern void updatePlayerAuthStatus();
	
	[DllImport ("__Internal")]
	private static extern void logFlurryEvent(string flurryEvent);
	
	[DllImport ("__Internal")]
	private static extern void initFlurry();
	
	[DllImport ("__Internal")]
	private static extern void showAchievements();
	
	[DllImport ("__Internal")]
	private static extern void submitAchievement(string aID, string percent);
	
	[DllImport ("__Internal")]
	private static extern void showLeaderBoard(string lbID);
	
	[DllImport ("__Internal")]
	private static extern void submitBestScore(string score, string lbID);
	
	[DllImport ("__Internal")]
	private static extern void initGameCenter();
	
	[DllImport ("__Internal")]
	private static extern void gamePlayStarted(string message);
	
	[DllImport ("__Internal")]
	private static extern void initAds();
	
	[DllImport ("__Internal")]
	private static extern void showAds(string location);
	
	[DllImport ("__Internal")]
	private static extern void rateApp();
	
	[DllImport ("__Internal")]
	private static extern void fbShare(string message);
	
	[DllImport ("__Internal")]
	private static extern void buyMoonRox(string moonRox);
	
	[DllImport ("__Internal")]
	private static extern void echoTest(string message);
	
	[DllImport ("__Internal")]
	private static extern void sendEmail(string xmlFile);
	
	[DllImport ("__Internal")]
	private static extern void init();
	
	// the following methods act as a bridge between native and Unity code, do not call these directly from Unity
	
	// IOS
	public static void IOS_ShowIADBanner(string show)
	{
		showIADBanner(show);
	}
	
	public static void IOS_InitIAds()
	{
		initIAds();
	}
	
	public static void IOS_RestorePurchases()
	{
		restorePurchases();
	}
	
	public static void IOS_UpdatePlayerAuthStatus()
	{
		updatePlayerAuthStatus();
	}
	
	public static void IOS_LogFlurryEvent(string flurryEvent)
	{
		logFlurryEvent(flurryEvent);
	}
	
	public static void IOS_InitFlurry()
	{
		initFlurry();
	}
	
	public static void IOS_ShowAchievements()
	{
		showAchievements();
		NativeCodeBridge.LogFlurryEvent("SHOW_ACHIEVEMENTS");
		
	}
	
	public static void IOS_SubmitAchievement(string aID, string percent)
	{
		submitAchievement(aID, percent);
		NativeCodeBridge.LogFlurryEvent("SUBMIT_ACHIEVEMENT_" + aID);
		
	}
	
	public static void IOS_ShowLeaderBoard(string lbID)
	{
		showLeaderBoard(lbID);
		NativeCodeBridge.LogFlurryEvent("SHOW_LEADER_BOARD_" + lbID);
	}
	
	public static void IOS_SubmitBestScore(string score, string lbID)
	{
		submitBestScore(score,lbID);
		NativeCodeBridge.LogFlurryEvent("SUBMIT_BEST_SCORE");
	}
	
	public static void IOS_InitGameCenter()
	{
		initGameCenter();
	}
	
	public static void IOS_GamePlayStarted(string message)
	{
		gamePlayStarted(message);
	}
	
	public static void IOS_InitAds()
	{
		initAds();
	}
	
	public static void IOS_ShowAds(string location)
	{
		showAds(location);
		NativeCodeBridge.LogFlurryEvent("SHOW_CB_ADS_" + location);
		
	}
	
	public static void IOS_RateApp()
	{
		rateApp();
		NativeCodeBridge.LogFlurryEvent("RATE_APP");
		
	}
	
	public static void IOS_FbShare(string message)
	{
		fbShare(message);
		NativeCodeBridge.LogFlurryEvent("FB_SHARE_" + message);
		
	}
	
	public static void IOS_BuyMoonRox(string moonRox)
	{
		buyMoonRox(moonRox);
		NativeCodeBridge.LogFlurryEvent("BUY_ROX_" + moonRox);
		
	}
	
	public static void IOS_EchoTest(string message)
	{
		echoTest(message);
	}
	
	public static void IOS_SendEmail(string xmlFile)
	{
		sendEmail(xmlFile);
	}
	
	public static void IOS_Init()
	{
		init();
	}
	
	// Android
	#if UNITY_ANDROID
	static AndroidPlugin plugin;
	#endif
	public static void Android_Init()
	{
		#if UNITY_ANDROID
		
		AndroidJNI.AttachCurrentThread();
		
		plugin = new AndroidPlugin();
		plugin.pluginObject = new AndroidJavaObject("com.aeustech.colormatch.Interface");
		
		#endif
	}
	
	public static void Android_FbShare(string message) 
	{
		#if UNITY_ANDROID
		
		//plugin.pluginObject.Call("fbPostStatusUpdate", message);
		
		#endif
	}
	
	public static void Android_LogFlurryEvent(string flurryEvent) 
	{
		#if UNITY_ANDROID
		
		//plugin.pluginObject.Call("flurryLogEvent", flurryEvent);
		
		#endif
	}
	
	public static void Android_ShowLeaderBoard(string lbID)
	{
		#if UNITY_ANDROID
		
		//plugin.pluginObject.Call("gplayLeaderboardShow", lbID);
		
		#endif
		
		//NativeCodeBridge.LogFlurryEvent("SHOW_LEADER_BOARD_" + lbID);
	}
	
	public static void Android_SubmitBestScore(string score, string lbID)
	{
		#if UNITY_ANDROID
		
		//plugin.pluginObject.Call("gplayLeaderboardSubmitScore", lbID, int.Parse(score));
		
		#endif
		
		//NativeCodeBridge.LogFlurryEvent("SUBMIT_BEST_SCORE_" + score + "_" + lbID);
		
	}
	
	public static void Android_SubmitAchievement(string aID)
	{
		#if UNITY_ANDROID
		
		//plugin.pluginObject.Call("gplayAchievementsUnlock", aID);
		
		#endif
		
		//NativeCodeBridge.LogFlurryEvent("SUBMIT_ACHIEVEMENT_" + aID);
		
	}
	
	public static void Android_ShowAchievements()
	{
		#if UNITY_ANDROID
		
		//plugin.pluginObject.Call("gplayAchievementsShow");
		
		#endif
		
		//NativeCodeBridge.LogFlurryEvent("SHOW_ACHIEVEMENTS");
		
	}
	
	public static void Android_BuyMoonRox(string moonRox)
	{
		#if UNITY_ANDROID
		
		//plugin.pluginObject.Call("gplayIABBuyItem", moonRox);
		
		#endif
		
		//NativeCodeBridge.LogFlurryEvent("BUY_ITEM_" + moonRox);
	}
	
	public static void Android_ShowAds(string location) 
	{
		#if UNITY_ANDROID
		
		//plugin.pluginObject.Call("cbShowAd", location);
		
		#endif
	}
	
	public static void Android_RateApp()
	{
		#if UNITY_ANDROID
		
		//plugin.pluginObject.Call("rateApp");
		
		#endif		
		
		//NativeCodeBridge.LogFlurryEvent("RATE_APP");
		
	}
	
	public static void Android_GenericFunctions(string message) 
	{
		#if UNITY_ANDROID
		
		//plugin.pluginObject.Call("genericFunctions", "");
		
		#endif
	}
	
	// utility functions
	static bool GameCenterEnabled() {
		
		//NativeCodeBridge.UpdatePlayerAuthStatus();
		//if(PlayerPrefs.GetInt("GameCenterEnabled") == 1) {
		//	NativeCodeBridge.LogFlurryEvent("GAME_CENTER_ENABLED");
		
		//	return true;
		//}
		
		//NativeCodeBridge.LogFlurryEvent("GAME_CENTER_DISABLED");
		
		return false;
	}
	
	// call the following methods from Unity, add any platform-specific conditions
	public static void ShowIADBanner(string show)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
			IOS_ShowIADBanner(show);
	}
	
	public static void InitIAds()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
			IOS_InitIAds();
	}
	
	public static void RestorePurchases()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
			IOS_RestorePurchases();
	}
	
	public static void UpdatePlayerAuthStatus()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
			IOS_UpdatePlayerAuthStatus();
	}
	
	public static void LogFlurryEvent(string flurryEvent)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			IOS_LogFlurryEvent(flurryEvent);
		} else if(Application.platform == RuntimePlatform.Android) {
			Android_LogFlurryEvent(flurryEvent);;
		}
	}
	
	public static void InitFlurry()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
			IOS_InitFlurry();
	}
	
	public static void ShowAchievements()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer && GameCenterEnabled()) {
			IOS_ShowAchievements();
		} else if (Application.platform == RuntimePlatform.Android) {
			Android_ShowAchievements();
		}
	}
	
	public static void SubmitAchievement(string aID, string percent)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer && GameCenterEnabled()) {
			IOS_SubmitAchievement(aID, percent);
		} else if (Application.platform == RuntimePlatform.Android) {
			Android_SubmitAchievement(aID);
		}
	}
	
	public static void ShowLeaderBoard(string lbID)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer && GameCenterEnabled()) {
			IOS_ShowLeaderBoard(lbID);
		} else if (Application.platform == RuntimePlatform.Android) {
			Android_ShowLeaderBoard(lbID);
		}
	}
	
	public static void SubmitBestScore(string score, string lbID)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer && GameCenterEnabled()) {
			IOS_SubmitBestScore(score, lbID);
		} else if (Application.platform == RuntimePlatform.Android) {
			Android_SubmitBestScore(score, lbID);
		}
	}
	
	public static void InitGameCenter()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
			IOS_InitGameCenter();
	}
	
	public static void GamePlayStarted(string message)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
			IOS_GamePlayStarted(message);
	}
	
	public static void InitAds()
	{
		
		if (Application.platform == RuntimePlatform.IPhonePlayer)
			IOS_InitAds();
		
		
	}
	
	public static void ShowAds(string location)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			IOS_ShowAds(location);
		} else if(Application.platform == RuntimePlatform.Android) {
			Android_ShowAds(location);
		}
	}
	
	public static void RateApp()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			IOS_RateApp();
		} else if(Application.platform == RuntimePlatform.Android) {
			Android_RateApp();
		}
	}
	
	public static void FbShare(string message)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			IOS_FbShare(message);
		} else if(Application.platform == RuntimePlatform.Android) {
			Android_FbShare(message);
		}
	}
	
	public static void BuyMoonRox(string moonRox)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			IOS_BuyMoonRox(moonRox);
		} else if(Application.platform == RuntimePlatform.Android) {
			Android_BuyMoonRox(moonRox);
		}
	}
	
	public static void EchoTest(string message)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
			IOS_EchoTest(message);
	}
	
	public static void SendEmail(string xmlFile)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
			IOS_SendEmail(xmlFile);
	}
	
	public static void Init()
	{
		
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			IOS_Init();
		} else if(Application.platform == RuntimePlatform.Android) {
			Android_Init();
		}
		
	}
	
	
	// Android Wear specific functions
	public static void DownloadFullVersion()
	{
		#if UNITY_ANDROID
		
		plugin.pluginObject.Call("downloadFullVersion");
		
		#endif
	}
	
	public static void QuitGame()
	{
		#if UNITY_ANDROID
		
		plugin.pluginObject.Call("quitGame");
		
		#endif
	}
	
}

