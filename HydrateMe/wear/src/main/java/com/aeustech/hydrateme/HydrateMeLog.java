package com.aeustech.hydrateme;

/**
 * Created by lnz on 12/2/14.
 */
public class HydrateMeLog {
    public int id;
    public String date;
    public String time;

    public HydrateMeLog(int id, String date, String  time) {
        this.id = id;
        this.date = date;
        this.time = time;
    }

}
