package com.aeustech.hydrateme;

import java.util.ArrayList;
import java.util.List;

public class Group {

    public String date;
    public int goal;
    public final List<HydrateMeLog> children = new ArrayList<HydrateMeLog>();

    public Group(String date, int goal) {
        this.date = date;
        this.goal = goal;
    }

}
