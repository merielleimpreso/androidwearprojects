package com.aeustech.hydrateme;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Merielle Impreso on 1/7/15.
 */
public class HydrateMeReceiver extends BroadcastReceiver {
    private Context mContext;
    SharedPreferences prefs;

    @Override
    public void onReceive(Context c, Intent i) {
        mContext = c;
        prefs = mContext.getSharedPreferences("com.aeustech.hydrateme", Context.MODE_PRIVATE);

        sendNote("Drink water!");
    }

    private void sendNote(String message) {
        if (isBetweenTimeRange(prefs)) {
            log("Drink water!");

            // Broadcast to reset timer
            // Convert time to seconds
            int hour = prefs.getInt("alarmHour", 0);
            int mins = prefs.getInt("alarmMinutes", 0);
            int msUntilAlarm = hour * 60 * 60;
            msUntilAlarm += mins * 60;
            msUntilAlarm *= 1000;

            Calendar alarmTime = Calendar.getInstance();
            alarmTime.add(Calendar.SECOND, msUntilAlarm / 1000);

            prefs.edit().putLong("alarmTime", alarmTime.getTimeInMillis()).apply();

            Intent mainIntent = new Intent(mContext,  MainActivity.class);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            mainIntent.putExtra("data", "notificationDefault/");
            PendingIntent mainPI = PendingIntent.getActivity(mContext, 0, mainIntent, PendingIntent.FLAG_CANCEL_CURRENT);

            Intent snoozeIntent = new Intent(mContext, MainActivity.class);
            snoozeIntent.putExtra("data", "notificationSnooze/");
            PendingIntent snoozePI = PendingIntent.getActivity(mContext, 1, snoozeIntent, PendingIntent.FLAG_CANCEL_CURRENT);

            Intent recordIntent = new Intent(mContext, MainActivity.class);
            recordIntent.putExtra("data", "notificationRecord/");
            recordIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent recordPI = PendingIntent.getActivity(mContext, 2, recordIntent, PendingIntent.FLAG_CANCEL_CURRENT);


            Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.notification_bg);
            // Make a notification
            Notification notif = new NotificationCompat.Builder(mContext)
                    .setContentTitle("HydrateMe")
                    .setContentText(message)
                    .setSmallIcon(R.drawable.notification)
                    .setVibrate(new long[]{0, 500, 100, 500, 100, 500})
                    .setLocalOnly(false)
                    .setAutoCancel(true)
                    .setContentIntent(mainPI)
                    .addAction(R.drawable.notification_snooze, "Snooze", snoozePI)
                    .addAction(R.drawable.notification_record, "Record", recordPI)
                    .extend(new NotificationCompat.WearableExtender().setBackground(bitmap))
                    .build();


            // Get an instance of the NotificationManager service
            NotificationManagerCompat notificationManager =
                    NotificationManagerCompat.from(mContext);

            // Issue the notification with notification manager.
            notificationManager.notify(0, notif);

            Intent broadcastIntent = new Intent("com.aeustech.hydrateme.broadcast");
            broadcastIntent.putExtra("data", "resetAlarmTimer/");
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(broadcastIntent);
        }
    }

    public boolean isBetweenTimeRange(SharedPreferences prefs) {
        Calendar cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR_OF_DAY); // Get hour in 24 hour format
        int minute = cal.get(Calendar.MINUTE);
        Date now = parseDate(hour + ":" + minute);

        int fromHour = prefs.getInt("fromHour", 8);
        int fromMinute = prefs.getInt("fromMinute", 0);
        String fromAMPM = prefs.getString("fromAMPM", "AM");

        if (fromAMPM.equals("PM")) {
            fromHour += 12;
        } else {
            if (fromHour == 12) {
                fromHour -= 12;
            }
        }

        int toHour = prefs.getInt("toHour", 8);
        int toMinute = prefs.getInt("toMinute", 0);
        String toAMPM = prefs.getString("toAMPM", "PM");

        if (toAMPM.equals("PM")) {
            toHour += 12;
        } else {
            if (toHour == 12) {
                toHour -= 12;
            }
        }

        Date start = parseDate(fromHour + ":" + fromMinute);
        Date end = parseDate(toHour + ":" + toMinute);

        if (start.after(end)) {
            if (now.after(start)) {
                return true;
            }
            if (now.before(end)) {
                return true;
            }
        } else if (start.equals(end)) {
            return true;
        } else {
            if (now.after(start) && now.before(end)) {
                return true;
            }
        }
        return false;
    }

    private Date parseDate(String date) {

        final String inputFormat = "HH:mm";
        SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.getDefault());
        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }

    public void log(String message) {
        //Log.d("HydrateMe", message);
    }


}
