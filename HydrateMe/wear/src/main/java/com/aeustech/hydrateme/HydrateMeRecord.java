package com.aeustech.hydrateme;

/**
 * Created by lnz on 11/26/14.
 */
public class HydrateMeRecord {
    public String date;
    public int goal;

    public HydrateMeRecord(String date, int goal) {
        this.date = date;
        this.goal = goal;
    }
}
