package com.aeustech.hydrateme;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.LocalBroadcastManager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyExpandableListAdapter extends BaseExpandableListAdapter {

    private final SparseArray<Group> groups;
    public LayoutInflater inflater;
    public Activity activity;

    public MyExpandableListAdapter(Activity act, SparseArray<Group> groups) {
        activity = act;
        this.groups = groups;
        inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groups.get(groupPosition).children.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat df2 = new SimpleDateFormat("hh:mm a");

        final HydrateMeLog child = (HydrateMeLog) getChild(groupPosition, childPosition);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.wear_page4_record_listrow_details, null);
        }
        TextView text = (TextView) convertView.findViewById(R.id.textView1);
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/Arvo-Regular.ttf");
        text.setTypeface(tf);

        try {
            Date d = df1.parse(child.time);
            text.setText(df2.format(d));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ImageView imgDelete = (ImageView) convertView.findViewById(R.id.imgDelete);
        if (groupPosition == 0) {
            imgDelete.setVisibility(View.VISIBLE);
            convertView.setOnClickListener(new OnClickListener() {
                @Override

                //Toast.makeText(activity, child.time, Toast.LENGTH_SHORT).show();
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setMessage("Do you want to remove?");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Group group = (Group) getGroup(groupPosition);
                                    group.children.remove(childPosition);
                                    notifyDataSetChanged();
                                    dialog.cancel();

                                    HydrateMeDatabase database = new HydrateMeDatabase(activity);
                                    ((MainActivity) activity).consumed -= 1;
                                    try {
                                        database.open();
                                        database.deleteLog(child);
                                        database.close();
                                    } catch (Exception e) {

                                    }
                                }
                            });
                    builder.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }

            });
        } else {
            imgDelete.setVisibility(View.INVISIBLE);
            convertView.setOnClickListener(null);
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return groups.get(groupPosition).children.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
        if (getGroupCount() == 9) {
            if (groupPosition >= 7) {
                Intent broadcastIntent = new Intent("com.aeustech.hydrateme.broadcast");
                broadcastIntent.putExtra("data", "goToUnlockPage/");
                LocalBroadcastManager.getInstance(activity).sendBroadcast(broadcastIntent);
            }
        }
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
        if (getGroupCount() == 9) {
            if (groupPosition >= 7) {
                Intent broadcastIntent = new Intent("com.aeustech.hydrateme.broadcast");
                broadcastIntent.putExtra("data", "goToUnlockPage/");
                LocalBroadcastManager.getInstance(activity).sendBroadcast(broadcastIntent);
            }
        }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        int childrenCount = getChildrenCount(groupPosition);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.wear_page4_record_listrow_group, null);
        }

        final int position = groupPosition;
        Group group = (Group) getGroup(groupPosition);
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/Arvo-Regular.ttf");

        if (getGroupCount() == 9) {
            if (groupPosition < 7) {
                updateViews(convertView, childrenCount, isExpanded, group, tf);
            } else {
                LinearLayout layoutRow = (LinearLayout) convertView.findViewById(R.id.layoutRow);
                TextView txtUnlock = (TextView) convertView.findViewById(R.id.txtUnlock);
                layoutRow.setVisibility(View.GONE);
                txtUnlock.setVisibility(View.VISIBLE);
                txtUnlock.setTypeface(tf);
                txtUnlock.setText(group.date);
            }
        } else {
            updateViews(convertView, childrenCount, isExpanded, group, tf);
        }
        return convertView;
    }

    private void updateViews(View convertView, int childrenCount, boolean isExpanded, Group group, Typeface tf) {
        LinearLayout layoutRow = (LinearLayout) convertView.findViewById(R.id.layoutRow);
        TextView txtUnlock = (TextView) convertView.findViewById(R.id.txtUnlock);
        layoutRow.setVisibility(View.VISIBLE);
        txtUnlock.setVisibility(View.GONE);

        ImageView imgIndicator = (ImageView) convertView.findViewById(R.id.imgIndicator);
        if (childrenCount > 0) {
            imgIndicator.setVisibility(View.VISIBLE);
            if (isExpanded) {
                imgIndicator.setImageResource(R.drawable.record_arrow2);
            } else {
                imgIndicator.setImageResource(R.drawable.record_arrow1);
            }
        } else {
            imgIndicator.setVisibility(View.INVISIBLE);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.textView1);
        textView.setTypeface(tf);
        textView.setText(group.date);

        TextView txtCount = (TextView) convertView.findViewById(R.id.txtCount);
        String count = Integer.toString(childrenCount) + "/" + group.goal;
        txtCount.setText(count);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        if (groupPosition == 0) {
            return true;
        } else {
            if (getGroupCount() == 9) {
                if (groupPosition < 7) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        }
    }

    void log(String msg) {
        //Log.d("HYDRATEME_A", msg);
    }
}
