package com.aeustech.hydrateme;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class HydrateMeDatabase {
    public static final String DATABASE_NAME = "aeus_hydrateme";
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_TABLE_RECORD = "record";
    public static final String RECORD_DATE = "date";
    public static final String RECORD_GOAL = "goal";
    public static final String DATABASE_TABLE_LOG = "date";
    public static final String LOG_ID = "logid";
    public static final String LOG_TIME = "time";

    private DbHelper helper;
    private Context context;
    public static SQLiteDatabase database;

    private static class DbHelper extends SQLiteOpenHelper {

        public DbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + DATABASE_TABLE_RECORD + " (" +
                            RECORD_DATE + " DATE UNIQUE PRIMARY KEY, " +
                            RECORD_GOAL + " INTEGER);"
            );

            db.execSQL("CREATE TABLE " + DATABASE_TABLE_LOG + " (" +
                            LOG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            RECORD_DATE + " DATE, " +
                            LOG_TIME + " DATETIME);"
            );
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_RECORD);
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_LOG);
            onCreate(db);
        }
    }

    public HydrateMeDatabase(Context context) {
        this.context = context;
    }

    public HydrateMeDatabase open() throws SQLException {
        this.helper = new DbHelper(context);
        this.database = helper.getWritableDatabase();
        return this;
    }

    public void close() {
        helper.close();
    }

    public long createEntry(HydrateMeRecord record) {
        ContentValues cv = new ContentValues();
        cv.put(RECORD_DATE, record.date);
        cv.put(RECORD_GOAL, record.goal);
        return database.insert(DATABASE_TABLE_RECORD, null, cv);
    }

    public long createLog() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String date = df.format(new Date());
        String time = df1.format(new Date());

        ContentValues cv = new ContentValues();
        cv.put(RECORD_DATE, date);
        cv.put(LOG_TIME, time);
        log("date = " + date + ", time = " + time);

        return database.insert(DATABASE_TABLE_LOG, null, cv);
    }

    public void updateEntry(HydrateMeRecord record) {
        ContentValues cv = new ContentValues();
        cv.put(RECORD_DATE, record.date);
        cv.put(RECORD_GOAL, record.goal);
        database.update(DATABASE_TABLE_RECORD, cv, "date='" + record.date + "'", null);
    }

    public HydrateMeRecord getRecord(String date) {
        HydrateMeRecord record = null;

        // Query database where date = today
        String[] columns = new String[] {
                HydrateMeDatabase.RECORD_DATE,
                HydrateMeDatabase.RECORD_GOAL };
        Cursor c = HydrateMeDatabase.database.query(HydrateMeDatabase.DATABASE_TABLE_RECORD,
                columns, "date='" + date + "'", null, null, null, null);

        // Get the index of each column
        int iDate = c.getColumnIndex(HydrateMeDatabase.RECORD_DATE);
        int iGoal = c.getColumnIndex(HydrateMeDatabase.RECORD_GOAL);

        // Get the result
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            String rDate = c.getString(iDate);
            int rGoal = c.getInt(iGoal);
            record = new HydrateMeRecord(rDate, rGoal);
            break;
        }

        return record;
    }

    public ArrayList<HydrateMeRecord> getAllRecords() {
        ArrayList<HydrateMeRecord> records = new ArrayList<HydrateMeRecord>();

        // Query database where date = today
        String[] columns = new String[] {
                HydrateMeDatabase.RECORD_DATE,
                HydrateMeDatabase.RECORD_GOAL };
        Cursor c = HydrateMeDatabase.database.query(HydrateMeDatabase.DATABASE_TABLE_RECORD,
                columns, null, null, null, null, HydrateMeDatabase.RECORD_DATE + " DESC");

        // Get the index of each column
        int iDate = c.getColumnIndex(HydrateMeDatabase.RECORD_DATE);
        int iGoal = c.getColumnIndex(HydrateMeDatabase.RECORD_GOAL);

        // Get the result
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            String rDate = c.getString(iDate);
            int rGoal = c.getInt(iGoal);
            records.add(new HydrateMeRecord(rDate, rGoal));
        }

        return records;
    }

    public int countLogsWithDate(String date) {
        String[] columns = new String[] { LOG_ID, RECORD_DATE, LOG_TIME };
        Cursor c = database.query(DATABASE_TABLE_LOG, columns, "date='" + date + "'", null, null, null, null);

        int count = 0;
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            count++;
        }
        return count;
    }

    public ArrayList<HydrateMeLog> getLogsWithDate(String date) {
        String[] columns = new String[] { LOG_ID, RECORD_DATE, LOG_TIME };
        Cursor c = database.query(DATABASE_TABLE_LOG, columns, "date='" + date + "'", null, null, null,
                HydrateMeDatabase.LOG_TIME + " DESC");
        ArrayList<HydrateMeLog> logs = new ArrayList<HydrateMeLog>();

        // Get the index of each column
        int iId = c.getColumnIndex(HydrateMeDatabase.LOG_ID);
        int iDate = c.getColumnIndex(HydrateMeDatabase.RECORD_DATE);
        int iTime = c.getColumnIndex(HydrateMeDatabase.LOG_TIME);

        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            int lId = c.getInt(iId);
            String lDate = c.getString(iDate);
            String lTime = c.getString(iTime);
            logs.add(new HydrateMeLog(lId, lDate, lTime));
        }
        return logs;
    }

    public void deleteLog(HydrateMeLog log) {
        database.delete(DATABASE_TABLE_LOG, LOG_ID + "=" + log.id, null);
    }

    public void deleteOldRecords() {
        database.delete(DATABASE_TABLE_RECORD, RECORD_DATE + "< date('now', '-30 days')", null);
        database.delete(DATABASE_TABLE_LOG, RECORD_DATE + "< date('now', '-30 days')", null);
    }

    public void log (String msg) {
        //Log.d("HYDRATEME_W", msg);
    }

}
