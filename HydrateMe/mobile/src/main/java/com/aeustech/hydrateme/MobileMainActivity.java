package com.aeustech.hydrateme;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.ArrayList;
import java.util.List;


public class MobileMainActivity extends Activity {
    // Variable to access this class context
    Context context;

    // Variables for pages
    LayoutInflater inflater;
    ViewGroup viewGroupLayout;
    HydrateMePagerAdapter pagerAdapter;
    ViewPager pager;
    int currentPage = 0;
    Button visitButton;

    // Variables used for watch-device communication
    private GoogleApiClient mGoogleApiClient;
    BroadcastReceiver resultReceiver;

    // Variables used for buying products
    Interface apiBindings;
    String product = "0";
    SharedPreferences prefs;
    Button buyButton;
    TextView txtUnlockMessage;

    public boolean boughtFromWatch = false;
    public boolean checkingPrevPurchases = false;
    public boolean consumeNC = false;
    public boolean prevPurchasesFound = false;

    // For logging purposes
    final static String TAG = "HydrateMe_H";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile_layout);

        context = this;
        prefs = this.getSharedPreferences("com.aeustech.hydrateme", Context.MODE_PRIVATE);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            product = extras.getString("product");
            log("product = " + product);
        }

        // Instantiate a ViewPager and a PagerAdapter
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewGroupLayout = (ViewGroup) findViewById(android.R.id.content);
        visitButton = (Button) viewGroupLayout.findViewById(R.id.btn_otherapps);

        pagerAdapter = new HydrateMePagerAdapter();
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);
        pagerAdapter.preloadPacks();

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

                if(currentPage > 3) {
                    visitButton.setText("How To Use");
                } else {
                    visitButton.setText("Other Apps");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Needed for communication between watch and device.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        log("onConnected: " + connectionHint);
                    }
                    @Override
                    public void onConnectionSuspended(int cause) {
                        log("onConnectionSuspended: " + cause);
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        log("onConnectionFailed: " + result);
                    }
                })
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();

        resultReceiver = initializeBroadcastReceiver();
        LocalBroadcastManager.getInstance(context).registerReceiver(resultReceiver, new IntentFilter("com.aeustech.hydrateme.broadcast"));

        apiBindings = new Interface();
        apiBindings.context = this;
        apiBindings.gplayInitIAB();

    }

    @Override
    protected void onStart() {
        super.onStart();
        flurryStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        flurryStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (resultReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(resultReceiver);
        }
        apiBindings.gplayDestroyIAB();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(apiBindings.iabHelper == null) {
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }

        if (!apiBindings.iabHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void tellWatchConnectedState(final String message){
        new AsyncTask<Void, Void, List<Node>>(){

            @Override
            protected List<Node> doInBackground(Void... params) {
                return getNodes();
            }

            @Override
            protected void onPostExecute(List<Node> nodeList) {
                for(Node node : nodeList) {
                    log("telling " + node.getId() + " " + message);

                    PendingResult<MessageApi.SendMessageResult> result = Wearable.MessageApi.sendMessage(
                            mGoogleApiClient,
                            node.getId(),
                            message,
                            null);

                    result.setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                            Log.v(TAG, "Phone: " + sendMessageResult.getStatus().getStatusMessage());
                        }
                    });
                }
            }
        }.execute();
    }

    private List<Node> getNodes() {
        List<Node> nodes = new ArrayList<Node>();
        NodeApi.GetConnectedNodesResult rawNodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
        for (Node node : rawNodes.getNodes()) {
            nodes.add(node);
        }
        return nodes;
    }

    private BroadcastReceiver initializeBroadcastReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                log("broadcast received = " + intent.getStringExtra("data"));

                String[] data = intent.getStringExtra("data").split("/");
                if (data[0].equalsIgnoreCase("goToUnlockPage")) {
                    showBuyPopup();
                }
            }
        };
    }

    public void showBuyPopup() {
        if(pagerAdapter.views.size() > 0) {
            pager.setCurrentItem(4);

            boughtFromWatch = true;

            if(prevPurchasesFound) {
                showAlert("You already own this item.\nPlease press the Restore button to unlock 30-day progress.");
            }

            flurryLogEvent("SHOW_BUY_CONFIRM_POPUP");
        }
    }

    public void finishedBuy() {

        if(!checkingPrevPurchases) {
            log("buy/restore completed");
            tellWatchConnectedState("complete/unlockMonthlyVersion/");
        } else {
            log("autorestore completed");
            tellWatchConnectedState("autorestore/unlockMonthlyVersion/");
        }

        buyButton.setVisibility(View.GONE);
        txtUnlockMessage.setVisibility(View.VISIBLE);

        if(!checkingPrevPurchases) {
            showAlert("30-day logs has been unlocked.\nPlease check your watch.");
        }

    }

    public void cancelBuy() {
        log("cancel buy");
        tellWatchConnectedState("cancel/unlockMonthlyVersion/");
    }

    public void showAlert(String message) {
        log("show alert");
        final String msg = message;
        new AlertDialog.Builder(context)
                .setTitle("Wear Rotary Calculator")
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                }).show();
    }



    private class HydrateMePagerAdapter extends PagerAdapter {
        ArrayList<View> views = new ArrayList<View>();

        public void preloadPacks() {
            views.clear();

            views.add((View) inflater.inflate(R.layout.mobile_layout_page1, null));
            views.add((View) inflater.inflate(R.layout.mobile_layout_page2, null));
            views.add((View) inflater.inflate(R.layout.mobile_layout_page3, null));
            views.add((View) inflater.inflate(R.layout.mobile_layout_page3_1, null));
            views.add((View) inflater.inflate(R.layout.mobile_layout_page4, null));
            views.add((View) inflater.inflate(R.layout.mobile_layout_page5, null));

            buyButton = (Button) views.get(4).findViewById(R.id.btn_buy);
            txtUnlockMessage = (TextView) views.get(4).findViewById(R.id.txtUnlockMessage);
        }

        @Override
        public int getCount() {
            return 6;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view==object);
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            View v = views.get(position);
            collection.addView(v, 0);
            return v;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }
    }

    public void buttonClicked(View v) {

        Intent appStoreIntent;

        switch (v.getId()) {

            case R.id.btn_wtc:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.weartipcalculator"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_WIT_PLAY_STORE");
                break;

            case R.id.btn_wrc:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.wearrotarycalculator"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_WIT_PLAY_STORE");
                break;

            case R.id.btn_wit:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.wearintervaltimer"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_WIT_PLAY_STORE");
                break;

            case R.id.btn_cm:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.colormatch"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_CM_PLAY_STORE");
                break;

            case R.id.btn_omgwe:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.ohmygravitywear"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_OMGW_PLAY_STORE");
                break;

            case R.id.btn_mb:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.matchblox"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_MB_PLAY_STORE");
                break;

            case R.id.btn_omg:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.ohmygravity"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_OMGH_PLAY_STORE");
                break;

            case R.id.btn_shapopo:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.shapopo"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_SPP_PLAY_STORE");
                break;

            case R.id.btn_rate:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.hydrateme"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);
                flurryLogEvent("LAUNCH_HM_PLAY_STORE");

                break;

            case R.id.btn_buy:
                checkingPrevPurchases = false;
                log("buy monthly version");
                showBuyPopup();

                boughtFromWatch = true;
                checkingPrevPurchases = false;
                apiBindings.gplayIABBuyItem("monthlyversion");

                break;

            case R.id.btn_restore:
                checkingPrevPurchases = false;
                apiBindings.gplayRestorePurchase();

                break;

            case R.id.btn_otherapps:
                if (pager.getCurrentItem() < 5) {
                    pager.setCurrentItem(5);
                    ((Button) v).setText("How To Use");

                    flurryLogEvent("SEE_OTHER_APPS");
                } else {
                    pager.setCurrentItem(0);
                    ((Button) v).setText("Other Apps");
                    flurryLogEvent("SEE_MANUAL");
                }
                break;

        }
    }

    public void flurryStart() {
        FlurryAgent.onStartSession(this, "C8D2KZXTH6HVJNPPKFV6");
        flurryLogEvent("APP_HANDHELD_STARTED");
    }

    public void flurryStop() {
        flurryLogEvent("APP_HANDHELD_STOPPED");

        FlurryAgent.onEndSession(this);
    }

    public void flurryLogEvent(String event) {
        log("Flurry event = " + event);

        FlurryAgent.logEvent(event);
    }

    void log(String msg) {
        //Log.d("HYDRATEME_H", msg);
    }

}
