package com.aeustech.hydrateme;

import android.content.ComponentName;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.ArrayList;
import java.util.List;

public class MobileService extends WearableListenerService {
    private GoogleApiClient mGoogleApiClient;

    private static String TAG = "HydrateMe_S";

    @Override
    public void onDestroy() {
        //Log.d(TAG, "onDestroy: service destroyed");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //Log.d(TAG, "onCreate: service created");

        //  Needed for communication between watch and device.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        //Log.d(TAG, "onConnected: " + connectionHint);
                    }
                    @Override
                    public void onConnectionSuspended(int cause) {
                        //Log.d(TAG, "onConnectionSuspended: " + cause);
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        //Log.d(TAG, "onConnectionFailed: " + result);
                    }
                })
                .addApi(Wearable.API)
                .build();

        mGoogleApiClient.connect();
    }

    /**
     * Here, the device actually receives the message that the phone sent, as a path.
     * We simply check that path's last segment and act accordingly.
     * @param messageEvent
     */
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {

        //Log.v(TAG, "msg rcvd = " + messageEvent.getPath());

        if(messageEvent.getPath().indexOf("goToUnlockPage") != -1) {
            //Log.d(TAG, "go to unlock page");
            ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 75);
            toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 1000);

            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            intent.setComponent(ComponentName.unflattenFromString("com.aeustech.hydrateme/com.aeustech.hydrateme.MobileMainActivity"));
            intent.addCategory("android.intent.category.LAUNCHER");
            intent.putExtra("product", "fullversion");
            startActivity(intent);

            Intent broadcastIntent = new Intent("com.aeustech.hydrateme.broadcast");
            broadcastIntent.putExtra("data", "goToUnlockPage/");
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        }
    }

    private List<Node> getNodes() {
        List<Node> nodes = new ArrayList<>();
        NodeApi.GetConnectedNodesResult rawNodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
        for (Node node : rawNodes.getNodes()) {
            nodes.add(node);
        }
        return nodes;
    }
}
