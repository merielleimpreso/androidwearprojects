package com.aeustech.wearrotarycalculator;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.wearable.view.DismissOverlayView;
import android.support.wearable.view.WatchViewStub;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

//WEAR
public class WearRotaryCalculator extends Activity implements MessageApi.MessageListener, GoogleApiClient.ConnectionCallbacks, View.OnLongClickListener{
    FrameLayout frmMain, functionsLayer, basicLayer, advancedLayer, inputLayer, toggleLayer, unlockLayer, tutorialLayer, errorLayer;
    TextView txtCurrentDigits, txtPreviousDigits, txtFunctionRecall, txtOperation;
    ImageView bg1, bg2, outerring1, outerring2, circleoverlay1, circleoverlay2, imgRad, imgDeg;
    ImageView imgBasicTutorial1, imgBasicTutorial2, imgBasicTutorial3, imgBasicTutorial4, imgAdvanceTutorial1;
    ImageButton btnMiddle, btnRad, btnDeg, btnC, btnUnlock, btnCheckPhone, btnMiddleTutorial1, btnEqual1, btnBasicTutorial5;
    ImageButton btnError1, btnError2, btnToggleToAdvance, btnToggleToBasic;
    SwipeGestureListener gestureListener;
    String digitsEntered = "";
    boolean solvedProblem = true, isCardUp = false, inDegMode = true, isRound = false;
    private DismissOverlayView mDismissOverlayView;

    GoogleApiClient mGoogleApiClient = null;
    Node node;
    MessageApi.MessageListener listener = null;
    private Node peerNode;
    SharedPreferences prefs;
    boolean unlockedAdvance = false;
    int basicTutorial = 1;
    boolean tutorialSeen = false;
    boolean advanceTutorialSeen = false;
    boolean errorDivByZero = false;

    String groupingSeparator = ",";
    String decimalSeparator = ",";

    public int screenHeight() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        log("screen height = " + metrics.heightPixels);
        return metrics.heightPixels;
    }

    public void adjustScreen(View view) {
        ImageButton btnError1 = (ImageButton) view.findViewById(R.id.btnError1);
        ImageButton btnError2 = (ImageButton) view.findViewById(R.id.btnError2);

        ImageView imgTutorialBasic1 = (ImageView) view.findViewById(R.id.imgTutorialBasic1);
        ImageView imgTutorialBasic5 = (ImageView) view.findViewById(R.id.btnTutorialBasic5);

        ImageView btnUnlock = (ImageView) view.findViewById(R.id.btnUnlock);
        ImageView btnCheckPhone = (ImageView) view.findViewById(R.id.btnCheckPhone);

        ImageView imgToggleToBasic = (ImageView) view.findViewById(R.id.imgToggleToBasic);
        ImageView imgToggleToAdvanced = (ImageView) view.findViewById(R.id.imgToggleToAdvanced);

        // samsung gear live, sony smartwatch 3, asus zenwatch
        if (screenHeight() == 320) {

            // lg g watch r
            if(isRound) {
                btnError1.setScaleX(1.15f);
                btnError1.setScaleY(1.15f);

                btnError2.setScaleX(1.15f);
                btnError2.setScaleY(1.15f);

                btnUnlock.setTranslationY(1);

            }

//            imgTutorialBasic1.setTranslationY(-15);

            // lg g watch
        } else if (screenHeight() == 280) {

//            btnError1.setTranslationY(-20);
            
            float scale = .8f;

            btnToggleToBasic.setScaleX(scale);
            btnToggleToBasic.setScaleY(scale);

            btnToggleToAdvance.setScaleX(scale);
            btnToggleToAdvance.setScaleY(scale);

            imgToggleToBasic.setScaleX(scale);
            imgToggleToBasic.setScaleY(scale);

            imgToggleToAdvanced.setScaleX(scale);
            imgToggleToAdvanced.setScaleY(scale);

            btnToggleToBasic.setTranslationY(-5);
            btnToggleToAdvance.setTranslationY(-5);

            imgToggleToBasic.setTranslationY(5);
            imgToggleToAdvanced.setTranslationY(-5);



            // moto 360
        } else {
            btnError1.setScaleX(1.15f);
            btnError1.setScaleY(1.15f);

            btnError2.setScaleX(1.15f);
            btnError2.setScaleY(1.15f);

//            imgTutorialBasic1.setTranslationY(-15);

            imgTutorialBasic5.setScaleX(1.15f);
            imgTutorialBasic5.setScaleY(1.15f);

            btnUnlock.setScaleX(1.15f);
            btnUnlock.setScaleY(1.15f);

            btnCheckPhone.setScaleX(1.15f);
            btnCheckPhone.setScaleY(1.15f);

//            imgToggleToBasic.setScaleX(1.25f);
//            imgToggleToBasic.setScaleY(1.25f);
//
//            imgToggleToAdvanced.setScaleX(1.25f);
//            imgToggleToAdvanced.setScaleY(1.25f);
//
//
//            btnToggleToBasic.setScaleX(1.25f);
//            btnToggleToBasic.setScaleY(1.25f);
//
//            btnToggleToAdvance.setScaleX(1.25f);
//            btnToggleToAdvance.setScaleY(1.25f);

            btnToggleToBasic.setTranslationY(-5);
            btnToggleToAdvance.setTranslationY(5);

            imgToggleToBasic.setTranslationY(-5);
            imgToggleToAdvanced.setTranslationY(5);



        }
    }


    @Override
    protected void onStart() {
        super.onStart();

        DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance();
        DecimalFormatSymbols symbols = format.getDecimalFormatSymbols();
        decimalSeparator = String.valueOf(symbols.getDecimalSeparator());

        log("debug - decimalSeparator = " + decimalSeparator);

        groupingSeparator = String.valueOf(symbols.getGroupingSeparator());
    }

        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Context context = this.getApplicationContext();
        final WearRotaryCalculator activity = this;
        mDismissOverlayView = new DismissOverlayView(this);

        prefs = this.getSharedPreferences("com.aeustech.wearrotarycalculator", Context.MODE_PRIVATE);
        unlockedAdvance = prefs.getBoolean("com.aeustech.wearrotarycalculator.advance", false);

        if (!unlockedAdvance) {
            advanceTutorialSeen = false;
            prefs.edit().putBoolean("com.aeustech.wearrotarycalculator.advanceTutorialSeen", false).apply();
        }

        // TEST ONLY
//        prefs.edit().putBoolean("com.aeustech.wearrotarycalculator.tutorialSeen", false).apply();


        WatchViewStub stub = new WatchViewStub(this);

        if(screenHeight() == 290) {
            stub.setRoundLayout(R.layout.round_activity_wear_rotary_calculator);
        } else {
            stub.setRoundLayout(R.layout.round_activity_wear_rotary_calculator_gwatchr);
        }

        if(screenHeight() == 280) {
            stub.setRectLayout(R.layout.rect_activity_wear_rotary_calculator_gwatch);
        } else {
            stub.setRectLayout(R.layout.rect_activity_wear_rotary_calculator);
        }

        log("--- on create");

//        addContentView(stub, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));

        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {

            @Override
            public void onLayoutInflated(WatchViewStub watchViewStub) {

                log("--- on layout inflated");

                // Detect layout shape
                frmMain = (FrameLayout) findViewById(R.id.frmMainRect);
                if (frmMain == null) {
                    log("round screen detected");
                    frmMain = (FrameLayout) findViewById(R.id.frmMainRound);
                    isRound = true;

                    ViewGroup parent = (ViewGroup) mDismissOverlayView.getParent();
                    if (parent != null) {
                        parent.removeView(mDismissOverlayView);
                    }
                    frmMain.addView(mDismissOverlayView, new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.MATCH_PARENT));
                } else {
                    log("rectangular screen detected");
                    frmMain.addView(mDismissOverlayView, new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.MATCH_PARENT));
                    isRound = false;

                }

                // Initiate views
                txtCurrentDigits = (TextView) findViewById(R.id.txtCurrentDigits);
                txtPreviousDigits = (TextView) findViewById(R.id.txtPreviousDigits);
                txtFunctionRecall = (TextView) findViewById(R.id.txtFunctionRecall);
                txtOperation = (TextView) findViewById(R.id.txtOperation);

                basicLayer = (FrameLayout) findViewById(R.id.basicLayer);
                advancedLayer = (FrameLayout) findViewById(R.id.advancedLayer);
                functionsLayer = (FrameLayout) findViewById(R.id.functionsLayer);
                inputLayer = (FrameLayout) findViewById(R.id.inputLayer);
                toggleLayer = (FrameLayout) findViewById(R.id.toggleLayer);
                unlockLayer = (FrameLayout) findViewById(R.id.unlockLayer);
                tutorialLayer = (FrameLayout) findViewById(R.id.tutorialLayer);
                errorLayer = (FrameLayout) findViewById(R.id.errorLayer);

                btnMiddle = (ImageButton) findViewById(R.id.btnMiddle);
                btnRad = (ImageButton) findViewById(R.id.btnRad);
                btnDeg = (ImageButton) findViewById(R.id.btnDeg);
                btnC = (ImageButton) findViewById(R.id.btnC);
                btnC.setOnLongClickListener(WearRotaryCalculator.this);
                btnUnlock = (ImageButton) findViewById(R.id.btnUnlock);
                btnCheckPhone = (ImageButton) findViewById(R.id.btnCheckPhone);
                btnMiddleTutorial1 = (ImageButton) findViewById(R.id.btnMiddleTutorial1);
                btnEqual1 = (ImageButton) findViewById(R.id.btnEqual1);
                btnBasicTutorial5 = (ImageButton) findViewById(R.id.btnTutorialBasic5);

                btnError1 = (ImageButton) findViewById(R.id.btnError1);
                btnError1.setVisibility(View.VISIBLE);

                btnError2 = (ImageButton) findViewById(R.id.btnError2);
                btnToggleToAdvance = (ImageButton) findViewById(R.id.btnToggleToAdvanced);
                btnToggleToBasic = (ImageButton) findViewById(R.id.btnToggleToBasic);

                bg1 = (ImageView) findViewById(R.id.bg1);
                bg2 = (ImageView) findViewById(R.id.bg2);
                outerring1 = (ImageView) findViewById(R.id.outerring1);
                outerring2 = (ImageView) findViewById(R.id.outerring2);
                circleoverlay1 = (ImageView) findViewById(R.id.circleoverlay1);
                circleoverlay2 = (ImageView) findViewById(R.id.circleoverlay2);
                imgRad = (ImageView) findViewById(R.id.imgRad);
                imgDeg = (ImageView) findViewById(R.id.imgDeg);
                imgBasicTutorial1 = (ImageView) findViewById(R.id.imgTutorialBasic1);
                imgBasicTutorial2 = (ImageView) findViewById(R.id.imgTutorialBasic2);
                imgBasicTutorial3 = (ImageView) findViewById(R.id.imgTutorialBasic3);
                imgBasicTutorial4 = (ImageView) findViewById(R.id.imgTutorialBasic4);
                imgAdvanceTutorial1 = (ImageView) findViewById(R.id.imgTutorialAdvance1);

                adjustScreen(frmMain);

                // Set listener for gestures
                gestureListener = new SwipeGestureListener(WearRotaryCalculator.this);
                btnMiddle.setOnTouchListener(gestureListener);

                tutorialSeen = prefs.getBoolean("com.aeustech.wearrotarycalculator.tutorialSeen", false);
                if (tutorialSeen) {
                    imgBasicTutorial1.setVisibility(View.GONE);
                    tutorialLayer.setVisibility(View.GONE);
                    basicTutorial = 100;
                }

               advanceTutorialSeen = prefs.getBoolean("com.aeustech.wearrotarycalculator.advanceTutorialSeen", false);

                // do not display advanced tutorial in onCreate because the screen may not be in advanced mode
//                if (unlockedAdvance && !advanceTutorialSeen) {
//                    tutorialLayer.setVisibility(View.VISIBLE);
//                    imgAdvanceTutorial1.setVisibility(View.VISIBLE);
//                }
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        log( "onConnectionFailed: " + result);
                    }
                })
                .addApi(Wearable.API)
                .build();

        mGoogleApiClient.connect();

        addContentView(stub, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));

        stub.inflate();

        }

    @Override
    public void onConnected(Bundle bundle) {
        log("connected to Google Play Services on Wear!");
        Wearable.MessageApi.addListener(mGoogleApiClient, this).setResultCallback(resultCallback);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private ResultCallback<Status> resultCallback =  new ResultCallback<Status>() {
        @Override
        public void onResult(Status status) {
            log("Status: " + status.getStatus().isSuccess());
            new AsyncTask<Void, Void, Void>(){
                @Override
                protected Void doInBackground(Void... params) {
//                    sendHandheldMessage();
                    return null;
                }
            }.execute();
        }
    };

    @Override
    public void onMessageReceived(final MessageEvent messageEvent) {
        log("handheld message: " + messageEvent.getPath());

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String handHeldMessage = messageEvent.getPath();
                String[] data = handHeldMessage.split("/");00

                if (data.length > 1) {

                    if (data[0].equalsIgnoreCase("complete")) {
                        unlockLayer.setVisibility(View.GONE);

                        log("finished buying");

                        prefs.edit().putBoolean("com.aeustech.wearrotarycalculator.advance", true).apply();

                        if (!advanceTutorialSeen) {
                            advanceTutorialSeen = true;
                            tutorialLayer.setVisibility(View.VISIBLE);
                            imgAdvanceTutorial1.setVisibility(View.VISIBLE);
                        }

                        if (advancedLayer.getVisibility() == View.INVISIBLE) {
                            animateButtonToggleToAdvance();
                        }
                    } else if (data[0].equalsIgnoreCase("autorestore")) {
                        // for autorestore, do not auto display tutorial, let the user discover it when they go to advanced mode
                        unlockLayer.setVisibility(View.GONE);
                        prefs.edit().putBoolean("com.aeustech.wearrotarycalculator.advance", true).apply();

                    } else if (data[0].equalsIgnoreCase("cancel")) {
                        btnUnlock.setVisibility(View.VISIBLE);
                        btnCheckPhone.setVisibility(View.GONE);
                        unlockLayer.setVisibility(View.VISIBLE);

                        log("cancelled buying");

                        prefs.edit().putBoolean("com.aeustech.wearrotarycalculator.advance", false).apply();
                    }
                }
            }
        });

        unlockedAdvance = prefs.getBoolean("com.aeustech.wearrotarycalculator.advance", false);
        if (unlockedAdvance) {
            unlockLayer.setVisibility(View.GONE);
        }
    }

    private void sendHandheldMessage(String message){

        log("send handheld message = " + message);

        NodeApi.GetConnectedNodesResult rawNodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();

        for (final Node node : rawNodes.getNodes()) {
            log("Node: " + node.getId());
            PendingResult<MessageApi.SendMessageResult> result = Wearable.MessageApi.sendMessage(
                    mGoogleApiClient,
                    node.getId(),
                    message,
                    null
            );

            result.setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                @Override
                public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                    //  The message is done sending.
                    //  This doesn't mean it worked, though.
                    log("Our callback is done.");
                    peerNode = node;    //  Save the node that worked so we don't have to loop again.
                }
            });
        }
    }


    public void log(String message) {
//        Log.d("WearRotaryCalculator_W", message);
    }

    public void buttonTutorialPressed(View v) {
        switch (v.getId()) {
            case R.id.btnTutorialBasic5:
                basicTutorial++;
                btnBasicTutorial5.setVisibility(View.GONE);
                tutorialLayer.setVisibility(View.GONE);
                // TEST ONLY
                prefs.edit().putBoolean("com.aeustech.wearrotarycalculator.tutorialSeen", true).apply();
                break;
        }
    }

    public void buttonPressed(View v) {
        final int MAXINPUT = 11;

        if (solvedProblem) {
            digitsEntered = "";
            solvedProblem = false;
            txtFunctionRecall.setText("");
        }

        if (txtCurrentDigits.getText().toString().equals("e") || txtCurrentDigits.getText().toString().equals("\u03C0")) {
            if (v.getId() == R.id.btnC) {
                if (digitsEntered.length() > 0) {
                    digitsEntered = digitsEntered.substring(0, digitsEntered.length()-1);
                }
                txtFunctionRecall.setText("");
                txtCurrentDigits.setText("");
            }
        } else {
            switch (v.getId()) {
                case R.id.btn0:
                    if (digitsEntered.length() < MAXINPUT)
                        digitsEntered += "0";
                    break;
                case R.id.btn1:
                    if (digitsEntered.length() < MAXINPUT)
                        digitsEntered += "1";
                    break;
                case R.id.btn2:
                    if (digitsEntered.length() < MAXINPUT)
                        digitsEntered += "2";
                    break;
                case R.id.btn3:
                    if (digitsEntered.length() < MAXINPUT)
                        digitsEntered += "3";
                    break;
                case R.id.btn4:
                    if (digitsEntered.length() < MAXINPUT)
                        digitsEntered += "4";
                    break;
                case R.id.btn5:
                    if (digitsEntered.length() < MAXINPUT)
                        digitsEntered += "5";
                    break;
                case R.id.btn6:
                    if (digitsEntered.length() < MAXINPUT)
                        digitsEntered += "6";
                    break;
                case R.id.btn7:
                    if (digitsEntered.length() < MAXINPUT)
                        digitsEntered += "7";
                    break;
                case R.id.btn8:
                    if (digitsEntered.length() < MAXINPUT)
                        digitsEntered += "8";
                    break;
                case R.id.btn9:
                    if (digitsEntered.length() < MAXINPUT)
                        digitsEntered += "9";
                    break;
                case R.id.btnDecimal:
                    if (digitsEntered.length() == 0) {
                        digitsEntered = "0" + decimalSeparator;
                    } else {
                        if (digitsEntered.length() < MAXINPUT)
//                            if (!digitsEntered.contains("."))
//                                digitsEntered += ".";
                            if (!digitsEntered.contains(decimalSeparator))
                                digitsEntered += decimalSeparator;
                    }
                    break;
                case R.id.btnC:
                    if (digitsEntered.length() > 0) {
                        digitsEntered = digitsEntered.substring(0, digitsEntered.length()-1);
                    } else {
                        if (txtOperation.getText().toString().length() > 0) {
                            txtOperation.setText("");
                            digitsEntered = txtPreviousDigits.getText().toString();
                            txtPreviousDigits.setText("");
                        }
                    }
                    txtFunctionRecall.setText("");
                    break;
                case R.id.btnUnlock:
                    btnUnlock.setVisibility(View.GONE);
                    btnCheckPhone.setVisibility(View.VISIBLE);

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            sendHandheldMessage("/phone/unlockadvance/");
                            return null;
                        }
                    }.execute();
                    break;
                case R.id.btnCheckPhone:
                    tutorialSeen = prefs.getBoolean("com.aeustech.wearrotarycalculator.tutorialSeen", false);
                    if (tutorialSeen) {
                        btnUnlock.setVisibility(View.VISIBLE);
                        btnCheckPhone.setVisibility(View.GONE);
                        unlockLayer.setVisibility(View.INVISIBLE);

                        animateButtonToggleToBasic();
                    }
                    break;
            }

            if (v.getId() != R.id.btnDecimal) {

                log("debug - digitsEntered = [" + digitsEntered + "]");

                if (digitsEntered.length() > 0) {
                    if (!digitsEntered.contains("E")) {
//                        digitsEntered = digitsEntered.replaceAll(",", "");

                        log("debug - groupingSeparator = " + groupingSeparator);

                        digitsEntered = digitsEntered.replace(groupingSeparator, "");

                        log("debug - digitsEntered = " + digitsEntered);

//                        String currentDigitsEntered = digitsEntered;
                        digitsEntered = decimalSeparator.equals(",") ? digitsEntered.replace(",",".") : digitsEntered;

                        BigDecimal number = new BigDecimal(digitsEntered);
                        DecimalFormat df = new DecimalFormat();

                        if (digitsEntered.contains(".")) {
                            if (digitsEntered.endsWith("0")) {
                                String[] splitString;

//                                if(decimalSeparator.equals(",")) {
//                                    splitString = digitsEntered.split(decimalSeparator);
//                                } else {
                                    splitString = digitsEntered.split("\\.");
//                                }

                                int n = splitString[1].length();
                                df.setMinimumFractionDigits(n);
                            } else {
                                df.setMinimumFractionDigits(0);
                            }
                            df.setMaximumFractionDigits(8);
                        }

                        /*if (digitsEntered.endsWith(".0")) {
                            df.setMinimumFractionDigits(1);
                        } else {
                            df.setMinimumFractionDigits(0);
                        }*/
                        digitsEntered = df.format(number);
                    }
                }
            }
            txtCurrentDigits.setText(digitsEntered);
        }

        if (v.getId()!=R.id.btnC && v.getId()!=R.id.btnUnlock && v.getId()!=R.id.btnCheckPhone && v.getId()!=R.id.btnDecimal) {
            if (basicTutorial == 1) {
                basicTutorial++;
                imgBasicTutorial1.setVisibility(View.GONE);

                btnMiddleTutorial1.setVisibility(View.VISIBLE);
                imgBasicTutorial2.setVisibility(View.VISIBLE);
            }
            if (basicTutorial == 3) {
                basicTutorial++;
                imgBasicTutorial3.setVisibility(View.GONE);

                imgBasicTutorial4.setVisibility(View.VISIBLE);
                btnEqual1.setVisibility(View.VISIBLE);
            }
        }

        checkCardPosition();
    }

    public void buttonTogglePressed(View v) {
        switch (v.getId()) {
            case R.id.btnToggleToAdvanced:
                log("toggle to advanced");
                tutorialSeen = prefs.getBoolean("com.aeustech.wearrotarycalculator.tutorialSeen", false);

                //--- TEST ONLY
//                tutorialSeen = true;
                //---

                if (tutorialSeen) {

                    animateButtonToggleToAdvance();
                    unlockedAdvance = prefs.getBoolean("com.aeustech.wearrotarycalculator.advance", false);

                    //--- TEST ONLY
//                    unlockedAdvance = true;
                    //---

                    if(!unlockedAdvance) {
                        unlockLayer.setVisibility(View.VISIBLE);
                    }

                    // show tutorial if not seen
                    if (unlockedAdvance && !advanceTutorialSeen) {
                        tutorialLayer.setVisibility(View.VISIBLE);
                        imgAdvanceTutorial1.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case R.id.btnToggleToBasic:
                animateButtonToggleToBasic();
                break;
            case R.id.btnPopupCancel:
                Animation fadeout = AnimationUtils.loadAnimation(this, R.anim.fadeout_1);
                fadeout.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        functionsLayer.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                functionsLayer.startAnimation(fadeout);
                break;
        }
        checkCardPosition();
    }

    public void buttonModePressed(View v) {
        switch (v.getId()) {
            case R.id.btnDeg:
                inDegMode = true;

                bg1.setVisibility(View.VISIBLE);
                outerring1.setVisibility(View.VISIBLE);
                circleoverlay1.setVisibility(View.VISIBLE);

                bg2.setVisibility(View.INVISIBLE);
                outerring2.setVisibility(View.INVISIBLE);
                circleoverlay2.setVisibility(View.INVISIBLE);

                imgDeg.setVisibility(View.INVISIBLE);
                imgRad.setVisibility(View.VISIBLE);

                break;
            case R.id.btnRad:
                inDegMode = false;

                bg2.setVisibility(View.VISIBLE);
                outerring2.setVisibility(View.VISIBLE);
                circleoverlay2.setVisibility(View.VISIBLE);

                bg1.setVisibility(View.INVISIBLE);
                outerring1.setVisibility(View.INVISIBLE);
                circleoverlay1.setVisibility(View.INVISIBLE);

                imgDeg.setVisibility(View.VISIBLE);
                imgRad.setVisibility(View.INVISIBLE);

                break;
        }

        checkCardPosition();
    }

    public void buttonOperationPressed(View v) {
        solvedProblem = false;
        String functionEntered = "";
        switch (v.getId()) {
            case R.id.btnAdd:
                functionEntered = "+";
                break;
            case R.id.btnSub:
                functionEntered = "-";
                break;
            case R.id.btnMul:
                functionEntered = "*";
                break;
            case R.id.btnDiv:
                functionEntered = "/";
                break;
            case R.id.btnRaise:
                if (digitsEntered.length() == 0) {
                    digitsEntered = "1";
                }
                functionEntered = "^";
                break;
        }

        if (v.getId() != R.id.btnRaise) {
            Animation fadeout = AnimationUtils.loadAnimation(this, R.anim.fadeout_1);
            fadeout.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    functionsLayer.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            functionsLayer.startAnimation(fadeout);
        }

        if (basicTutorial == 3) {
            imgBasicTutorial3.setVisibility(View.VISIBLE);
        }

        txtOperation.setText(functionEntered);
        txtPreviousDigits.setText(digitsEntered);
        txtFunctionRecall.setText("");

        digitsEntered = "";
        txtCurrentDigits.setText(digitsEntered);

        checkCardPosition();
    }

    public void buttonMiddlePressed(View v) {
        if (!isCardUp) {
            String previousDigits = txtPreviousDigits.getText().toString();
            String currentDigits = txtCurrentDigits.getText().toString();
            String operation = txtOperation.getText().toString();

//            previousDigits = previousDigits.replaceAll(",", "");
//            currentDigits = currentDigits.replaceAll(",", "");

            previousDigits = previousDigits.replace(groupingSeparator, "");
            currentDigits = currentDigits.replace(groupingSeparator, "");

            if (currentDigits.length() > 0) {
                if (operation.length() > 0) {
                    BigDecimal num1;
                    BigDecimal num2;

                    if (previousDigits.equals("e")) {
                        num1 = new BigDecimal(Math.E);
                    } else if (previousDigits.equals("\u03C0")) {
                        num1 = new BigDecimal(Math.PI);
                    } else if (previousDigits.length() == 0) {
                        if (operation.equals("+") || operation.equals("-")) {
                            num1 = new BigDecimal(0);
                        } else {
                            num1 = new BigDecimal(1);
                        }
                    } else {
                        num1 = new BigDecimal(previousDigits.replace(",", "."));
                    }
                    if (currentDigits.equals("e")) {
                        num2 = new BigDecimal(Math.E);
                    } else if (currentDigits.equals("\u03C0")) {
                        num2 = new BigDecimal(Math.PI);
                    } else if (currentDigits.length() == 0) {
                        if (operation.equals("+") || operation.equals("-")) {
                            num2 = new BigDecimal(0);
                        } else {
                            num2 = new BigDecimal(1);
                        }
                    } else {

                        currentDigits = decimalSeparator.equals(",") ? currentDigits.replace(",",".") : currentDigits;

                        num2 = new BigDecimal(currentDigits);
                    }

                    try {
                        BigDecimal answer = new BigDecimal(0);
                        if (operation.equals("+")) {
                            answer = num1.add(num2, MathContext.DECIMAL32);
                        } else if (operation.equals("-")) {
                            answer = num1.subtract(num2, MathContext.DECIMAL32);
                        } else if (operation.equals("*")) {
                            answer = num1.multiply(num2, MathContext.DECIMAL32);
                        } else if (operation.equals("/")) {
                            answer = num1.divide(num2, MathContext.DECIMAL32);
                        } else if (operation.equals("^")) {
                            answer = num1.pow(num2.intValue(), MathContext.DECIMAL32);
                        }

                        digitsEntered = answer.toString();

                        // display in scientific notation if needed
                        if(digitsEntered.length() > 9) {
                            DecimalFormat df = new DecimalFormat("0.00E00");
                            digitsEntered = df.format(answer.doubleValue());
                        } else {
                            DecimalFormat df = new DecimalFormat();
                            digitsEntered = df.format(answer.doubleValue());
                            df.setMinimumFractionDigits(0);
                        }
                        txtCurrentDigits.setText(digitsEntered);
                    } catch (Exception e) {
                        if (operation.equals("/")) {
                            if (txtCurrentDigits.getText().toString().equals("0")) {
                                errorDivByZero = true;
                            }
                        }

                        digitsEntered = "";
                        txtCurrentDigits.setText("ERROR");
                        errorLayer.setVisibility(View.VISIBLE);
                        btnError1.setVisibility(View.VISIBLE);


                    }

                    txtPreviousDigits.setText("");
                    txtOperation.setText("");
                    txtFunctionRecall.setText("");
                    solvedProblem = true;
                }

                if (v.getId() != R.id.btnEqual && v.getId() != R.id.btnEqual1 && !txtCurrentDigits.getText().toString().equals("ERROR")) {
                    Animation fadein = AnimationUtils.loadAnimation(this, R.anim.fadein_1);
                    fadein.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            functionsLayer.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    functionsLayer.startAnimation(fadein);
                }

                if (basicTutorial == 2) {
                    basicTutorial++;
                    btnMiddleTutorial1.setVisibility(View.GONE);
                    imgBasicTutorial2.setVisibility(View.GONE);
                }

                if (basicTutorial == 4) {
                    basicTutorial++;
                    btnEqual1.setVisibility(View.GONE);
                    imgBasicTutorial4.setVisibility(View.GONE);
                    btnBasicTutorial5.setVisibility(View.VISIBLE);

                }
            }
        }
        checkCardPosition();
    }

    public void buttonFunctionPressed(View v) {
        String currentDigits = txtCurrentDigits.getText().toString();
        String previousDigits = txtPreviousDigits.getText().toString();

//        currentDigits = currentDigits.replaceAll(",", "");
        currentDigits = currentDigits.replace(groupingSeparator, "");

        boolean error = false;

        if (currentDigits.equals("ERROR")) {
            currentDigits = "";
        }

        if (currentDigits.length() > 0) {
            double num;
            if (currentDigits.equals("e")) {
                num = Math.E;
            } else if (currentDigits.equals("\u03C0")) {
                num = Math.PI;
            } else {
                num = Double.parseDouble(currentDigits.replace(",", "."));
            }

            BigDecimal answer = new BigDecimal(0);
            String functionRecall = "";

            switch (v.getId()) {
                case R.id.btnSin:
                    if (inDegMode)
                        num = Math.toRadians(num);

                    try {
                        answer = new BigDecimal(Math.sin(num));
                        functionRecall = "sin(" + txtCurrentDigits.getText().toString() + ")=";
                    } catch (Exception e) {
                        error = true;
                    }
                    solvedProblem = true;
                    break;
                case R.id.btnCos:
                    if (inDegMode)
                        num = Math.toRadians(num);

                    try {
                        answer = new BigDecimal(Math.cos(num));
                        functionRecall = "cos(" + txtCurrentDigits.getText().toString() + ")=";
                    } catch (Exception e) {
                        error = true;
                    }
                    solvedProblem = true;
                    break;
                case R.id.btnTan:
                    if (inDegMode)
                        num =  Math.toRadians(num);
                    else
                        num = (num == 0.785) ? Math.PI/4 : num; // hardcoded solution for tan(pi/4)
                    try {

                        answer = new BigDecimal(Math.tan(num));
                        functionRecall = "tan(" + txtCurrentDigits.getText().toString() + ")=";
                     } catch (Exception e) {
                        error = true;
                    }
                    solvedProblem = true;

                    break;
                case R.id.btnLn:
                    if (inDegMode)
                        num = Math.toRadians(num);

                    try {
                        answer = new BigDecimal(Math.log(num));
                        functionRecall = "ln(" + txtCurrentDigits.getText().toString() + ")=";
                    } catch (Exception e) {
                        error = true;
                    }
                    solvedProblem = true;
                    break;
                case R.id.btnLog:
                    if (inDegMode)
                        num = Math.toRadians(num);
                    try {
                        answer = new BigDecimal(Math.log(num) / Math.log(10));
                        functionRecall = "log(" + txtCurrentDigits.getText().toString() + ")=";
                    } catch (Exception e) {
                        error = true;
                    }
                    solvedProblem = true;
                    break;
                case R.id.btnSquare:
                    try {
                        answer = new BigDecimal(num*num);
                        functionRecall = txtCurrentDigits.getText().toString() + "^2=";
                    } catch (Exception e) {
                        error = true;
                    }
                    solvedProblem = true;
                    break;
                case R.id.btnSqrt:
                    try {
                        answer = new BigDecimal(Math.sqrt(num));
                        functionRecall = "sqrt(" + txtCurrentDigits.getText().toString() + ")=";
                    } catch (Exception e) {
                        error = true;
                    }
                    solvedProblem = true;
                    break;
                case R.id.btnFactorial:
                    answer = BigDecimal.ONE;
                    BigDecimal n = new BigDecimal(num);

                    if (n.compareTo(BigDecimal.ZERO) == -1) {
                        error = true;
                    } else if (n.compareTo(new BigDecimal(100)) == 1) {
                        error = true;
                    } else {
                        while (n.compareTo(BigDecimal.ONE) == 1) { //while n > 1
                            try {
                                answer = answer.multiply(n);
                            } catch (Exception e) {
                                error = true;
                                n = BigDecimal.ONE;
                            }
                            if (!error) {
                                n = n.subtract(BigDecimal.ONE);
                            }
                        }
                        if (!error) {
                            functionRecall = txtCurrentDigits.getText().toString() + "!=";
                        }
                    }

                    solvedProblem = true;
                    break;
                case R.id.btnNeg:answer = new BigDecimal(num * -1f);
                    functionRecall = "";
                    break;
            }

            if (error) {
                digitsEntered = "";
                txtCurrentDigits.setText("ERROR");
                errorLayer.setVisibility(View.VISIBLE);
                btnError1.setVisibility(View.VISIBLE);
            } else {
                if (answer.compareTo(BigDecimal.ZERO) == 1) {
                    answer = (answer.doubleValue() < 0.00000001) ? new BigDecimal(0) : answer;
                } else {
                    answer = (answer.doubleValue() < -0.00000001 && answer.doubleValue() > -1) ? new BigDecimal(0) : answer;
                }

                answer = answer.multiply(BigDecimal.ONE, MathContext.DECIMAL32);
                digitsEntered = answer.toString();

                // display in scientific notation if needed
                if(digitsEntered.length() > 9) {
                    DecimalFormat df = new DecimalFormat("0.00E00");
                    digitsEntered = df.format(answer.doubleValue());
                } else {
                    DecimalFormat df = new DecimalFormat();
                    df.setMinimumFractionDigits(0);
                    digitsEntered = df.format(answer);
                }

                txtCurrentDigits.setText(digitsEntered);
                txtFunctionRecall.setText(functionRecall);
            }

            if (previousDigits.length() > 0 && !error) {
                txtPreviousDigits.setVisibility(View.INVISIBLE);

                Animation alphaout = AnimationUtils.loadAnimation(WearRotaryCalculator.this, R.anim.alphaout);
                txtFunctionRecall.animate().setStartDelay(1500);
                alphaout.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        txtPreviousDigits.setVisibility(View.VISIBLE);
                        txtFunctionRecall.setText("");
                        txtFunctionRecall.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                txtFunctionRecall.startAnimation(alphaout);
            }
        }
        checkCardPosition();
    }

    public void buttonConstantPressed(View v) {
        txtFunctionRecall.setText("");
        switch (v.getId()) {
            case R.id.btnE:
                txtCurrentDigits.setText("e");
                digitsEntered = "e";
                break;
            case R.id.btnPi:
                txtCurrentDigits.setText("\u03C0");
                digitsEntered = "\u03C0";
                break;
        }
    }

    public void buttonErrorPressed(View v) {
        switch (v.getId()) {
            case R.id.btnError1:
                if (errorDivByZero) {
                    btnError1.setVisibility(View.GONE);
                    btnError2.setVisibility(View.VISIBLE);
                    errorDivByZero = false;
                } else {
                    btnError1.setVisibility(View.GONE);
                    errorLayer.setVisibility(View.GONE);
                    txtCurrentDigits.setText("");
                }
                break;
            case R.id.btnError2:
                btnError2.setVisibility(View.GONE);
                errorLayer.setVisibility(View.GONE);
                txtCurrentDigits.setText("");
                break;
        }
    }

    void checkCardPosition() {
        if (isCardUp) {
            Animation swipeDown = AnimationUtils.loadAnimation(this, R.anim.swipedown);
            inputLayer.startAnimation(swipeDown);

            if (btnRad.getVisibility() == View.VISIBLE) {
                btnRad.setVisibility(View.INVISIBLE);
            }
            if (btnDeg.getVisibility() == View.VISIBLE) {
                btnDeg.setVisibility(View.INVISIBLE);
            }
            isCardUp = false;
        }
    }

    void animateButtonToggleToAdvance() {
        toggleLayer.setVisibility(View.VISIBLE);

        Animation fadeout = AnimationUtils.loadAnimation(this, R.anim.fadeout);
        fadeout.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                basicLayer.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        basicLayer.startAnimation(fadeout);

        Animation fadein = AnimationUtils.loadAnimation(this, R.anim.fadein);
        fadein.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                advancedLayer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        advancedLayer.startAnimation(fadein);

        Animation rotate = AnimationUtils.loadAnimation(this, R.anim.rotateout);
        rotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                btnToggleToAdvance.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //toggleLayer.setRotation(180.0f);
                btnToggleToBasic.setVisibility(View.VISIBLE);
                if(screenHeight() == 290) {
                    btnToggleToBasic.setTranslationY(5);
                }

                toggleLayer.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        toggleLayer.startAnimation(rotate);
    }

    void animateButtonToggleToBasic() {
        toggleLayer.setVisibility(View.VISIBLE);

        Animation fadeout = AnimationUtils.loadAnimation(this, R.anim.fadeout);
        fadeout.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                advancedLayer.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        advancedLayer.startAnimation(fadeout);

        Animation fadein = AnimationUtils.loadAnimation(this, R.anim.fadein);
        fadein.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                basicLayer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        basicLayer.startAnimation(fadein);

        Animation rotate = AnimationUtils.loadAnimation(this, R.anim.rotatein);
        rotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                btnToggleToBasic.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                toggleLayer.setVisibility(View.GONE);
                btnToggleToAdvance.setVisibility(View.VISIBLE);
                if(screenHeight() == 290) {
                    btnToggleToAdvance.setTranslationY(5);
                }


            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        toggleLayer.startAnimation(rotate);
    }

    @Override
    public boolean onLongClick(View v) {
        if (v.getId() == R.id.btnC) {
            digitsEntered = "";
            txtCurrentDigits.setText(digitsEntered);
            txtPreviousDigits.setText("");
            txtOperation.setText("");
            txtFunctionRecall.setText("");
        }
        checkCardPosition();
        return false;
    }

    class SwipeGestureListener extends GestureDetector.SimpleOnGestureListener implements View.OnTouchListener {
        Context context;
        GestureDetector gDetector;
        static final int SWIPE_MIN_DISTANCE = 15;
        static final int SWIPE_MAX_OFF_PATH = 80;
        static final int SWIPE_THRESHOLD_VELOCITY = 30;

        public SwipeGestureListener() {
            super();
        }

        public SwipeGestureListener(Context context) {
            this(context, null);
        }

        public SwipeGestureListener(Context context, GestureDetector gDetector) {

            if (gDetector == null)
                gDetector = new GestureDetector(context, this);

            this.context = context;
            this.gDetector = gDetector;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {

            // Swipe detection
            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) { // Vertical swipe
                if (Math.abs(e1.getX() - e2.getX()) > SWIPE_MAX_OFF_PATH
                        || Math.abs(velocityY) < SWIPE_THRESHOLD_VELOCITY) {
                    return false;
                }
                if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE) {
                    log("Swipe Up");

                    unlockedAdvance = prefs.getBoolean("com.aeustech.wearrotarycalculator.advance", false);

                    //--- TEST ONLY
//                    unlockedAdvance = true;
                    //---

                    if (unlockedAdvance) {

                        if (imgAdvanceTutorial1.getVisibility() == View.VISIBLE) {
                            imgAdvanceTutorial1.setVisibility(View.GONE);
                            tutorialLayer.setVisibility(View.GONE);
                            prefs.edit().putBoolean("com.aeustech.wearrotarycalculator.advanceTutorialSeen", true).apply();
                            advanceTutorialSeen = true;
                        }

                        Animation swipeUp = AnimationUtils.loadAnimation(WearRotaryCalculator.this, R.anim.swipeup);
                        swipeUp.setAnimationListener(new Animation.AnimationListener() {

                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                if (inDegMode == true) {
                                    btnRad.setVisibility(View.VISIBLE);
                                } else {
                                    btnDeg.setVisibility(View.VISIBLE);
                                }

                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        inputLayer.startAnimation(swipeUp);
                        isCardUp = true;
                    }


                } else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE) {
                    log("Swipe Down");
                    checkCardPosition();
                }
            } else { // Horizontal swipe
                if (Math.abs(velocityX) < SWIPE_THRESHOLD_VELOCITY) {
                    return false;
                }
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE) {
                    // Swipe Right
                } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE) {
                    // Swipe Left
                }
            }

            return super.onFling(e1, e2, velocityX, velocityY);

        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return gDetector.onTouchEvent(event);
        }

        public GestureDetector getDetector() {
            return gDetector;
        }

    }
}