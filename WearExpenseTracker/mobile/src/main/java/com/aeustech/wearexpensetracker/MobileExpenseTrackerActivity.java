package com.aeustech.wearexpensetracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


// TO DO: Replace values for store and wear connection
// Change names, etc


public class MobileExpenseTrackerActivity extends Activity {
    // Variable to access this class context
    Context context;

    // Variables for pages
    LayoutInflater inflater;
    ViewGroup viewGroupLayout;
    ExpenseTrackerPagerAdapter pagerAdapter;
    ViewPager pager;
    int currentPage = 0;
    Button visitButton, btnExitApp;

    // Variables used for watch-device communication
    private GoogleApiClient mGoogleApiClient;
    BroadcastReceiver resultReceiver;

    // Variables used for buying products
    Interface apiBindings;
    String product = "0";
    SharedPreferences prefs;
    ImageButton buyButton;
    TextView txtUnlockMessage;
    LinearLayout layoutBudgetUnlock, layoutTransactionsUnlock, layoutReviewUnlock, layoutStatisticsUnlock;


    public boolean boughtFromWatch = false;
    public boolean checkingPrevPurchases = false;
    public boolean consumeNC = false;
    public boolean prevPurchasesFound = false;
    boolean autorestrore = false;

    // For logging purposes
    final static String TAG = "WearExpenseTrackerMobileActivity";

    // Variables for database
    MobileExpenseTrackerDatabase database;
    final static String DEFAULT_REVIEW_CATEGORY = "TOTAL EXPENSES";

    // Common variable
    DateFormat dfDate, dfTime, dfDay, dfMonth, dfToday, dfMonthDate;
    int transactionMonth, transactionYear, nowMonth, nowYear, statsYear;

    // Variables for pie charts
    String[] categories = new String[8];
    int[] colors = new int[8];

    GraphicalView chartView;
    XYMultipleSeriesRenderer multiRenderer;
    XYMultipleSeriesDataset dataset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile_layout);

        context = this;
        prefs = this.getSharedPreferences("com.aeustech.wearexpensetracker", Context.MODE_PRIVATE);
        database = new MobileExpenseTrackerDatabase(context);

        initializeFormatters();
        initializeValuesForCharts();
        initializePagerAdapter();
        initializeAPI();
    }

    private void initializeFormatters() {
        dfDate = new SimpleDateFormat("yyyy-MM-dd");
        dfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        dfDay = new SimpleDateFormat("dd EEE");
        dfMonth = new SimpleDateFormat("MMM yyyy");
        dfToday = new SimpleDateFormat("dd MMM yyyy, EEE");
        dfMonthDate = new SimpleDateFormat("MMM dd");

        Calendar cal = Calendar.getInstance();
        transactionYear = cal.get(Calendar.YEAR);
        transactionMonth = cal.get(Calendar.MONTH);
        nowYear = cal.get(Calendar.YEAR);
        nowMonth = cal.get(Calendar.MONTH);
        statsYear = cal.get(Calendar.YEAR);
    }

    private void initializeValuesForCharts() {
        categories = new String[] { MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_PERSONAL,
                MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_HOUSE,
                MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_FOOD,
                MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_TRANSPORTATION,
                MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_CLOTHING,
                MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_FUN,
                MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_FAMILY,
                MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_MISCELLANEOUS
        };

        colors = new int[] { Color.parseColor("#6CA7C0"),
                Color.parseColor("#BAB124"),
                Color.parseColor("#42CF86"),
                Color.parseColor("#F8504D"),
                Color.parseColor("#833AA7"),
                Color.parseColor("#D853AE"),
                Color.parseColor("#F7931E"),
                Color.parseColor("#618246"),
        };
    }

    private void initializePagerAdapter() {
        // Instantiate a ViewPager and a PagerAdapter
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewGroupLayout = (ViewGroup) findViewById(android.R.id.content);
        visitButton = (Button) viewGroupLayout.findViewById(R.id.btn_otherapps);
        btnExitApp = (Button) viewGroupLayout.findViewById(R.id.btnExitApp);

        pagerAdapter = new ExpenseTrackerPagerAdapter();
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);
        pagerAdapter.preloadPacks();

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

                if(currentPage > pagerAdapter.getCount()-1) {
                    visitButton.setText("How To Use");
                } else {
                    visitButton.setText("Other Apps");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initializeAPI() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            product = extras.getString("product");
        }

        // Needed for communication between watch and device.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        log("onConnected: " + connectionHint);
                    }
                    @Override
                    public void onConnectionSuspended(int cause) {
                        log("onConnectionSuspended: " + cause);
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        log("onConnectionFailed: " + result);
                    }
                })
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();

        resultReceiver = initializeBroadcastReceiver();
        LocalBroadcastManager.getInstance(context).registerReceiver(resultReceiver, new IntentFilter("com.aeustech.wearexpensetracker.broadcast"));

        apiBindings = new Interface();
        apiBindings.context = this;
        apiBindings.gplayInitIAB();

    }

    private BroadcastReceiver initializeBroadcastReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String[] data = intent.getStringExtra("data").split("/");

                if (data[0].equals("goToInAppPage")) {
                    showBuyPopup();
                } else {
                    if (data[0].equals("budget")) {
                        if (data[1].equals("complete")) {
                            pager.setCurrentItem(3);
                            Toast.makeText(MobileExpenseTrackerActivity.this, "Budgets are synced", Toast.LENGTH_SHORT).show();
                        }
                    } else if (data[0].equals("sync")) {
                        if (data[1].equals("complete")) {
                            pager.setCurrentItem(1);
                            Toast.makeText(MobileExpenseTrackerActivity.this, "Expenses are synced", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        // Update database
                        String string = intent.getStringExtra("data");
                        try {
                            JSONArray array = new JSONArray(string);
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                int id = object.getInt(MobileExpenseTrackerDatabase.EXPENSE_ID);
                                String date = object.getString(MobileExpenseTrackerDatabase.EXPENSE_DATE);
                                String datetime = object.getString(MobileExpenseTrackerDatabase.EXPENSE_DATETIME);
                                String category = object.getString(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY);
                                float amount = (float) object.getDouble(MobileExpenseTrackerDatabase.EXPENSE_AMOUNT);
                                int deleted = object.getInt(MobileExpenseTrackerDatabase.EXPENSE_DELETED);
                                Expense expense = new Expense(id, date, datetime, category, amount);
                                String whereClause = MobileExpenseTrackerDatabase.EXPENSE_ID + "='" + id + "'";

                                database.open();
                                ArrayList<Expense> arrayList = database.getExpenses(whereClause);

                                if (arrayList.size() > 0) {
                                    log(id + "");
                                    if (deleted == 1) {
                                        database.deleteExpense(expense);
                                    } else {
                                        database.updateExpense(expense);
                                    }
                                } else {
                                    if (deleted == 0) {
                                        database.insertExpense(expense);
                                    }
                                }
                                database.close();
                            }
                            updateAllViewsOnDataChanged(0);
                            tellWatchConnectedState("sync/finish", "");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                log("broadcast received = " + intent.getStringExtra("data"));

            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        flurryStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        flurryStop();

        System.runFinalization();
        System.exit(0);
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (resultReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(resultReceiver);
        }
        apiBindings.gplayDestroyIAB();
    }


    /* --- FUNCTIONS FOR IN-APP --- */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(apiBindings.iabHelper == null) {
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }

        if (!apiBindings.iabHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void showBuyPopup() {
        if(pagerAdapter.views.size() > 0) {
            pager.setCurrentItem(5);

            boughtFromWatch = true;

            if(prevPurchasesFound) {
                showAlert("You already own this item.\nPlease press the Restore button to unlock full version.");
            }

            flurryLogEvent("SHOW_BUY_CONFIRM_POPUP");
        }
    }

    public void finishedBuy() {
        if(!checkingPrevPurchases) {
            log("buy/restore completed");
            tellWatchConnectedState("complete/fullversion/", "");
        } else {
            log("autorestore completed");
            tellWatchConnectedState("autorestore/fullversion/", "");
        }

        buyButton.setVisibility(View.GONE);
        txtUnlockMessage.setVisibility(View.VISIBLE);

        layoutTransactionsUnlock.setVisibility(View.GONE);
        layoutReviewUnlock.setVisibility(View.GONE);
        layoutBudgetUnlock.setVisibility(View.GONE);
        layoutStatisticsUnlock.setVisibility(View.GONE);

        prefs.edit().putBoolean("com.aeustech.wearexpensetracker.fullversion", true).apply();

        if(!checkingPrevPurchases) {
            showAlert("Full version has been unlocked.\nPlease check your watch.");
        }
    }

    public void cancelBuy() {
        tellWatchConnectedState("cancel/fullversion/", "");
    }

    public void showAlert(String message) {
        final String msg = message;
        new AlertDialog.Builder(context)
                .setTitle("Wear Expense Manager")
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                }).show();
    }


    /* --- FUNCTIONS FOR HANDHELD-WEAR COMMUNICATION --- */

    private void tellWatchConnectedState(final String message, final String data){
        new AsyncTask<Void, Void, List<Node>>(){

            @Override
            protected List<Node> doInBackground(Void... params) {
                return getNodes();
            }

            @Override
            protected void onPostExecute(List<Node> nodeList) {
                for(Node node : nodeList) {
                    log("telling " + node.getId() + " " + message);

                    PendingResult<MessageApi.SendMessageResult> result = Wearable.MessageApi.sendMessage(
                            mGoogleApiClient,
                            node.getId(),
                            message,
                            data.getBytes());

                    result.setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                            Log.v(TAG, "Phone: " + sendMessageResult.getStatus().getStatusMessage());
                        }
                    });
                    break;
                }
            }
        }.execute();
    }

    private List<Node> getNodes() {
        List<Node> nodes = new ArrayList<Node>();
        NodeApi.GetConnectedNodesResult rawNodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
        for (Node node : rawNodes.getNodes()) {
            nodes.add(node);
            break;
        }
        return nodes;
    }


    /* --- FUNCTIONS FOR UI DISPLAY --- */

    private class ExpenseTrackerPagerAdapter extends PagerAdapter {
        ArrayList<View> views = new ArrayList<View>();

        // Views for Transactions
        TextView txtMonth, txtNoTransaction;
        ImageButton btnNextMonth, btnPrevMonth, btnSyncFromWear;
        ExpandableListView listTransactions;

        // Views for Reviews
        TextView txtReviewTotal, txtReviewMonth;
        ImageButton btnReviewNextMonth;
        ExpandableListView listReviews;

        // Views for Budget
        EditText etBudgetAmount;
        FrameLayout frmBudgetShow, frmBudgetEdit;
        ImageButton btnSyncBudgets;
        Spinner spinCategory, spinPeriod;
        ArrayAdapter<CharSequence> spinCategoryAdapter, spinPeriodAdapter;
        ExpandableListView listBudgets;

        // Views for Statistics
        FrameLayout frmMonthlyStatistics;
        ImageButton btnStatsNext, btnStatsPrev;
        TextView txtStatsYear, txtLoading;
        Spinner spinStatsCategory;
        ArrayAdapter<CharSequence> spinStatsCategoryAdapter;

        public void preloadPacks() {
            views.clear();

            views.add((View) inflater.inflate(R.layout.mobile_howto, null));
            views.add((View) inflater.inflate(R.layout.mobile_transactions, null));
            views.add((View) inflater.inflate(R.layout.mobile_review, null));
            views.add((View) inflater.inflate(R.layout.mobile_budget, null));
            views.add((View) inflater.inflate(R.layout.mobile_statistics, null));
            views.add((View) inflater.inflate(R.layout.mobile_inapp, null));
            views.add((View) inflater.inflate(R.layout.mobile_layout_otherapps, null));

            buyButton = (ImageButton) views.get(5).findViewById(R.id.btn_buy);
            txtUnlockMessage = (TextView) views.get(5).findViewById(R.id.txtUnlockMessage);

            boolean isFullVersion = prefs.getBoolean("com.aeustech.wearexpensetracker.fullversion", false);
            layoutTransactionsUnlock = (LinearLayout) views.get(1).findViewById(R.id.layoutTransactionsUnlock);
            layoutReviewUnlock = (LinearLayout) views.get(2).findViewById(R.id.layoutReviewUnlock);
            layoutBudgetUnlock = (LinearLayout) views.get(3).findViewById(R.id.layoutBudgetUnlock);
            layoutStatisticsUnlock = (LinearLayout) views.get(4).findViewById(R.id.layoutStatisticsUnlock);

            if (isFullVersion) {
                txtUnlockMessage.setVisibility(View.VISIBLE);
                buyButton.setVisibility(View.GONE);

                layoutTransactionsUnlock.setVisibility(View.GONE);
                layoutReviewUnlock.setVisibility(View.GONE);
                layoutBudgetUnlock.setVisibility(View.GONE);
                layoutStatisticsUnlock.setVisibility(View.GONE);
            }
        }

        @Override
        public int getCount() {
            return 7;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view==object);
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            View v = views.get(position);
            collection.addView(v, 0);


            // Get position (xml pages)
            switch (position) {

                case 1:
                    btnNextMonth = (ImageButton) findViewById(R.id.btnNextMonth);
                    btnPrevMonth = (ImageButton) findViewById(R.id.btnPrevMonth);
                    btnSyncFromWear = (ImageButton) findViewById(R.id.btn_sync);

                    txtMonth = (TextView) findViewById(R.id.txtMonth);
                    txtNoTransaction = (TextView) findViewById(R.id.txtNoTransaction);
                    listTransactions = (ExpandableListView) findViewById(R.id.listTransaction);

                    updateListTransactions();
                    updateMonthText(txtMonth, btnNextMonth);

                    break;

                case 2:
                    btnReviewNextMonth = (ImageButton) findViewById(R.id.btnReviewNextMonth);
                    txtReviewMonth = (TextView) findViewById(R.id.txtReviewMonth);
                    txtReviewTotal = (TextView) findViewById(R.id.txtReviewTotal);
                    listReviews = (ExpandableListView) findViewById(R.id.listReview);

                    updateListReviews();
                    updateMonthText(txtReviewMonth, btnReviewNextMonth);

                    break;

                case 3:
                    btnSyncBudgets = (ImageButton) findViewById(R.id.btnBudgetToWear);

                    etBudgetAmount = (EditText) findViewById(R.id.etBudgetAmount);
                    etBudgetAmount.addTextChangedListener(new CurrencyTextWatcher());

                    frmBudgetShow = (FrameLayout) findViewById(R.id.frmBudgetShow);
                    frmBudgetEdit = (FrameLayout) findViewById(R.id.frmBudgetEdit);

                    spinCategory = (Spinner) findViewById(R.id.spinCategory);
                    spinCategoryAdapter = ArrayAdapter.createFromResource(MobileExpenseTrackerActivity.this.context, R.array.categories, R.layout.simple_spinner_item);
                    spinCategoryAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    spinCategory.setAdapter(spinCategoryAdapter);

                    spinPeriod = (Spinner) findViewById(R.id.spinPeriod);
                    spinPeriodAdapter = ArrayAdapter.createFromResource(MobileExpenseTrackerActivity.this.context, R.array.periods, R.layout.simple_spinner_item);
                    spinPeriodAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item_gold);
                    spinPeriod.setAdapter(spinPeriodAdapter);

                    listBudgets = (ExpandableListView) findViewById(R.id.listBudget);
                    updateListBudgets();

                    break;

                case 4:
                    frmMonthlyStatistics = (FrameLayout) findViewById(R.id.frameLayoutStatisticsMonthly);

                    spinStatsCategory = (Spinner) findViewById(R.id.spinStatsCategory);
                    spinStatsCategoryAdapter = ArrayAdapter.createFromResource(MobileExpenseTrackerActivity.this.context, R.array.statsCategories, R.layout.simple_spinner_item);
                    spinStatsCategoryAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item_pink);
                    spinStatsCategory.setAdapter(spinStatsCategoryAdapter);
                    spinStatsCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            String category = spinStatsCategory.getSelectedItem().toString();
                            drawYearlyGraph(0, category);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    btnStatsNext = (ImageButton) findViewById(R.id.btnStatsNext);
                    btnStatsPrev = (ImageButton) findViewById(R.id.btnStatsPrev);
                    txtStatsYear = (TextView) findViewById(R.id.txtStatsYear);
                    txtLoading = (TextView) findViewById(R.id.txtLoading);
                    txtLoading.setAlpha(70f);
                    drawYearlyGraph(0, DEFAULT_REVIEW_CATEGORY);

                    break;
            }
            return v;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }
    }

    public void updateListTransactions() {
        pagerAdapter.btnPrevMonth.setClickable(false);
        pagerAdapter.btnNextMonth.setClickable(false);
        pagerAdapter.btnSyncFromWear.setClickable(false);

        new AsyncTask<Void, Void, Void>() {
            SparseArray<Group> groups;

            @Override
            protected Void doInBackground(Void... params) {
                groups = groupTransactionByDate(transactionMonth, transactionYear);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                MyExpandableListAdapter adapter = new MyExpandableListAdapter(MobileExpenseTrackerActivity.this, groups);
                pagerAdapter.listTransactions.setAdapter(adapter);

                pagerAdapter.btnPrevMonth.setClickable(true);
                pagerAdapter.btnNextMonth.setClickable(true);
                pagerAdapter.btnSyncFromWear.setClickable(true);

                if (groups.size() == 0) {
                    pagerAdapter.txtNoTransaction.setVisibility(View.VISIBLE);
                } else {
                    pagerAdapter.txtNoTransaction.setVisibility(View.GONE);
                }
            }
        }.execute();
    }

    public void updateListReviews() {

        new AsyncTask<Void, Void, Void>() {
            SparseArray<GroupReview> groups;
            float totalThisMonth = 0;

            @Override
            protected Void doInBackground(Void... params) {
                groups = new SparseArray<GroupReview>();
                ArrayList<Expense> expenses = getMonthlyExpenses(transactionYear, transactionMonth);

                for (Expense e : expenses) {
                    totalThisMonth += e.amount;
                }

                for (int i = 0; i < categories.length; i++) {
                    float totalForCategory = 0;
                    for (Expense e : expenses) {
                        if (e.category.equals(categories[i])) {
                            totalForCategory += e.amount;
                        }
                    }
                    int percentage = (int) Math.round((totalForCategory / totalThisMonth) * 100);
                    groups.append(i, new GroupReview(categories[i], totalForCategory, percentage));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                MyExpandableListAdapterReview adapter = new MyExpandableListAdapterReview(MobileExpenseTrackerActivity.this, groups);
                pagerAdapter.listReviews.setAdapter(adapter);
                pagerAdapter.txtReviewTotal.setText(formatDecimal(totalThisMonth));

            }
        }.execute();
    }

    public void updateListBudgets() {
        pagerAdapter.btnSyncBudgets.setClickable(false);

        new AsyncTask<Void, Void, Void>() {
            SparseArray<Budget> budgets = new SparseArray<Budget>();

            @Override
            protected Void doInBackground(Void... params) {
                for(int i = 0; i < categories.length; i++) {
                    Budget budget = getBudgetActivated(categories[i]);
                    if (budget == null) {
                        budget = new Budget(-1, categories[i], "", "", "", 0);
                    }
                    budgets.append(i, budget);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                MyExpandableListAdapterBudget adapter = new MyExpandableListAdapterBudget(MobileExpenseTrackerActivity.this, budgets);
                pagerAdapter.listBudgets.setAdapter(adapter);
                pagerAdapter.btnSyncBudgets.setClickable(true);
            }
        }.execute();
    }


    public void updateMonthText(TextView txtMonth, ImageButton btnNextMonth) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, transactionMonth);
        cal.set(Calendar.YEAR, transactionYear);
        String strMonth = dfMonth.format(cal.getTime());
        txtMonth.setText(strMonth.toUpperCase());

        if (nowMonth == transactionMonth && nowYear == transactionYear) {
            btnNextMonth.setClickable(false);
            btnNextMonth.setVisibility(View.INVISIBLE);
        } else {
            btnNextMonth.setClickable(true);
            btnNextMonth.setVisibility(View.VISIBLE);
        }
    }

    public void drawYearlyGraph(int add, final String category) {
        pagerAdapter.spinStatsCategory.setClickable(false);
        pagerAdapter.btnStatsNext.setClickable(false);
        pagerAdapter.btnStatsPrev.setClickable(false);
        pagerAdapter.frmMonthlyStatistics.removeAllViews();
        pagerAdapter.txtLoading.setVisibility(View.VISIBLE);

        statsYear += add;
        pagerAdapter.txtStatsYear.setText(Integer.toString(statsYear));

        if (statsYear == nowYear) {
            pagerAdapter.btnStatsNext.setClickable(false);
            pagerAdapter.btnStatsNext.setVisibility(View.INVISIBLE);
        } else {
            pagerAdapter.btnStatsNext.setClickable(true);
            pagerAdapter.btnStatsNext.setVisibility(View.VISIBLE);

        }

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                String[] months = new String[12];
                months[0] = "Jan";
                months[1] = "Feb";
                months[2] = "Mar";
                months[3] = "Apr";
                months[4] = "May";
                months[5] = "Jun";
                months[6] = "Jul";
                months[7] = "Aug";
                months[8] = "Sep";
                months[9] = "Oct";
                months[10] = "Nov";
                months[11] = "Dec";

                double [] expenses = new double[12];
                for (int i=0; i<expenses.length; i++) {
                    float total = 0;
                    ArrayList<Expense> arrayList = getMonthlyExpenses(statsYear, i);
                    for (Expense e : arrayList) {
                        if (category.equals(DEFAULT_REVIEW_CATEGORY)) {
                            total += e.amount;
                        } else {
                            if (e.category.equals(category)) {
                                total += e.amount;
                            }
                        }
                    }
                    expenses[i] = total;
                }

                double maxExpenses = 0;
                for (int i=0; i<expenses.length; i++) {
                    if (expenses[i] > maxExpenses) {
                        maxExpenses = expenses[i];
                    }
                }

                // Creating an XYSeries
                XYSeries series = new XYSeries("");
                for (int i=0; i<expenses.length; i++) {
                    series.add(i, expenses[i]);
                }

                // Creating a dataset
                dataset = new XYMultipleSeriesDataset();
                dataset.addSeries(series);

                // Creating renderer to customize
                XYSeriesRenderer renderer = new XYSeriesRenderer();
                renderer.setColor(Color.parseColor("#EF484F"));
                renderer.setLineWidth(2);
                renderer.setFillPoints(true);
                renderer.setDisplayChartValues(true);
                renderer.setDisplayChartValuesDistance(10);
                renderer.setChartValuesTextAlign(Paint.Align.CENTER);

                DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance();
                format.setMaximumFractionDigits(2);
                format.setMinimumFractionDigits(2);
                renderer.setChartValuesFormat(format);

                multiRenderer = new XYMultipleSeriesRenderer();
                multiRenderer.setOrientation(XYMultipleSeriesRenderer.Orientation.HORIZONTAL);
                multiRenderer.setXLabels(0);

                /* Customizing graphs */
                multiRenderer.setChartTitleTextSize(28);
                multiRenderer.setAxisTitleTextSize(12);
                multiRenderer.setLabelsTextSize(20);
                multiRenderer.setZoomButtonsVisible(false);
                multiRenderer.setPanEnabled(false, false);
                multiRenderer.setClickEnabled(false);
                multiRenderer.setZoomEnabled(false, false);
                multiRenderer.setShowGridY(false);
                multiRenderer.setShowGridX(false);
                multiRenderer.setFitLegend(false);
                multiRenderer.setShowGrid(false);
                multiRenderer.setZoomEnabled(false);
                multiRenderer.setExternalZoomEnabled(false);
                multiRenderer.setAntialiasing(true);
                multiRenderer.setShowLegend(false);
                multiRenderer.setInScroll(false);
                multiRenderer.setBarSpacing(0.5);
                multiRenderer.setLegendHeight(30);
                multiRenderer.setXLabelsAlign(Paint.Align.CENTER);
                multiRenderer.setYLabelsAlign(Paint.Align.LEFT);
                multiRenderer.setTextTypeface("sans_serif", Typeface.NORMAL);
                multiRenderer.setYLabels(10);
                multiRenderer.setXAxisMin(-0.5);
                multiRenderer.setMargins(new int[]{30, 30, 30, 30});
                multiRenderer.setYAxisMax(maxExpenses+(maxExpenses/16));
                multiRenderer.setMarginsColor(Color.parseColor("#FFFFFF"));

                for(int i=0; i<expenses.length;i++){
                    multiRenderer.addXTextLabel(i, months[i]);
                }
                multiRenderer.addSeriesRenderer(renderer);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                chartView = ChartFactory.getBarChartView(context, dataset, multiRenderer, BarChart.Type.DEFAULT);
                pagerAdapter.spinStatsCategory.setClickable(true);
                pagerAdapter.btnStatsNext.setClickable(true);
                pagerAdapter.btnStatsPrev.setClickable(true);
                pagerAdapter.frmMonthlyStatistics.addView(chartView, new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT,
                        FrameLayout.LayoutParams.MATCH_PARENT));
                pagerAdapter.txtLoading.setVisibility(View.INVISIBLE);
            }
        }.execute();
    }

    public void updateAllViewsOnDataChanged(int monthAdd) {
        // Set add = 1, previous month
        // Set add = 0, when month is not changed
        // Set add = 1, next month
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, transactionMonth);
        cal.set(Calendar.YEAR, transactionYear);
        cal.add(Calendar.MONTH, monthAdd);

        transactionMonth = cal.get(Calendar.MONTH);
        transactionYear = cal.get(Calendar.YEAR);

        updateListTransactions();
        updateMonthText(pagerAdapter.txtReviewMonth, pagerAdapter.btnReviewNextMonth);

        updateListReviews();
        updateMonthText(pagerAdapter.txtMonth, pagerAdapter.btnNextMonth);
    }


    /* --- FUNCTIONS FOR BUTTONS --- */

    public void editBudget(Budget budget) {
        pagerAdapter.frmBudgetShow.setVisibility(View.GONE);
        pagerAdapter.frmBudgetEdit.setVisibility(View.VISIBLE);

        String category = "", period = "", amount = "";

        category = budget.category;
        if (budget.id < 0) {
            period = "Daily";
            amount = "0.00";
        } else {
            period = budget.period;
            amount = formatDecimal(budget.amount);
        }
        pagerAdapter.spinCategory.setSelection(pagerAdapter.spinCategoryAdapter.getPosition(category));
        pagerAdapter.spinPeriod.setSelection(pagerAdapter.spinPeriodAdapter.getPosition(period));
        pagerAdapter.etBudgetAmount.setText(amount);
    }

    public void buttonClicked(View v) {

        Intent appStoreIntent;

        switch (v.getId()) {

            case R.id.btn_wtc:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.weartipcalculator"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_WIT_PLAY_STORE");
                break;

            case R.id.btn_hm:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.aeustech.hydrateme"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_HM_PLAY_STORE");
                break;

            case R.id.btn_wrc:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.wearrotarycalculator"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_WIT_PLAY_STORE");
                break;

            case R.id.btn_htc:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.hitthecan"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_HTC_PLAY_STORE");
                break;

            case R.id.btn_wit:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.wearintervaltimer"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_WIT_PLAY_STORE");
                break;

            case R.id.btn_cm:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.colormatch"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_CM_PLAY_STORE");
                break;

            case R.id.btn_omgwe:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.ohmygravitywear"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_OMGW_PLAY_STORE");
                break;

            case R.id.btn_mb:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.matchblox"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_MB_PLAY_STORE");
                break;

            case R.id.btn_omg:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.ohmygravity"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_OMGH_PLAY_STORE");
                break;

            case R.id.btn_shapopo:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.shapopo"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_SPP_PLAY_STORE");
                break;

            case R.id.btn_rate:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.wearexpensetracker"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);
                flurryLogEvent("LAUNCH_HM_PLAY_STORE");

                break;

            case R.id.btn_buy:
                checkingPrevPurchases = false;
                log("buy full version");
                showBuyPopup();

                boughtFromWatch = true;
                checkingPrevPurchases = false;
                apiBindings.gplayIABBuyItem("fullversion");

                break;

            case R.id.btn_restore:
                checkingPrevPurchases = false;
                apiBindings.gplayRestorePurchase();

                break;

            case R.id.btn_otherapps:
                if (pager.getCurrentItem() < pagerAdapter.getCount()-1) {
                    pager.setCurrentItem(pagerAdapter.getCount()-1);
                    ((Button) v).setText("How To Use");

                    flurryLogEvent("SEE_OTHER_APPS");
                } else {
                    pager.setCurrentItem(0);
                    ((Button) v).setText("Other Apps");
                    flurryLogEvent("SEE_MANUAL");
                }
                break;

            case R.id.btn_sync:
                boolean firstInstall = prefs.getBoolean("com.aeustech.wearexpensetracker.firstInstall", true);

                if (firstInstall) {
                    prefs.edit().putBoolean("com.aeustech.wearexpensetracker.firstInstall", false).apply();
                    btnExitApp.setVisibility(View.VISIBLE);
                } else {
                    tellWatchConnectedState("sync/database", "");
                }

                break;

            case R.id.btnExitApp:
                moveTaskToBack(true);
                finish();

                break;

            case R.id.btnBudgetOk:
                String category = pagerAdapter.spinCategory.getSelectedItem().toString();
                updateBudget(category, false);
                updateListBudgets();

                pager.setCurrentItem(3);
                pagerAdapter.etBudgetAmount.setText("");
                pagerAdapter.spinPeriod.setSelection(pagerAdapter.spinPeriodAdapter.getPosition("Daily"));
                pagerAdapter.frmBudgetShow.setVisibility(View.VISIBLE);
                pagerAdapter.frmBudgetEdit.setVisibility(View.GONE);
                break;

            case R.id.btnBudgetNew:
                category = pagerAdapter.spinCategory.getSelectedItem().toString();
                updateBudget(category, true);
                updateListBudgets();

                pager.setCurrentItem(3);
                pagerAdapter.etBudgetAmount.setText("");
                pagerAdapter.spinPeriod.setSelection(pagerAdapter.spinPeriodAdapter.getPosition("Daily"));
                pagerAdapter.frmBudgetShow.setVisibility(View.VISIBLE);
                pagerAdapter.frmBudgetEdit.setVisibility(View.GONE);
                break;

            case R.id.btnBudgetCancel:
                pager.setCurrentItem(3);
                pagerAdapter.frmBudgetShow.setVisibility(View.VISIBLE);
                pagerAdapter.frmBudgetEdit.setVisibility(View.GONE);
                updateListBudgets();
                break;

            case R.id.btnBudgetToWear:
                try {
                    database.open();
                    tellWatchConnectedState("budget/database", database.getBudgetsToSync().toString());
                    database.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.btnNextMonth:
                updateAllViewsOnDataChanged(1);
                break;

            case R.id.btnPrevMonth:
                updateAllViewsOnDataChanged(-1);
                break;

            case R.id.btnReviewNextMonth:
                updateAllViewsOnDataChanged(1);
                break;

            case R.id.btnReviewPrevMonth:
                updateAllViewsOnDataChanged(-1);
                break;

            case R.id.btnStatsNext:
                drawYearlyGraph(1, DEFAULT_REVIEW_CATEGORY);
                break;

            case R.id.btnStatsPrev:
                drawYearlyGraph(-1, DEFAULT_REVIEW_CATEGORY);
                break;

            case R.id.btnTransactionsUnlock:
                pager.setCurrentItem(5);
                break;

            case R.id.btnReviewUnlock:
                pager.setCurrentItem(5);
                break;

            case R.id.btnBudgetUnlock:
                pager.setCurrentItem(5);
                break;

            case R.id.btnStatisticsUnlock:
                pager.setCurrentItem(5);
                break;

        }
    }

    /* ---- FUNCTIONS FOR DATABASE MECHANISMS --- */

    public SparseArray<Group> groupTransactionByDate(int month, int year) {
        SparseArray<Group> groups = new SparseArray<Group>();

        Calendar cal = Calendar.getInstance();
        cal.set(year, month, 1);
        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.set(year, month, maxDay);
        int myMonth = cal.get(Calendar.MONTH);
        int i = 0;

        try {
            while (myMonth == cal.get(Calendar.MONTH)) {
                String date = dfDate.format(cal.getTime());
                String whereClause = MobileExpenseTrackerDatabase.EXPENSE_DATE + "='" + date + "'";

                database.open();
                ArrayList<Expense> expense = database.getExpenses(whereClause);
                database.close();

                if (expense.size() > 0) {
                    Group group = new Group(dfDay.format(cal.getTime()), getTotalExpensesWithDate(date), expense);
                    groups.append(i, group);
                    i++;
                }
                cal.add(Calendar.DAY_OF_MONTH, -1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (groups.size() > 0) {
            Group group = new Group("", 0, new ArrayList<Expense>());
            groups.append(i, group);
        }
        return groups;
    }

    public ArrayList<Expense> getMonthlyExpenses(int year, int month) {
        ArrayList<Expense> expenses = new ArrayList<Expense>();

        Calendar cal = Calendar.getInstance();
        cal.set(year, month, 1);
        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.set(year, month, maxDay);
        int myMonth = cal.get(Calendar.MONTH);
        int i = 0;

        try {
            while (myMonth == cal.get(Calendar.MONTH)) {
                String date = dfDate.format(cal.getTime());
                String whereClause = MobileExpenseTrackerDatabase.EXPENSE_DATE + "='" + date + "'";

                database.open();
                ArrayList<Expense> expenseWithDate = database.getExpenses(whereClause);
                database.close();

                for (Expense e : expenseWithDate) {
                    expenses.add(e);
                }
                cal.add(Calendar.DAY_OF_MONTH, -1);
            }
        } catch (Exception e) {
            LogHelper.log(LogHelper.TAG_MobileExpenseTrackerActivity, e.getLocalizedMessage());
        }
        return expenses;
    }

    public float getTotalExpensesWithDate(String date) {
        ArrayList<Expense> expenses = new ArrayList<Expense>();
        float total = 0;

        try {
            database.open();
            String whereClause = MobileExpenseTrackerDatabase.EXPENSE_DATE + "='" + date + "'";

            expenses = database.getExpenses(whereClause);

            for (Expense e : expenses) {
                total += e.amount;
            }

            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return total;
    }

    public Budget updateBudget(String category, boolean isNew) {
        ArrayList<Budget> budgets = new ArrayList<Budget>();
        Budget budget = null;

        try {
            String date = dfDate.format(new Date());

            database.open();
            String whereClause = MobileExpenseTrackerDatabase.BUDGET_CATEGORY + "='" + category + "' ";
            budgets = database.getBudget(whereClause);

            float amount = Float.parseFloat(pagerAdapter.etBudgetAmount.getText().toString());
            String period = pagerAdapter.spinPeriod.getSelectedItem().toString();
            String startDate = dfDate.format(new Date());

            Calendar cal = Calendar.getInstance();
            if (period.equals("Weekly")) {
                cal.add(Calendar.DATE, 6);
            } else if (period.equals("Monthly")) {
                cal.add(Calendar.DATE, 29);
            }
            String endDate = dfDate.format(cal.getTime());

            if (budgets.size() > 0) {
                budget = budgets.get(0);

                if (isNew) {
                    budget.startDate = startDate;
                    budget.endDate = endDate;
                } else {
                    try {
                        // Update the budget
                        Calendar c = Calendar.getInstance();
                        c.setTime(dfDate.parse(budget.endDate));
                        c.add(Calendar.DATE, 1);

                        Date dateToUpdate = c.getTime();
                        Date now = new Date();

                        if (now.after(dateToUpdate)) {
                            budget.startDate = startDate;
                            budget.endDate = endDate;
                        } else {
                            if (!budget.period.equals(period)) {
                                budget.startDate = startDate;
                                budget.endDate = endDate;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                budget.category = category;
                budget.period = period;
                budget.amount = amount;
                database.updateBudget(budget);
            } else {
                budget = new Budget(0, category, period, startDate, endDate, amount);
                database.insertBudget(budget);
            }

            tellWatchConnectedState("budget/database", database.getBudgetsToSync().toString());

            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return budget;
    }

    public Budget getBudgetActivated(String category) {
        Budget budget = null;
        ArrayList<Budget> budgets = new ArrayList<Budget>();

        try {
            String date = dfDate.format(new Date());

            database.open();
            String whereClause = MobileExpenseTrackerDatabase.BUDGET_CATEGORY + "='" + category + "' ";
            budgets = database.getBudget(whereClause);
            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (budgets.size() > 0) {
            budget = budgets.get(0);

            try {
                Date endDate = dfDate.parse(budget.endDate);
                // Update the budget
                Calendar c = Calendar.getInstance();
                c.setTime(dfDate.parse(budget.endDate));
                c.add(Calendar.DATE, 1);

                Date dateToUpdate = c.getTime();
                Date now = new Date();

                if (now.after(dateToUpdate)) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(endDate);
                    if (budget.period.equals("Weekly")) {
                        cal.add(Calendar.DATE, 6);
                    } else if (budget.period.equals("Monthly")) {
                        cal.add(Calendar.DATE, 29);
                    }
                    budget.startDate = budget.endDate;
                    budget.endDate = dfDate.format(cal.getTime());

                    database.open();
                    database.updateBudget(budget);
                    database.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return budget;
    }


    /* --- FUNCTIONS FOR FLURRY --- */

    public void flurryStart() {
        // TO DO: Change flurry key
        FlurryAgent.onStartSession(this, "R9YC5C9WK2WG3GTV6PXG");
        flurryLogEvent("APP_HANDHELD_STARTED");
    }

    public void flurryStop() {
        flurryLogEvent("APP_HANDHELD_STOPPED");
        FlurryAgent.onEndSession(this);
    }

    public void flurryLogEvent(String event) {
        log("Flurry event = " + event);
        FlurryAgent.logEvent(event);
    }


    /* OTHER HELPER METHODS */

    String formatDecimal(float number) {
        DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance();
        format.setMaximumFractionDigits(2);
        format.setMinimumFractionDigits(2);
        return format.format(number);
    }

    void log(String msg) {
        Log.d("WearExpenseTrackerMobileActivity", msg);
    }

}




