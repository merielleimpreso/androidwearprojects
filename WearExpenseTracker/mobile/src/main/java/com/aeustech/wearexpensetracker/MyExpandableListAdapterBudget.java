package com.aeustech.wearexpensetracker;

import android.app.Activity;
import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by lnz on 3/23/15.
 */
public class MyExpandableListAdapterBudget extends BaseExpandableListAdapter {

    private final SparseArray<Budget> budgets;
    public LayoutInflater inflater;
    public Activity activity;

    public MyExpandableListAdapterBudget(Activity act, SparseArray<Budget> budgets) {
        activity = act;
        this.budgets = budgets;
        inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        //return budgets.get(groupPosition).children.get(childPosition);
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        /*DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat df2 = new SimpleDateFormat("hh:mm a");

        final Expense child = (Expense) getChild(groupPosition, childPosition);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.mobile_transactions_details, null);
        }

        ImageView imgCategory = (ImageView) convertView.findViewById(R.id.imgCategory);
        imgCategory.setImageResource(getDrawableCategory(child.category));

        TextView txtTransactionAmount = (TextView) convertView.findViewById(R.id.txtTransactionAmount);
        txtTransactionAmount.setText(formatDecimal(child.amount));

        TextView txtTransactionTime = (TextView) convertView.findViewById(R.id.txtTransactionTime);
        try {
            Date d = df1.parse(child.datetime);
            txtTransactionTime.setText(df2.format(d));
        } catch (ParseException e) {
            e.printStackTrace();
        }*/

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        /*if (budgets.get(groupPosition) != null) {
            return budgets.get(groupPosition).children.size();
        } else {

        }*/
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        if (budgets != null) {
            return budgets.get(groupPosition);
        } else {
            return null;
        }
    }

    @Override
    public int getGroupCount() {
        if (budgets != null) {
            return budgets.size();
        } else {
            return 0;
        }
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        Budget budget = (Budget) getGroup(groupPosition);
        ((MobileExpenseTrackerActivity) activity).editBudget(budget);
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        Budget budget = (Budget) getGroup(groupPosition);
        ((MobileExpenseTrackerActivity) activity).editBudget(budget);
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        int childrenCount = getChildrenCount(groupPosition);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.mobile_budget_group, null);
        }

        final int position = groupPosition;
        Budget budget = (Budget) getGroup(groupPosition);

        ImageView imgBudget = (ImageView) convertView.findViewById(R.id.imgBudget);
        TextView txtBudgetCategory = (TextView) convertView.findViewById(R.id.txtBudgetCategory);
        TextView txtBudgetAmount = (TextView) convertView.findViewById(R.id.txtBudgetAmount);
        TextView txtBudgetPeriod = (TextView) convertView.findViewById(R.id.txtBudgetPeriod);
        TextView txtBudgetDate = (TextView) convertView.findViewById(R.id.txtBudgetDates);

        imgBudget.setImageResource(getDrawableCategory(budget.category));
        txtBudgetCategory.setText(budget.category);

        if (budget.id < 0) {
            txtBudgetDate.setVisibility(View.GONE);
            txtBudgetPeriod.setVisibility(View.GONE);
            txtBudgetAmount.setText("---");
        } else {
            txtBudgetDate.setVisibility(View.VISIBLE);
            txtBudgetPeriod.setVisibility(View.VISIBLE);

            DateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat dfMonthDate = new SimpleDateFormat("MMM dd");
            String dateStr = "";

            try {
                Date startDate = dfDate.parse(budget.startDate);
                Date endDate = dfDate.parse(budget.endDate);

                dateStr = dfMonthDate.format(startDate);
                dateStr += " - ";
                dateStr += dfMonthDate.format(endDate);
            } catch (Exception e) {
                e.printStackTrace();

            }
            txtBudgetPeriod.setText(budget.period);
            txtBudgetAmount.setText(formatDecimal(budget.amount));

            if (budget.period.equals("Daily")) {
                txtBudgetDate.setText("");
            } else {
                txtBudgetDate.setText(dateStr);
            }


        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    String formatDecimal(float number) {
        DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance();
        format.setMaximumFractionDigits(2);
        format.setMinimumFractionDigits(2);
        return format.format(number);
    }

    int getDrawableCategory(String category) {
        if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_PERSONAL)) {
            return R.drawable.expense_cat1_personal;
        } else if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_HOUSE)) {
            return R.drawable.expense_cat2_house;
        } else if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_FOOD)) {
            return R.drawable.expense_cat3_food;
        } else if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_TRANSPORTATION)) {
            return R.drawable.expense_cat4_transpo;
        } else if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_CLOTHING)) {
            return R.drawable.expense_cat5_clothes;
        } else if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_FUN)) {
            return R.drawable.expense_cat6_fun;
        } else if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_FAMILY)) {
            return R.drawable.expense_cat7_family;
        } else if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_MISCELLANEOUS)) {
            return R.drawable.expense_cat8_misc;
        } else {
            return 0;
        }
    }
}
