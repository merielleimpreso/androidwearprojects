package com.aeustech.wearexpensetracker;

/**
 * Created by Merielle Impreso on 1/28/15.
 */
public class Expense {
    public int id;
    public String date;
    public String datetime;
    public String category;
    public float amount;

    public Expense(int id, String date, String datetime, String category, float amount) {
        this.id = id;
        this.date = date;
        this.datetime = datetime;
        this.category = category;
        this.amount = amount;
    }
}
