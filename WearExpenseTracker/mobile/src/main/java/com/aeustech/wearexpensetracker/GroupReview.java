package com.aeustech.wearexpensetracker;

/**
 * Created by lnz on 3/25/15.
 */
public class GroupReview {
    public String category;
    public float amount;
    public int percentage;

    public GroupReview(String category, float amount, int percentage) {
        this.category = category;
        this.amount = amount;
        this.percentage = percentage;
    }
}
