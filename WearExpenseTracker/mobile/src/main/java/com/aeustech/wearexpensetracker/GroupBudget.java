package com.aeustech.wearexpensetracker;

import java.util.ArrayList;

/**
 * Created by Merielle Impreso on 3/23/15.
 */
public class GroupBudget {
    public ArrayList<Budget> children = new ArrayList<Budget>();

    public GroupBudget(ArrayList<Budget> children) {
        this.children = children;
    }
}
