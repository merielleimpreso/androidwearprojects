package com.aeustech.wearexpensetracker;

import android.util.Log;

/**
 * Created by Merielle Impreso on 2/4/15.
 */
public class LogHelper {
    public static String TAG_MobileExpenseTrackerActivity = "MobileExpenseTrackerActivity";
    public static String TAG_MobileExpenseTrackerDatabase = "MobileExpenseTrackerDatabase";

    public static void log(String tag, String msg) {
        Log.d(tag, msg);
    }
}
