package com.aeustech.wearexpensetracker;

import android.app.Activity;
import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

/**
 * Created by lnz on 3/25/15.
 */
public class MyExpandableListAdapterReview extends BaseExpandableListAdapter {

    private final SparseArray<GroupReview> reviews;
    public LayoutInflater inflater;
    public Activity activity;

    public MyExpandableListAdapterReview(Activity act, SparseArray<GroupReview> reviews) {
        activity = act;
        this.reviews = reviews;
        inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        //return budgets.get(groupPosition).children.get(childPosition);
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        /*DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat df2 = new SimpleDateFormat("hh:mm a");

        final Expense child = (Expense) getChild(groupPosition, childPosition);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.mobile_transactions_details, null);
        }

        ImageView imgCategory = (ImageView) convertView.findViewById(R.id.imgCategory);
        imgCategory.setImageResource(getDrawableCategory(child.category));

        TextView txtTransactionAmount = (TextView) convertView.findViewById(R.id.txtTransactionAmount);
        txtTransactionAmount.setText(formatDecimal(child.amount));

        TextView txtTransactionTime = (TextView) convertView.findViewById(R.id.txtTransactionTime);
        try {
            Date d = df1.parse(child.datetime);
            txtTransactionTime.setText(df2.format(d));
        } catch (ParseException e) {
            e.printStackTrace();
        }*/

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        /*if (budgets.get(groupPosition) != null) {
            return budgets.get(groupPosition).children.size();
        } else {

        }*/
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        if (reviews != null) {
            return reviews.get(groupPosition);
        } else {
            return null;
        }
    }

    @Override
    public int getGroupCount() {
        if (reviews != null) {
            return reviews.size();
        } else {
            return 0;
        }
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        int childrenCount = getChildrenCount(groupPosition);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.mobile_review_group, null);
        }

        GroupReview review = (GroupReview) getGroup(groupPosition);

        ImageView imgReview = (ImageView) convertView.findViewById(R.id.imgReview);
        TextView txtReviewCategory = (TextView) convertView.findViewById(R.id.txtReviewCategory);
        TextView txtReviewAmount = (TextView) convertView.findViewById(R.id.txtReviewAmount);
        TextView txtReviewPercentage = (TextView) convertView.findViewById(R.id.txtReviewPercentage);


        imgReview.setImageResource(getDrawableCategory(review.category));
        txtReviewCategory.setText(review.category);

        if (review.amount > 0) {
            txtReviewPercentage.setVisibility(View.VISIBLE);
            txtReviewAmount.setText(formatDecimal(review.amount));
            txtReviewPercentage.setText(review.percentage + "%");
        } else {
            txtReviewPercentage.setVisibility(View.GONE);
            txtReviewAmount.setText("---");
        }



        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    String formatDecimal(float number) {
        DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance();
        format.setMaximumFractionDigits(2);
        format.setMinimumFractionDigits(2);
        return format.format(number);
    }

    int getDrawableCategory(String category) {
        if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_PERSONAL)) {
            return R.drawable.expense_cat1_personal_0;
        } else if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_HOUSE)) {
            return R.drawable.expense_cat2_house_0;
        } else if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_FOOD)) {
            return R.drawable.expense_cat3_food_0;
        } else if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_TRANSPORTATION)) {
            return R.drawable.expense_cat4_transpo_0;
        } else if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_CLOTHING)) {
            return R.drawable.expense_cat5_clothes_0;
        } else if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_FUN)) {
            return R.drawable.expense_cat6_fun_0;
        } else if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_FAMILY)) {
            return R.drawable.expense_cat7_family_0;
        } else if (category.equalsIgnoreCase(MobileExpenseTrackerDatabase.EXPENSE_CATEGORY_MISCELLANEOUS)) {
            return R.drawable.expense_cat8_misc_0;
        } else {
            return 0;
        }
    }
}
