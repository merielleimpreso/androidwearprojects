package com.aeustech.wearexpensetracker;

/**
 * Created by Merielle Impreso on 3/12/15.
 */
public class Budget {
    public int id;
    public String category;
    public String period;
    public String startDate;
    public String endDate;
    public float amount;

    public Budget(int id, String category, String period, String startDate, String endDate, float amount) {
        this.id = id;
        this.category = category;
        this.period = period;
        this.startDate = startDate;
        this.endDate = endDate;
        this.amount = amount;
    }

}
