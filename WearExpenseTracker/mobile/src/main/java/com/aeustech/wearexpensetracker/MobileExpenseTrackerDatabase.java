package com.aeustech.wearexpensetracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Merielle Impreso on 3/4/15.
 */
public class MobileExpenseTrackerDatabase {
    public static final String DATABASE_NAME = "aeus_extrak";
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_TABLE_EXPENSE = "expense";
    public static final String EXPENSE_ID = "id";
    public static final String EXPENSE_DATE = "date";
    public static final String EXPENSE_DATETIME = "datetime";
    public static final String EXPENSE_CATEGORY = "category";
    public static final String EXPENSE_AMOUNT = "amount";
    public static final String EXPENSE_DELETED = "deleted";

    public static final String DATABASE_TABLE_BUDGET = "budget";
    public static final String BUDGET_ID = "id";
    public static final String BUDGET_CATEGORY = "category";
    public static final String BUDGET_PERIOD = "period";
    public static final String BUDGET_START_DATE = "startDate";
    public static final String BUDGET_END_DATE = "endDate";
    public static final String BUDGET_AMOUNT = "amount";

    public static final String EXPENSE_CATEGORY_PERSONAL = "PERSONAL";
    public static final String EXPENSE_CATEGORY_HOUSE = "HOUSE";
    public static final String EXPENSE_CATEGORY_FOOD = "FOOD";
    public static final String EXPENSE_CATEGORY_TRANSPORTATION = "TRANSPORT";
    public static final String EXPENSE_CATEGORY_CLOTHING = "CLOTHING";
    public static final String EXPENSE_CATEGORY_FUN = "FUN";
    public static final String EXPENSE_CATEGORY_FAMILY = "FAMILY";
    public static final String EXPENSE_CATEGORY_MISCELLANEOUS = "MISC";

    private DbHelper helper;
    private Context context;
    public static SQLiteDatabase database;

    private static class DbHelper extends SQLiteOpenHelper {

        public DbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + DATABASE_TABLE_EXPENSE + " (" +
                            EXPENSE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, " +
                            EXPENSE_DATE + " DATE, " +
                            EXPENSE_DATETIME + " DATETIME, " +
                            EXPENSE_CATEGORY + " STRING, " +
                            EXPENSE_AMOUNT + " FLOAT);"
            );

            db.execSQL("CREATE TABLE " + DATABASE_TABLE_BUDGET + " (" +
                            BUDGET_ID + " INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, " +
                            BUDGET_CATEGORY + " STRING, " +
                            BUDGET_PERIOD + " STRING, " +
                            BUDGET_START_DATE + " DATE, " +
                            BUDGET_END_DATE + " DATE, " +
                            BUDGET_AMOUNT + " FLOAT);"
            );
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_EXPENSE);
            onCreate(db);
        }
    }

    public MobileExpenseTrackerDatabase(Context context) {
        this.context = context;
    }

    public MobileExpenseTrackerDatabase open() throws SQLException {
        this.helper = new DbHelper(context);
        this.database = helper.getWritableDatabase();
        return this;
    }

    public void close() {
        helper.close();
    }

    public long insertExpense(Expense expense) {
        ContentValues cv = new ContentValues();
        cv.put(EXPENSE_ID, expense.id);
        cv.put(EXPENSE_DATE, expense.date);
        cv.put(EXPENSE_DATETIME, expense.datetime);
        cv.put(EXPENSE_CATEGORY, expense.category);
        cv.put(EXPENSE_AMOUNT, expense.amount);
        //LogHelper.log(LogHelper.TAG_WearExpenseTrackerDatabase, "insertExpense");
        return database.insert(DATABASE_TABLE_EXPENSE, null, cv);
    }

    public void updateExpense(Expense expense) {
        ContentValues cv = new ContentValues();
        cv.put(EXPENSE_DATE, expense.date);
        cv.put(EXPENSE_DATETIME, expense.datetime);
        cv.put(EXPENSE_CATEGORY, expense.category);
        cv.put(EXPENSE_AMOUNT, expense.amount);
        //LogHelper.log(LogHelper.TAG_WearExpenseTrackerDatabase, "updateExpense");
        database.update(DATABASE_TABLE_EXPENSE, cv, "id='" + expense.id + "'", null);
    }

    public ArrayList<Expense> getExpenses(String whereClause) {
        String[] columns = new String[] { EXPENSE_ID,
                EXPENSE_DATE,
                EXPENSE_DATETIME,
                EXPENSE_CATEGORY,
                EXPENSE_AMOUNT };
        Cursor c = database.query(DATABASE_TABLE_EXPENSE, columns, whereClause, null, null, null,
                EXPENSE_DATETIME + " DESC");
        ArrayList<Expense> expenses = new ArrayList<Expense>();

        //Get the index of each column
        int iId = c.getColumnIndex(EXPENSE_ID);
        int iDate = c.getColumnIndex(EXPENSE_DATE);
        int iDateTime = c.getColumnIndex(EXPENSE_DATETIME);
        int iCategory = c.getColumnIndex(EXPENSE_CATEGORY);
        int iAmount = c.getColumnIndex(EXPENSE_AMOUNT);

        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            int eId = c.getInt(iId);
            String eDate = c.getString(iDate);
            String eDatetime = c.getString(iDateTime);
            String eCategory = c.getString(iCategory);
            float eAmount = c.getFloat(iAmount);
            expenses.add(new Expense(eId, eDate, eDatetime, eCategory, eAmount));
        }
        c.close();

        return expenses;
    }

    public void deleteExpense(Expense expense) {
        database.delete(DATABASE_TABLE_EXPENSE, EXPENSE_ID + "=" + expense.id, null);
    }

    public long insertBudget(Budget budget) {
        ContentValues cv = new ContentValues();
        cv.put(BUDGET_CATEGORY, budget.category);
        cv.put(BUDGET_PERIOD, budget.period);
        cv.put(BUDGET_START_DATE, budget.startDate);
        cv.put(BUDGET_END_DATE, budget.endDate);
        cv.put(BUDGET_AMOUNT, budget.amount);

        LogHelper.log(LogHelper.TAG_MobileExpenseTrackerDatabase, "insertBudget");
        return database.insert(DATABASE_TABLE_BUDGET, null, cv);
    }

    public void updateBudget(Budget budget) {
        ContentValues cv = new ContentValues();
        cv.put(BUDGET_ID, budget.id);
        cv.put(BUDGET_CATEGORY, budget.category);
        cv.put(BUDGET_PERIOD, budget.period);
        cv.put(BUDGET_START_DATE, budget.startDate);
        cv.put(BUDGET_END_DATE, budget.endDate);
        cv.put(BUDGET_AMOUNT, budget.amount);

        LogHelper.log(LogHelper.TAG_MobileExpenseTrackerDatabase, "updateBudget");
        database.update(DATABASE_TABLE_BUDGET, cv, "category='" + budget.category + "'", null);
    }

    public ArrayList<Budget> getBudget(String whereClause) {
        String[] columns = new String[] { BUDGET_ID,
                BUDGET_CATEGORY,
                BUDGET_PERIOD,
                BUDGET_START_DATE,
                BUDGET_END_DATE,
                BUDGET_AMOUNT };
        Cursor c = database.query(DATABASE_TABLE_BUDGET, columns, whereClause, null, null, null, null);
        ArrayList<Budget> budgets = new ArrayList<Budget>();

        //Get the index of each column
        int iId = c.getColumnIndex(BUDGET_ID);
        int iCategory = c.getColumnIndex(BUDGET_CATEGORY);
        int iPeriod = c.getColumnIndex(BUDGET_PERIOD);
        int iStartDate = c.getColumnIndex(BUDGET_START_DATE);
        int iEndDate = c.getColumnIndex(BUDGET_END_DATE);
        int iAmount = c.getColumnIndex(BUDGET_AMOUNT);

        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            int bId = c.getInt(iId);
            String bCategory = c.getString(iCategory);
            String bPeriod = c.getString(iPeriod);
            String bStartDate = c.getString(iStartDate);
            String bEndDate = c.getString(iEndDate);
            float bAmount = c.getFloat(iAmount);
            budgets.add(new Budget(bId, bCategory, bPeriod, bStartDate, bEndDate, bAmount));
        }
        c.close();

        return budgets;
    }

    public JSONArray getBudgetsToSync() throws JSONException {
        ArrayList<Budget> budgets = getBudget(null);
        JSONArray jsonArray = new JSONArray();

        int limit = budgets.size();
        for (int i = 0; i < limit; i++) {
            JSONObject object = new JSONObject();
            Budget b = budgets.get(i);

            try {
                object.put(BUDGET_ID, b.id);
                object.put(BUDGET_CATEGORY, b.category);
                object.put(BUDGET_PERIOD, b.period);
                object.put(BUDGET_START_DATE, b.startDate);
                object.put(BUDGET_END_DATE, b.endDate);
                object.put(BUDGET_AMOUNT, b.amount);
            } catch (JSONException exception) {
                exception.printStackTrace();
            }
            jsonArray.put(object);
        }
        return jsonArray;
    }

}
