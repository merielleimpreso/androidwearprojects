package com.aeustech.wearexpensetracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;

import com.android.vending.billing.util.IabHelper;
import com.android.vending.billing.util.IabResult;
import com.android.vending.billing.util.Inventory;
import com.android.vending.billing.util.Purchase;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.util.List;

public class Interface {

    public Activity context;
    String purchasedItem;
    public IabHelper iabHelper;
    boolean iabSetupDone = false;
    IabHelper mHelper;
    static int logCounter = 0;

    public Interface() {
    }

    public static void log(String message) {
        //Log.d("WearHydrateMe_I", (logCounter++) + " - " + message);
    }

    public void gplayRestorePurchase() {
        iabHelper.queryInventoryAsync(mGotInventoryListener);
    }

    public IabHelper gplayInitIAB() {

        if (!gplayServicesAvailable()) return null;

        String a = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsCLlWKctUlxPL7n9wyd5Q2bkaoF49GWup0rrH3OAW3BFpIhu2hSB0CVWQF/Kbq7DhHlF+Q/FFbJ4u7VFl";
        String b = "7/my2OFym2KEKmKMZDbohxL/2G5gRsTGpG4mxUsktGnrowfuCsyQxHiURqJqyrYA0F5FUUkZBrfnRmv1Znn0lBInWKWJI4s9lWXQfsrqJ1AV4JBnBOEC1B3VL53oR";
        String c = "x+7JThzis/2rmbqQftLy6yeCiDc11hlmwRVXBSumvsvEiEDFvBkNJen6jIdlzrGLnSAKJUY0WQVrqOjcnAZ0ZOzE067KSepy73ZDsi9/1iG2KYa4FBMSP/fbEPGBK49BDlJFtOxQIDAQAB";

        String base64EncodedPublicKey = a + b + c;

        // compute your public key and store it in base64EncodedPublicKey
        mHelper = new IabHelper(context, base64EncodedPublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    log("IAB setup error: " + result);
                    iabSetupDone = false;
                    return;
                } else {
                    log("IAB setup success: " + result);
                    iabSetupDone = true;
                    ((MobileExpenseTrackerActivity)context).checkingPrevPurchases = true;
                    iabHelper.queryInventoryAsync(mGotInventoryListener);
                }
                if (mHelper == null) return;
            }
        });
        iabHelper = mHelper;

        return mHelper;
    }

    public void gplayDestroyIAB() {
        if (iabHelper != null) iabHelper.dispose();
        iabHelper = null;
    }

    public void gplayIABBuyItem(String itemID) {

        if(! gplayServicesAvailable() || ! iabSetupDone) {
            // if bought from watch, tell watch user cancelled on phone
            ((MobileExpenseTrackerActivity) context).cancelBuy();
            showAlert("Please try buying from your watch again.");

            return;
        }

        //TO DO: Comment out, delete android.test.purchased
        //purchasedItem = "android.test.purchased";

        purchasedItem = "com.aeustech.wearexpensetracker." + itemID;

        log("about to purchase item = " + purchasedItem);
        itemID = purchasedItem;
        //itemID = "android.test.purchased";
        try {
            iabHelper.launchPurchaseFlow(context, itemID, 1000,
                    mPurchaseFinishedListener, "bought_" + itemID);
        } catch (Exception e) {
            ((MobileExpenseTrackerActivity)context).cancelBuy();
            log("GAME SERVICES ERROR: " + e.getMessage());
            showAlert("Please log on to Google Play services to use this feature.");
        }
    }



    public boolean gplayServicesAvailable() {
        // Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Activity Recognition",
                    "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason
        } else {

            showAlert("Google Play services is not available on this device.");

            // Get the error dialog from Google Play services
//            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
//                    resultCode,
//                    UnityPlayer.currentActivity,
//                    -1);
//
//            // If Google Play services can provide an error dialog
//            if (errorDialog != null) {
//                // Create a new DialogFragment for the error dialog
//                ErrorDialogFragment errorFragment =
//                        new ErrorDialogFragment();
//                // Set the dialog in the DialogFragment
//                errorFragment.setDialog(errorDialog);
//                // Show the error dialog in the DialogFragment
//                errorFragment.show(
//                        getSupportFragmentManager(),
//                        "Activity Recognition");
//            }
            return false;
        }
    }

    public void showAlert(String message) {
        log("show alert");

        final String msg = message;
        new AlertDialog.Builder(context)
                .setTitle("Wear Expense Manager")
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .show();
    }

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            log("Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
//            if (StaticObjects.iabHelper == null) return;

            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item to the game
                String sku = purchase.getSku();
                log("Consumption successful. Provisioning " + sku);
            }
            else {
                log("Error while consuming: " + result);
            }

            log("End consumption flow.");
        }
    };

    void unlockItemPurchased(Purchase purchase) {
        String boughtItem = purchase.getSku().replace(".", "/");
        String[] data = boughtItem.split("/");

        if (data.length >= 4) {
            if(data[3].equalsIgnoreCase("fullversion")) {
                log("unlock product = " + data[3]);
                ((MobileExpenseTrackerActivity) context).finishedBuy();
            }
        } else if (data.length == 3) { // for android test purchased
            if (data[2].equalsIgnoreCase("purchased")) {
                ((MobileExpenseTrackerActivity) context).finishedBuy();

            }
        }
    }

    // Google Play Billing service
    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            log("Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (iabHelper == null) return;

            if (result.isFailure()) {
                log("Error purchasing: " + result.getResponse());

                // already owned
                if(result.getResponse() == 7) {
                    log("item is already owned, unlocking");
                    showAlert("You already own this item.\nPlease press the Restore button to restore your previous purchases.");
                } else {
                    // if bought from watch, tell watch user cancelled on phone
                    ((MobileExpenseTrackerActivity) context).cancelBuy();
                }
                return;
            }

            log("Purchase successful.");

            log("purchased " + purchase.getSku());

            unlockItemPurchased(purchase);
        }
    };

    // Listener that's called when we finish querying the items we own
    final IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            log("Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                log("Failed to query inventory: " + result);
                return;
            }

            log("Query inventory was successful: " + inventory.getAllPurchases().toString());

            List<Purchase> purchases = inventory.getAllPurchases();

            // if we have owned but not consumed item, use it
//    	            if (inventory.hasPurchase("android.test.purchased")) {
            if (purchases.size() > 0) {
//    	                mHelper.consumeAsync(inventory.getPurchase("android.test.purchased"), mConsumeFinishedListener);
//                    purchasedItem = purchases.get(0).getSku();
//    	            log("prev purchase sku = " + purchasedItem);
                // unlock items for non-consumables

                // comment out for non-consumables, do not consume
//                    mHelper.consumeAsync(inventory.getPurchase(purchasedItem), mConsumeFinishedListener);

                // consume all, for testing only
                for(Purchase p : purchases) {
                    log("prev sku = " + p.getSku());
                    ((MobileExpenseTrackerActivity)context).prevPurchasesFound = true;
                    unlockItemPurchased(p);

                    // TESTING: enable to buy non-consumable again
                    if(((MobileExpenseTrackerActivity)context).consumeNC) {
                        mHelper.consumeAsync(inventory.getPurchase(p.getSku()), mConsumeFinishedListener);
                    }
                }
            } else {
                if(! ((MobileExpenseTrackerActivity)context).checkingPrevPurchases) {
                    showAlert("No previous purchases found.");
                }
            }
        }
    };
}

