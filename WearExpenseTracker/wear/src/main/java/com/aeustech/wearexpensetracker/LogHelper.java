package com.aeustech.wearexpensetracker;

import android.util.Log;

/**
 * Created by Merielle Impreso on 2/4/15.
 */
public class LogHelper {
    public static String TAG_WearExpenseTrackerActivity = "WearExpenseTrackerActivity";
    public static String TAG_WearExpenseTrackerDatabase = "WearExpenseTrackerDatabase";
    public static String TAG_MyExpandableListAdapter = "MyExpandableListAdapter";

    public static void log(String tag, String msg) {
        Log.d(tag, msg);
    }
}
