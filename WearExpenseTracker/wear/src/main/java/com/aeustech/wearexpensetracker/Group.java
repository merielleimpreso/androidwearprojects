package com.aeustech.wearexpensetracker;

import java.util.ArrayList;

/**
 * Created by lnz on 2/5/15.
 */
public class Group {
    public String date;
    public float amount;
    public ArrayList<Expense> children = new ArrayList<Expense>();

    public Group(String date, float amount, ArrayList<Expense> children) {
        this.date = date;
        this.amount = amount;
        this.children = children;
    }

}
