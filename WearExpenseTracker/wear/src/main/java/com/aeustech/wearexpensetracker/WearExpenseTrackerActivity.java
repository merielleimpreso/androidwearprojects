package com.aeustech.wearexpensetracker;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.wearable.view.DismissOverlayView;
import android.support.wearable.view.WatchViewStub;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.Display;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.MultipleCategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class WearExpenseTrackerActivity extends Activity implements GoogleApiClient.ConnectionCallbacks,
        MessageApi.MessageListener, View.OnLongClickListener {

    // Let other class access this MainActivity context
    public Context context;
    SharedPreferences prefs;

    // Variable for wear layouts
    FrameLayout frmMain;
    boolean isRound;
    public DismissOverlayView mDismissOverlayView;

    // Variables for pages
    LayoutInflater inflater;
    ViewGroup viewGroupLayout;
    WearExpenseTrackerPagerAdapter pagerAdapter;
    CustomViewPager pager;
    int currentPage = 0;
    GestureDetector gestureDetector;

    // Variable for number values & SeekArc
    DecimalFormat format;
    String localeSeparator = "\\.";
    int max = 5000;

    // Variables for database
    WearExpenseTrackerDatabase database;
    public static final int DATABASE_SYNC_LIMIT = 50;

    // Common variable
    DateFormat dfDate, dfTime, dfTimeOnly, dfDay, dfMonth, dfToday, dfDayComplete;
    int transactionMonth, transactionYear, nowMonth, nowYear;
    String nowDate;
    final static String DEFAULT_CATEGORY = "SELECT";
    final static String DEFAULT_REVIEW_CATEGORY = "TOTAL EXPENSES";
    final static int PAGE_TODAY = 1;
    final static int PAGE_HISTORY = 2;
    final static int PAGE_REVIEW = 3;
    final static int PAGE_BUDGET = 4;
    String[] MONTHS_FOR_SELECTION = new String[12];

    // Variables for charts
    String[] categories = new String[8];
    int[] colors = new int[8];

    // Handheld-Wear communication
    private Node peerNode;
    GoogleApiClient mGoogleApiClient = null;
    String nodeId;
    long CONNECTION_TIME_OUT_MS = 5000;

    boolean isShownTutorialReview = false;
    boolean isShownTutorialBudget = false;


    // Charts




    /* ---- FUNCTIONS USED IN INITIALIZATION ---- */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Instantiate MainActivity context
        context = this;
        prefs = this.getSharedPreferences("com.aeustech.wearexpensetracker", Context.MODE_PRIVATE);

        initializeAPI();
        initializeLayout();
        initializeValuesForCharts();
        initializeFormatters();
    }

    @Override
    protected void onResume() {
        super.onResume();
        pager.setCurrentItem(1);
    }

    private void initializeLayout() {
        // Stub layouts
        mDismissOverlayView = new DismissOverlayView(this);

        WatchViewStub stub = new WatchViewStub(this);
        stub.setRoundLayout(R.layout.layout_main_round);
        stub.setRectLayout(R.layout.layout_main_rect);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub watchViewStub) {
                LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, "--- on layout inflated");

                // Detect layout shape
                frmMain = (FrameLayout) findViewById(R.id.frmMainRect);
                if (frmMain == null) {
                    LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, "round screen detected");

                    frmMain = (FrameLayout) findViewById(R.id.frmMainRound);
                    isRound = true;

                    ViewGroup parent = (ViewGroup) mDismissOverlayView.getParent();
                    if (parent != null) {
                        parent.removeView(mDismissOverlayView);
                    }
                    frmMain.addView(mDismissOverlayView, new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.MATCH_PARENT));
                } else {
                    LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, "rectangular screen detected");
                    frmMain.addView(mDismissOverlayView, new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.MATCH_PARENT));
                    isRound = false;
                }
                initializeDatabase();
                initializePagerAdapter();
            }
        });
        addContentView(stub, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        stub.inflate();
    }

    private void initializeValuesForCharts() {
        MONTHS_FOR_SELECTION = new String[] {
                "JAN",
                "FEB",
                "MAR",
                "APR",
                "MAY",
                "JUN",
                "JUL",
                "AUG",
                "SEP",
                "OCT",
                "NOV",
                "DEC"
        };
        categories = new String[] { WearExpenseTrackerDatabase.EXPENSE_CATEGORY_PERSONAL,
                WearExpenseTrackerDatabase.EXPENSE_CATEGORY_HOUSE,
                WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FOOD,
                WearExpenseTrackerDatabase.EXPENSE_CATEGORY_TRANSPORTATION,
                WearExpenseTrackerDatabase.EXPENSE_CATEGORY_CLOTHING,
                WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FUN,
                WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FAMILY,
                WearExpenseTrackerDatabase.EXPENSE_CATEGORY_MISCELLANEOUS
        };
        colors = new int[] { Color.parseColor("#6CA7C0"),
                Color.parseColor("#BAB124"),
                Color.parseColor("#42CF86"),
                Color.parseColor("#F8504D"),
                Color.parseColor("#833AA7"),
                Color.parseColor("#D853AE"),
                Color.parseColor("#F7931E"),
                Color.parseColor("#618246"),
        };
    }

    private void initializeFormatters() {
        dfDate = new SimpleDateFormat("yyyy-MM-dd");
        dfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        dfTimeOnly = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        dfDay = new SimpleDateFormat("dd EEE");
        dfMonth = new SimpleDateFormat("MMM yyyy");
        dfToday = new SimpleDateFormat("dd MMM yyyy, EEE");
        dfDayComplete = new SimpleDateFormat("EEEE");

        Calendar cal = Calendar.getInstance();
        transactionYear = cal.get(Calendar.YEAR);
        transactionMonth = cal.get(Calendar.MONTH);
        nowYear = transactionYear;
        nowMonth = transactionMonth;
        nowDate = dfDate.format(new Date());

        // Instantiate localeSeparator
        format = (DecimalFormat) DecimalFormat.getInstance();
        format.setMaximumFractionDigits(2);
        format.setMinimumFractionDigits(2);
        DecimalFormatSymbols symbols = format.getDecimalFormatSymbols();
        localeSeparator = String.valueOf(symbols.getDecimalSeparator());
    }

    private void initializeAPI() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, "onConnectionFailed: " + result);
                    }
                })
                .addApi(Wearable.API)
                .build();

        mGoogleApiClient.connect();
    }

    private void initializeDatabase() {
        database = new WearExpenseTrackerDatabase(context);
    }

    private void initializePagerAdapter() {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewGroupLayout = (ViewGroup) findViewById(android.R.id.content);

        pagerAdapter = new WearExpenseTrackerPagerAdapter();
        pager = (CustomViewPager) findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);
        pagerAdapter.preloadPacks();
        pager.setOffscreenPageLimit(pagerAdapter.getCount());
        pager.setCurrentItem(1);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Check if scrolled to page 1
                if (position == 0) {
                    mDismissOverlayView.setIntroText("Are you sure you want to exit the app?");
                    mDismissOverlayView.setVisibility(View.VISIBLE);
                    mDismissOverlayView.show();
                } else {
                    if (position == PAGE_REVIEW) {
                        if (!isShownTutorialReview) {
                            Toast.makeText(WearExpenseTrackerActivity.this, "Long press for category", Toast.LENGTH_SHORT).show();
                            isShownTutorialReview = true;
                        }

                    } else if (position == PAGE_BUDGET) {
                        if (!isShownTutorialBudget) {
                            Toast.makeText(WearExpenseTrackerActivity.this, "Long press for category", Toast.LENGTH_SHORT).show();
                            isShownTutorialBudget = true;
                        }

                    }
                    mDismissOverlayView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private List<Node> getNodes() {
        List<Node> nodes = new ArrayList<Node>();
        NodeApi.GetConnectedNodesResult rawNodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
        for (Node node : rawNodes.getNodes()) {
            nodes.add(node);
        }
        return nodes;
    }

    /*private void retrieveDeviceNode() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mGoogleApiClient.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                NodeApi.GetConnectedNodesResult result = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();

                List<Node> nodes = result.getNodes();
                if (nodes.size() > 0) {
                    nodeId = nodes.get(0).getId();
                }
                mGoogleApiClient.disconnect();
            }
        }).start();
    }

    private void sendMessageToHandheld(final String path, final String message) {
        retrieveDeviceNode();
        if (nodeId != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mGoogleApiClient.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                    Wearable.MessageApi.sendMessage(mGoogleApiClient, nodeId, path, message.getBytes());
                    mGoogleApiClient.disconnect();
                }
            });
        }
    }*/

    @Override
    public boolean onLongClick(View view) {
        if (view.getId() == R.id.btnShowReviewCategories) {
            pagerAdapter.frmReview.setVisibility(View.GONE);
            pagerAdapter.frmReviewCategory.setVisibility(View.VISIBLE);
        } else if (view.getId() == R.id.btnShowBudgetCategories) {
            pagerAdapter.frmBudget.setVisibility(View.GONE);
            pagerAdapter.frmBudgetCategory.setVisibility(View.VISIBLE);
        } else if (view.getId() == R.id.txtAddExpense) {
            pagerAdapter.txtAddExpense.setText("00");
        } else if (view.getId() == R.id.txtAddExpenseThousand) {
            pagerAdapter.txtAddExpenseThousand.setText("00");
        } else if (view.getId() == R.id.txtAddExpenseHundredThousand) {
            pagerAdapter.txtAddExpenseHundredThousand.setText("00");
        } else if (view.getId() == R.id.txtAddExpenseCents) {
            pagerAdapter.txtAddExpenseCents.setText("00");
        }

        return true;
    }

    // Class for sliding pages
    public class WearExpenseTrackerPagerAdapter extends PagerAdapter {

        // Views for page TODAY
        FrameLayout frmShowExpense, frmAddExpense, frmAddExpenseCategories, frmExpenseChart, frmExpensesOverbudget, frmAddExpenseDate;
        ImageButton btnAddPersonal, btnAddHouse, btnAddFood, btnAddTranspo, btnAddClothes, btnAddFun, btnAddFamily, btnAddMisc;
        ImageButton btnShowExpenseAdd, btnShowExpenseAddCategories;
        SeekArc seekAddExpense;
        Spinner spinDD, spinMM, spinYY;
        TextView txtSelected, txtTodayDate, txtOverbudget, txtExpenseDay;
        TextView txtAddExpense, txtAddExpenseCents, txtAddExpenseDate, txtAddExpenseCategory, txtShowExpenses, separator;
        TextView txtAddExpenseThousand, txtAddExpenseHundredThousand;
        View viewLineAddExpenseThousand, viewLineAddExpenseHundredThousand;

        // Views for page HISTORY
        TextView txtMonth, txtNoTransaction;
        ExpandableListView listTransactions;
        FrameLayout layoutDeleteBg;

        // Views for page REVIEW
        FrameLayout frmReviewExpenseChart, frmReview, frmReviewCategory, btnShowReviewCategories;
        ImageButton btnReviewTotal, btnReviewPersonal, btnReviewHouse, btnReviewFood, btnReviewTranspo, btnReviewClothes, btnReviewFun, btnReviewFamily, btnReviewMisc;
        ImageView imgReviewBg, imgReviewEditCategory;
        TextView txtReviewMonth, txtReviewTotal, txtReviewCategory, txtReviewPercentage, txtReviewSelectCategory;
        View viewLineReview;

        // Views for page BUDGET
        Button btnCheckPhone;
        FrameLayout frmBudgetChart, frmBudgetCategory, frmBudget, btnShowBudgetCategories, frmLocked, frmBudgetButtons;
        ImageButton btnGoToExpenses, btnGoToBudget, btnGoToRemaining;
        ImageButton btnBudgetPersonal, btnBudgetHouse, btnBudgetFood, btnBudgetTranspo, btnBudgetClothes, btnBudgetFun, btnBudgetFamily, btnBudgetMisc;
        ImageView imgBudgetEditCategory;
        TextView txtBudgetTitle, txtBudgetCategory, txtBudgetAmount, txtBudgetDays, txtBudgetSelectCategory;

        // Variables for SeekArc
        int prevProgress = 0;
        int revo = 0;

        // Arrays for layout views
        ArrayList<View> views = new ArrayList<View>();

        // Variables for list
        // more efficient than HashMap for mapping integers to objects
        SparseArray<Group> groups = new SparseArray<Group>();

        // Load pages (xml)
        public void preloadPacks() {
            views.clear();

            views.add((View) inflater.inflate(R.layout.wear_page_exit, null));
            views.add((View) inflater.inflate(R.layout.wear_expenses, null));
            views.add((View) inflater.inflate(R.layout.wear_transactions, null));
            views.add((View) inflater.inflate(R.layout.wear_review, null));
            views.add((View) inflater.inflate(R.layout.wear_budgets, null));
        }

        // Return number of pages
        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }

        // Instantiate views
        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            View v = views.get(position);
            collection.addView(v, 0);

            // Get position (xml pages)
            switch (position) {
                case PAGE_TODAY:
                    //layoutThousand = (LinearLayout) findViewById(R.id.layoutThousand);
                    //layoutHundredThousand = ()

                    txtOverbudget = (TextView) findViewById(R.id.txtBudgetOver);
                    txtTodayDate = (TextView) findViewById(R.id.txtTodayDate);
                    txtTodayDate.setText(dfToday.format(new Date()).toUpperCase());

                    txtSelected = (TextView) findViewById(R.id.txtAddExpense);
                    txtSelected.setTextColor(0xFF000000);
                    txtSelected.setBackgroundColor(0xAAFFFFFF);

                    txtAddExpense = (TextView) findViewById(R.id.txtAddExpense);
                    txtAddExpenseCents = (TextView) findViewById(R.id.txtAddExpenseCents);
                    txtAddExpenseCategory = (TextView) findViewById(R.id.txtAddExpenseCategory);
                    txtAddExpenseDate = (TextView) findViewById(R.id.txtAddExpenseDate);
                    txtAddExpenseDate.setText(dfToday.format(new Date()));

                    txtAddExpenseThousand = (TextView) findViewById(R.id.txtAddExpenseThousand);
                    txtAddExpenseHundredThousand = (TextView) findViewById(R.id.txtAddExpenseHundredThousand);
                    txtAddExpenseThousand.setAlpha(0);
                    txtAddExpenseHundredThousand.setAlpha(0);

                    viewLineAddExpenseHundredThousand = findViewById(R.id.viewLineHundredThousand);
                    viewLineAddExpenseThousand = findViewById(R.id.viewLineThousand);

                    separator = (TextView) findViewById(R.id.separator);
                    separator.setText(localeSeparator);

                    seekAddExpense = (SeekArc) findViewById(R.id.seekAddExpense);
                    seekAddExpense.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {

                        @Override
                        public void onStopTrackingTouch(SeekArc seekArc) {
                        }

                        @Override
                        public void onStartTrackingTouch(SeekArc seekArc) {
                        }

                        @Override
                        public void onProgressChanged(SeekArc seekArc, int progress,
                                                      boolean fromUser) {

                            if (txtSelected.getId() == R.id.txtAddExpenseCents) {
                                revo = 0;
                            } else {
                                if (revo > 1) {
                                    revo = 1;
                                }
                            }

                            int amount = Math.round((float) ((progress + (revo * max)) / 100.0f));
                            if (amount > 99) {
                                amount = 99;
                            }

                            txtSelected.setText(zeroPad(amount));

                            int diff = progress - prevProgress;
                            if (diff > 4000) {
                                revo--;
                                revo = revo < 0 ? 0 : revo;
                            } else if (diff < -4000) {
                                revo++;
                            }
                            prevProgress = progress;
                        }
                    });

                    float total = getTotalExpensesWithDate(nowDate);
                    txtShowExpenses = (TextView) findViewById(R.id.txtShowExpenses);
                    txtShowExpenses.setText(format.format(total));

                    btnAddPersonal = (ImageButton) findViewById(R.id.btnAddPersonal);
                    btnAddHouse = (ImageButton) findViewById(R.id.btnAddHouse);
                    btnAddFood = (ImageButton) findViewById(R.id.btnAddFood);
                    btnAddTranspo = (ImageButton) findViewById(R.id.btnAddTranspo);
                    btnAddClothes = (ImageButton) findViewById(R.id.btnAddClothes);
                    btnAddFun = (ImageButton) findViewById(R.id.btnAddFun);
                    btnAddFamily = (ImageButton) findViewById(R.id.btnAddFamily);
                    btnAddMisc = (ImageButton) findViewById(R.id.btnAddMisc);
                    btnShowExpenseAddCategories = (ImageButton) findViewById(R.id.btnShowExpenseAddCategories);
                    btnShowExpenseAdd = (ImageButton) findViewById(R.id.btnShowExpenseAdd);

                    if(screenHeight() == 320) {

                    } else if (screenHeight() == 280) {
                        btnShowExpenseAdd.setTranslationY(-22f);
                        seekAddExpense.setScaleX(0.9f);
                        seekAddExpense.setScaleY(0.9f);

                    // moto360
                    } else {
                        btnShowExpenseAdd.setTranslationY(27f);
                    }

                    frmShowExpense = (FrameLayout) findViewById(R.id.frameLayoutShowExpense);
                    frmAddExpense = (FrameLayout) findViewById(R.id.frameLayoutAddExpense);
                    frmAddExpenseCategories = (FrameLayout) findViewById(R.id.frameLayoutAddExpenseCategories);
                    frmExpenseChart = (FrameLayout) findViewById(R.id.frameLayoutExpenseChart);
                    frmExpensesOverbudget = (FrameLayout) findViewById(R.id.frameLayoutExpenseOverBudget);
                    frmAddExpenseDate = (FrameLayout) findViewById(R.id.frameLayoutAddExpenseDate);

                    drawExpensesChart();

                    ArrayAdapter<String> dataAdapter;
                    List<String> list;

                    spinYY = (Spinner) findViewById(R.id.spinnerYY);
                    list = new ArrayList<String>();
                    for (int i = nowYear; i >= 1980; i--) {
                        list.add(Integer.toString(i));
                    }
                    dataAdapter = new ArrayAdapter<String>(WearExpenseTrackerActivity.this.context, android.R.layout.simple_spinner_item,list);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
                    spinYY.setAdapter(dataAdapter);
                    spinYY.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            int selectedYear = Integer.parseInt(adapterView.getSelectedItem().toString());
                            updateMonthAdapter(selectedYear);
                            updateDayDisplay();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    spinMM = (Spinner) findViewById(R.id.spinnerMM);
                    updateMonthAdapter(nowYear);
                    spinMM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            int selectedYear = Integer.parseInt(spinYY.getItemAtPosition(spinYY.getSelectedItemPosition()).toString());
                            int selectedMonth = getMonthInt(adapterView.getSelectedItem().toString());
                            updateDateAdapter(selectedYear, selectedMonth);
                            updateDayDisplay();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    spinDD = (Spinner) findViewById(R.id.spinnerDD);
                    spinDD.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            updateDayDisplay();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    updateDateAdapter(nowYear, nowMonth);

                    txtExpenseDay = (TextView) findViewById(R.id.txtAddExpenseDay);
                    updateDayDisplay();

                    break;

                case PAGE_HISTORY:
                    txtMonth = (TextView) findViewById(R.id.txtMonth);
                    txtNoTransaction = (TextView) findViewById(R.id.txtNoTransaction);
                    layoutDeleteBg = (FrameLayout) findViewById(R.id.layoutDeleteBg);

                    listTransactions = (ExpandableListView) findViewById(R.id.listTransaction);
                    updateListTransactions();
                    updateMonthText(txtMonth);

                    break;

                case PAGE_REVIEW:
                    frmReviewExpenseChart = (FrameLayout) findViewById(R.id.frameLayoutExpenseReviewChart);
                    frmReview = (FrameLayout) findViewById(R.id.frmLayoutReview);
                    frmReviewCategory = (FrameLayout) findViewById(R.id.frameLayoutReviewCategory);
                    btnShowReviewCategories = (FrameLayout) findViewById(R.id.btnShowReviewCategories);
                    btnShowReviewCategories.setOnLongClickListener(WearExpenseTrackerActivity.this);

                    txtReviewMonth = (TextView) findViewById(R.id.txtReviewMonth);
                    txtReviewTotal = (TextView) findViewById(R.id.txtReviewTotal);
                    txtReviewCategory = (TextView) findViewById(R.id.txtReviewCategory);
                    txtReviewPercentage = (TextView) findViewById(R.id.txtReviewPercentage);
                    txtReviewSelectCategory = (TextView) findViewById(R.id.txtReviewSelectCategory);

                    imgReviewBg = (ImageView) findViewById(R.id.imgReviewBg);
                    imgReviewEditCategory = (ImageView) findViewById(R.id.imgReviewEditCategory);

                    btnReviewTotal = (ImageButton) findViewById(R.id.btnReviewTotal);
                    btnReviewPersonal = (ImageButton) findViewById(R.id.btnReviewPersonal);
                    btnReviewHouse = (ImageButton) findViewById(R.id.btnReviewHouse);
                    btnReviewFood = (ImageButton) findViewById(R.id.btnReviewFood);
                    btnReviewTranspo = (ImageButton) findViewById(R.id.btnReviewTranspo);
                    btnReviewClothes = (ImageButton) findViewById(R.id.btnReviewClothes);
                    btnReviewFun = (ImageButton) findViewById(R.id.btnReviewFun);
                    btnReviewFamily = (ImageButton) findViewById(R.id.btnReviewFamily);
                    btnReviewMisc = (ImageButton) findViewById(R.id.btnReviewMisc);

                    viewLineReview = findViewById(R.id.viewLineReview);

                    updateMonthText(txtReviewMonth);
                    drawReviewExpensesChart(DEFAULT_REVIEW_CATEGORY);

                    break;

                case PAGE_BUDGET:
                    frmBudgetChart = (FrameLayout) findViewById(R.id.frameLayoutBudgetChart);
                    frmBudget = (FrameLayout) findViewById(R.id.frameLayoutBudget);
                    frmBudgetCategory = (FrameLayout) findViewById(R.id.frameLayoutBudgetCategory);
                    btnShowBudgetCategories = (FrameLayout) findViewById(R.id.btnShowBudgetCategories);
                    btnShowBudgetCategories.setOnLongClickListener(WearExpenseTrackerActivity.this);

                    frmBudgetButtons = (FrameLayout) findViewById(R.id.frameLayoutBudgetButtons);
                    if(screenHeight() == 320) {

                    } else if (screenHeight() == 280) {
                        frmBudgetButtons.setTranslationY(-29f);

                    // moto360
                    } else {
                        frmBudgetButtons.setTranslationY(21f);
                    }

                    frmLocked = (FrameLayout) findViewById(R.id.frameLayoutLocked);
                    boolean isFullVersion = prefs.getBoolean("com.aeustech.wearexpensetracker.fullVersion", false);
                    if (isFullVersion) {
                        frmLocked.setVisibility(View.GONE);
                    }

                    btnCheckPhone = (Button) findViewById(R.id.btnCheckPhone);
                    btnGoToExpenses = (ImageButton) findViewById(R.id.btnGoToExpenses);
                    btnGoToBudget = (ImageButton) findViewById(R.id.btnGoToBudget);
                    btnGoToRemaining = (ImageButton) findViewById(R.id.btnGoToRemaining);
                    txtBudgetTitle = (TextView) findViewById(R.id.txtRemainingTitle);
                    txtBudgetCategory = (TextView) findViewById(R.id.txtRemainingCategory);
                    txtBudgetSelectCategory = (TextView) findViewById(R.id.txtBudgetSelectCategory);
                    txtBudgetAmount  = (TextView) findViewById(R.id.txtRemainingAmount);
                    txtBudgetDays  = (TextView) findViewById(R.id.txtRemainingDays);

                    btnBudgetPersonal = (ImageButton) findViewById(R.id.btnBudgetPersonal);
                    btnBudgetHouse = (ImageButton) findViewById(R.id.btnBudgetHouse);
                    btnBudgetFood = (ImageButton) findViewById(R.id.btnBudgetFood);
                    btnBudgetTranspo = (ImageButton) findViewById(R.id.btnBudgetTranspo);
                    btnBudgetClothes = (ImageButton) findViewById(R.id.btnBudgetClothes);
                    btnBudgetFun = (ImageButton) findViewById(R.id.btnBudgetFun);
                    btnBudgetFamily = (ImageButton) findViewById(R.id.btnBudgetFamily);
                    btnBudgetMisc = (ImageButton) findViewById(R.id.btnBudgetMisc);

                    imgBudgetEditCategory = (ImageView) findViewById(R.id.imgBudgetEditCategory);
                    imgBudgetEditCategory.setColorFilter(Color.parseColor("#6CA7C0"));

                    updateBudgetView(btnGoToRemaining);

                    break;
            }



            return v;
        }
    }


    /* ---- FUNCTIONS ON UI DISPLAY ---- */

    public void buttonPressed(View v) {
        switch (v.getId()) {

            /* --- Buttons in selecting category --- */
            case R.id.btnAddPersonal:
                updateDefaultImageButtonAddCategories();
                pagerAdapter.txtAddExpenseCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_PERSONAL);
                pagerAdapter.btnAddPersonal.setImageResource(R.drawable.expense_cat1_personal);

                break;

            case R.id.btnAddHouse:
                updateDefaultImageButtonAddCategories();
                pagerAdapter.txtAddExpenseCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_HOUSE);
                pagerAdapter.btnAddHouse.setImageResource(R.drawable.expense_cat2_house);

                break;

            case R.id.btnAddFood:
                updateDefaultImageButtonAddCategories();
                pagerAdapter.txtAddExpenseCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FOOD);
                pagerAdapter.btnAddFood.setImageResource(R.drawable.expense_cat3_food);

                break;

            case R.id.btnAddTranspo:
                updateDefaultImageButtonAddCategories();
                pagerAdapter.txtAddExpenseCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_TRANSPORTATION);
                pagerAdapter.btnAddTranspo.setImageResource(R.drawable.expense_cat4_transpo);

                break;

            case R.id.btnAddClothes:
                updateDefaultImageButtonAddCategories();
                pagerAdapter.txtAddExpenseCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_CLOTHING);
                pagerAdapter.btnAddClothes.setImageResource(R.drawable.expense_cat5_clothes);

                break;

            case R.id.btnAddFun:
                updateDefaultImageButtonAddCategories();
                pagerAdapter.txtAddExpenseCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FUN);
                pagerAdapter.btnAddFun.setImageResource(R.drawable.expense_cat6_fun);

                break;

            case R.id.btnAddFamily:
                updateDefaultImageButtonAddCategories();
                pagerAdapter.txtAddExpenseCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FAMILY);
                pagerAdapter.btnAddFamily.setImageResource(R.drawable.expense_cat7_family);

                break;

            case R.id.btnAddMisc:
                updateDefaultImageButtonAddCategories();
                pagerAdapter.txtAddExpenseCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_MISCELLANEOUS);
                pagerAdapter.btnAddMisc.setImageResource(R.drawable.expense_cat8_misc);

                break;

            /* --- Function buttons, update views --- */

            /* --- TODAY page --- */
            case R.id.btnExpenseAddDate:
                updateTodayView(v);
                break;

            case R.id.btnDateOk:
                int selectedYear = Integer.parseInt(pagerAdapter.spinYY.getSelectedItem().toString());
                int selectedMonth = getMonthInt(pagerAdapter.spinMM.getSelectedItem().toString());
                int selectedDate = Integer.parseInt(pagerAdapter.spinDD.getSelectedItem().toString());

                Calendar c = Calendar.getInstance();
                c.set(Calendar.YEAR, selectedYear);
                c.set(Calendar.MONTH, selectedMonth);
                c.set(Calendar.DAY_OF_MONTH, selectedDate);

                String dateStr = dfToday.format(c.getTime());
                pagerAdapter.txtAddExpenseDate.setText(dateStr.toUpperCase());

                updateTodayView(v);

                break;


            case R.id.btnShowExpenseAdd:
                updateAddExpenseDefault();
                updateTodayView(v);
                break;

            case R.id.btnShowExpenseAddCategories:
                String strAmt = pagerAdapter.txtAddExpenseHundredThousand.getText().toString()
                        + pagerAdapter.txtAddExpenseThousand.getText().toString()
                        + pagerAdapter.txtAddExpense.getText().toString()
                        + "."
                        + pagerAdapter.txtAddExpenseCents.getText().toString();
                float amt = Float.parseFloat(strAmt);
                if (amt == 0) {
                    Toast.makeText(this, "Set amount", Toast.LENGTH_SHORT).show();
                } else {
                    updateTodayView(v);
                }
                break;

            case R.id.btnExpenseAdd:
                if (pagerAdapter.txtAddExpenseCategory.getText().equals(DEFAULT_CATEGORY)) {
                    Toast.makeText(this, "Set category", Toast.LENGTH_SHORT).show();
                } else {
                    String category = pagerAdapter.txtAddExpenseCategory.getText().toString();

                    // Check for budget
                    try {
                        float budgetAmount = 0, expensesAmount = 0;

                        database.open();
                        String whereClause = WearExpenseTrackerDatabase.BUDGET_CATEGORY + "='" + category + "'";
                        ArrayList<Budget> budgets = database.getBudget(whereClause);
                        database.close();

                        if (budgets.size() > 0) {
                            Budget budget = budgets.get(0);

                            Date startDate = dfDate.parse(budget.startDate);
                            c = Calendar.getInstance();
                            c.add(Calendar.DATE, 1);
                            Date now = c.getTime();

                            budgetAmount = budget.amount;

                            whereClause = WearExpenseTrackerDatabase.EXPENSE_DELETED + "=0 ";
                            whereClause += "AND ";
                            whereClause += WearExpenseTrackerDatabase.EXPENSE_DATE + "<='" + budget.endDate + "'";
                            whereClause += "AND ";
                            whereClause += WearExpenseTrackerDatabase.EXPENSE_DATE + ">='" + budget.startDate + "' ";
                            whereClause += "AND ";
                            whereClause += WearExpenseTrackerDatabase.EXPENSE_CATEGORY + "='" + category + "'";

                            database.open();
                            ArrayList<Expense> expenses = database.getExpenses(whereClause);
                            for (Expense e : expenses) {
                                expensesAmount += e.amount;
                            }
                            database.close();

                            String strAmount = pagerAdapter.txtAddExpenseHundredThousand.getText().toString()
                                    + pagerAdapter.txtAddExpenseThousand.getText().toString()
                                    + pagerAdapter.txtAddExpense.getText().toString()
                                    + "."
                                    + pagerAdapter.txtAddExpenseCents.getText().toString();

                            expensesAmount +=  Float.parseFloat(strAmount);

                            if (expensesAmount > budgetAmount) {
                                String msg = category;
                                msg += " budget limit reached!";
                                pagerAdapter.txtOverbudget.setText(msg);
                                pagerAdapter.frmExpensesOverbudget.setVisibility(View.VISIBLE);
                            } else {
                                insertExpense(v);
                                float percentage = expensesAmount / budgetAmount;
                                if (percentage >= 0.8f) {
                                    String msg = "Your ";
                                    msg += category;
                                    msg += " budget is almost at its limit.";
                                    Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
                                }
                            }
                        } else {
                            insertExpense(v);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                break;

            case R.id.btnCancelExpenseAdd:
                updateTodayView(v);
                break;

            case R.id.btnBackToInputAmount:
                updateTodayView(v);
                break;

            case R.id.btnContinueAddNo:
                updateTodayView(v);
                break;

            case R.id.btnContinueAddYes:
                insertExpense(v);
                break;

            case R.id.btnDateCancel:
                updateTodayView(v);
                break;



            /* --- REVIEW page --- */

            case R.id.btnBackToReview:
                pagerAdapter.frmReview.setVisibility(View.VISIBLE);
                pagerAdapter.frmReviewCategory.setVisibility(View.GONE);
                break;

            case R.id.btnReview:
                pagerAdapter.frmReview.setVisibility(View.VISIBLE);
                pagerAdapter.frmReviewCategory.setVisibility(View.GONE);

                updateDefaultImageButtonReviewCategories();
                drawReviewExpensesChart(pagerAdapter.txtReviewCategory.getText().toString());
                break;

            case R.id.btnReviewTotal:
                updateDefaultImageButtonReviewCategories();
                pagerAdapter.txtReviewCategory.setText(DEFAULT_REVIEW_CATEGORY);
                pagerAdapter.txtReviewSelectCategory.setText(DEFAULT_REVIEW_CATEGORY);
                pagerAdapter.btnReviewTotal.setImageResource(R.drawable.expense_total);

                break;

            case R.id.btnReviewPersonal:
                updateDefaultImageButtonReviewCategories();
                pagerAdapter.txtReviewCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_PERSONAL);
                pagerAdapter.txtReviewSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_PERSONAL);
                pagerAdapter.btnReviewPersonal.setImageResource(R.drawable.expense_cat1_personal);

                break;

            case R.id.btnReviewHouse:
                updateDefaultImageButtonReviewCategories();
                pagerAdapter.txtReviewCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_HOUSE);
                pagerAdapter.txtReviewSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_HOUSE);
                pagerAdapter.btnReviewHouse.setImageResource(R.drawable.expense_cat2_house);

                break;

            case R.id.btnReviewFood:
                updateDefaultImageButtonReviewCategories();
                pagerAdapter.txtReviewCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FOOD);
                pagerAdapter.txtReviewSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FOOD);
                pagerAdapter.btnReviewFood.setImageResource(R.drawable.expense_cat3_food);

                break;

            case R.id.btnReviewTranspo:
                updateDefaultImageButtonReviewCategories();
                pagerAdapter.txtReviewCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_TRANSPORTATION);
                pagerAdapter.txtReviewSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_TRANSPORTATION);
                pagerAdapter.btnReviewTranspo.setImageResource(R.drawable.expense_cat4_transpo);

                break;

            case R.id.btnReviewClothes:
                updateDefaultImageButtonReviewCategories();
                pagerAdapter.txtReviewCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_CLOTHING);
                pagerAdapter.txtReviewSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_CLOTHING);
                pagerAdapter.btnReviewClothes.setImageResource(R.drawable.expense_cat5_clothes);

                break;

            case R.id.btnReviewFun:
                updateDefaultImageButtonReviewCategories();
                pagerAdapter.txtReviewCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FUN);
                pagerAdapter.txtReviewSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FUN);
                pagerAdapter.btnReviewFun.setImageResource(R.drawable.expense_cat6_fun);

                break;

            case R.id.btnReviewFamily:
                updateDefaultImageButtonReviewCategories();
                pagerAdapter.txtReviewCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FAMILY);
                pagerAdapter.txtReviewSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FAMILY);
                pagerAdapter.btnReviewFamily.setImageResource(R.drawable.expense_cat7_family);

                break;

            case R.id.btnReviewMisc:
                updateDefaultImageButtonReviewCategories();
                pagerAdapter.txtReviewCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_MISCELLANEOUS);
                pagerAdapter.txtReviewSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_MISCELLANEOUS);
                pagerAdapter.btnReviewMisc.setImageResource(R.drawable.expense_cat8_misc);

                break;



            /* --- BUDGET PAGE --- */

            case R.id.btnGoToBudget:
                pagerAdapter.btnGoToBudget.setVisibility(View.GONE);
                pagerAdapter.btnGoToExpenses.setVisibility(View.VISIBLE);
                pagerAdapter.btnGoToRemaining.setVisibility(View.GONE);
                updateBudgetView(v);

                break;

            case R.id.btnGoToExpenses:
                pagerAdapter.btnGoToBudget.setVisibility(View.GONE);
                pagerAdapter.btnGoToExpenses.setVisibility(View.GONE);
                pagerAdapter.btnGoToRemaining.setVisibility(View.VISIBLE);
                updateBudgetView(v);
                break;

            case R.id.btnGoToRemaining:
                pagerAdapter.btnGoToBudget.setVisibility(View.VISIBLE);
                pagerAdapter.btnGoToExpenses.setVisibility(View.GONE);
                pagerAdapter.btnGoToRemaining.setVisibility(View.GONE);
                updateBudgetView(v);
                break;


            case R.id.btnShowBudgetCategories:
                pagerAdapter.frmBudget.setVisibility(View.GONE);
                pagerAdapter.frmBudgetCategory.setVisibility(View.VISIBLE);
                break;

            case R.id.btnBackToBudget:
                pagerAdapter.frmBudget.setVisibility(View.VISIBLE);
                pagerAdapter.frmBudgetCategory.setVisibility(View.GONE);
                break;

            case R.id.btnBudget:
                pagerAdapter.frmBudget.setVisibility(View.VISIBLE);
                pagerAdapter.frmBudgetCategory.setVisibility(View.GONE);

                String title = pagerAdapter.txtBudgetTitle.getText().toString();
                if (title.equals("BALANCE")) {
                    updateBudgetView(pagerAdapter.btnGoToRemaining);
                } else if (title.equals("EXPENSES")) {
                    updateBudgetView(pagerAdapter.btnGoToExpenses);
                } else if (title.equals("BUDGET")) {
                    updateBudgetView(pagerAdapter.btnGoToBudget);
                }

                //drawReviewExpensesChart(pagerAdapter.txtReviewCategory.getText().toString());
                break;

            case R.id.btnBudgetPersonal:
                updateDefaultImageButtonBudgetCategories();
                pagerAdapter.txtBudgetCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_PERSONAL);
                pagerAdapter.txtBudgetSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_PERSONAL);
                pagerAdapter.btnBudgetPersonal.setImageResource(R.drawable.expense_cat1_personal);

                break;

            case R.id.btnBudgetHouse:
                updateDefaultImageButtonBudgetCategories();
                pagerAdapter.txtBudgetCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_HOUSE);
                pagerAdapter.txtBudgetSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_HOUSE);
                pagerAdapter.btnBudgetHouse.setImageResource(R.drawable.expense_cat2_house);

                break;

            case R.id.btnBudgetFood:
                updateDefaultImageButtonBudgetCategories();
                pagerAdapter.txtBudgetCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FOOD);
                pagerAdapter.txtBudgetSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FOOD);
                pagerAdapter.btnBudgetFood.setImageResource(R.drawable.expense_cat3_food);

                break;

            case R.id.btnBudgetTranspo:
                updateDefaultImageButtonBudgetCategories();
                pagerAdapter.txtBudgetCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_TRANSPORTATION);
                pagerAdapter.txtBudgetSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_TRANSPORTATION);
                pagerAdapter.btnBudgetTranspo.setImageResource(R.drawable.expense_cat4_transpo);

                break;

            case R.id.btnBudgetClothes:
                updateDefaultImageButtonBudgetCategories();
                pagerAdapter.txtBudgetCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_CLOTHING);
                pagerAdapter.txtBudgetSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_CLOTHING);
                pagerAdapter.btnBudgetClothes.setImageResource(R.drawable.expense_cat5_clothes);

                break;

            case R.id.btnBudgetFun:
                updateDefaultImageButtonBudgetCategories();
                pagerAdapter.txtBudgetCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FUN);
                pagerAdapter.txtBudgetSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FUN);
                pagerAdapter.btnBudgetFun.setImageResource(R.drawable.expense_cat6_fun);

                break;

            case R.id.btnBudgetFamily:
                updateDefaultImageButtonBudgetCategories();
                pagerAdapter.txtBudgetCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FAMILY);
                pagerAdapter.txtBudgetSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FAMILY);
                pagerAdapter.btnBudgetFamily.setImageResource(R.drawable.expense_cat7_family);

                break;

            case R.id.btnBudgetMisc:
                updateDefaultImageButtonBudgetCategories();
                pagerAdapter.txtBudgetCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_MISCELLANEOUS);
                pagerAdapter.txtBudgetSelectCategory.setText(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_MISCELLANEOUS);
                pagerAdapter.btnBudgetMisc.setImageResource(R.drawable.expense_cat8_misc);

                break;

            case R.id.frameLayoutLocked:
                pagerAdapter.frmBudgetButtons.setVisibility(View.INVISIBLE);
                pagerAdapter.btnCheckPhone.setVisibility(View.VISIBLE);
                break;

            case R.id.btnCheckPhone:
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        sendHandheldMessage("goToInAppPage/", "");
                        return null;
                    }
                }.execute();
                pagerAdapter.frmBudgetButtons.setVisibility(View.VISIBLE);
                pagerAdapter.btnCheckPhone.setVisibility(View.GONE);


                break;



            /*case R.id.btnShowEditBudget:
                pager.setPagingEnabled(false);

                if (pagerAdapter.txtSelected != pagerAdapter.txtEditBudget && pagerAdapter.txtSelected != pagerAdapter.txtEditBudgetCents) {
                    pagerAdapter.txtSelected = pagerAdapter.txtEditBudget;
                }
                pagerAdapter.txtSelected.setTextColor(0xFF000000);
                pagerAdapter.txtSelected.setBackgroundColor(0xAAFFFFFF);

                pagerAdapter.frmShowBudget.setVisibility(View.GONE);
                pagerAdapter.frmEditBudget.setVisibility(View.VISIBLE);

                break;

            case R.id.btnEditBudget:
                pager.setPagingEnabled(true);
                pagerAdapter.frmShowBudget.setVisibility(View.VISIBLE);
                pagerAdapter.frmEditBudget.setVisibility(View.GONE);

                try {

                    String strAmount = pagerAdapter.txtEditBudget.getText().toString()
                            + "."
                            + pagerAdapter.txtEditBudgetCents.getText().toString();

                    Budget budget = getBudgetWithMonthYear(transactionYear, transactionMonth);
                    budget.amount = Float.parseFloat(strAmount);

                    database.open();
                    database.updateBudget(budget);
                    database.close();

                    updateAllViewsWhenMonthChanges(0);

                } catch (Exception e) {
                    LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, e.getLocalizedMessage());
                }

                break;

            case R.id.btnBudgetMonthNext:
                updateAllViewsWhenMonthChanges(1);
                break;

            case R.id.btnBudgetMonthPrev:
                updateAllViewsWhenMonthChanges(-1);
                break;*/

            /*case R.id.btnReviewNextMonth:
                updateAllViewsWhenMonthChanges(1);
                break;

            case R.id.btnReviewPrevMonth:
                updateAllViewsWhenMonthChanges(-1);
                break;*/


        }
    }

    public void textViewSelected(View v) {
        if (v.getId() == R.id.txtAddExpenseDate) {
            //DatePickerFragment fragment = new DatePickerFragment();
            //fragment.show(getFragmentManager(), "datePicker");
            updateTodayView(v);
        } else {
            if (v.getId() == R.id.txtAddExpenseHundredThousand || v.getId() == R.id.txtAddExpenseThousand) {
                v.setAlpha(255f);

                if (v.getId() == R.id.txtAddExpenseThousand) {
                    pagerAdapter.viewLineAddExpenseThousand.setVisibility(View.GONE);

                    if (pagerAdapter.txtAddExpenseHundredThousand.getText().toString().equals("00")) {
                        pagerAdapter.txtAddExpenseHundredThousand.setAlpha(0);
                        pagerAdapter.viewLineAddExpenseHundredThousand.setVisibility(View.VISIBLE);
                    }
                } else {
                    int value = Integer.parseInt(pagerAdapter.txtAddExpenseThousand.getText().toString());
                    pagerAdapter.txtAddExpenseThousand.setText(zeroPad(value));

                    pagerAdapter.txtAddExpenseThousand.setAlpha(255f);
                    pagerAdapter.viewLineAddExpenseHundredThousand.setVisibility(View.GONE);
                    pagerAdapter.viewLineAddExpenseThousand.setVisibility(View.GONE);
                }
            } else {
                int amtHundredThousand = Integer.parseInt(pagerAdapter.txtAddExpenseHundredThousand.getText().toString());
                if (amtHundredThousand == 0) {
                    pagerAdapter.txtAddExpenseHundredThousand.setAlpha(0);
                    pagerAdapter.viewLineAddExpenseHundredThousand.setVisibility(View.VISIBLE);
                } else {
                    pagerAdapter.txtAddExpenseHundredThousand.setText(oneDigitPad(amtHundredThousand));
                }

                int amtThousand = Integer.parseInt(pagerAdapter.txtAddExpenseThousand.getText().toString());
                if (amtHundredThousand == 0) {
                    if (amtThousand == 0) {
                        pagerAdapter.txtAddExpenseThousand.setAlpha(0);
                        pagerAdapter.viewLineAddExpenseThousand.setVisibility(View.VISIBLE);
                    } else {
                        pagerAdapter.txtAddExpenseThousand.setText(oneDigitPad(amtThousand));
                    }
                }

                int amt = Integer.parseInt(pagerAdapter.txtAddExpense.getText().toString());
                if (amtHundredThousand == 0 && amtThousand == 0) {
                    if (amt > 0) {
                        pagerAdapter.txtAddExpense.setText(oneDigitPad(amt));
                    }
                }
            }

            pagerAdapter.txtSelected.setTextColor(0xFFFFFFFF);
            pagerAdapter.txtSelected.setBackgroundColor(0x00000000);
            pagerAdapter.txtSelected.setOnLongClickListener(null);

            ((TextView) v).setTextColor(0xFF000000);
            ((TextView) v).setBackgroundColor(0xAAFFFFFF);
            v.setOnLongClickListener(WearExpenseTrackerActivity.this);

            pagerAdapter.txtSelected = (TextView) v;

            if (v.getId() == R.id.txtAddExpense || v.getId() == R.id.txtAddExpenseCents
                    || v.getId() == R.id.txtAddExpenseThousand || v.getId() == R.id.txtAddExpenseHundredThousand) {
                if (v.getId() == R.id.txtAddExpenseCents) {
                    pagerAdapter.seekAddExpense.setMax(9900);
                    max = 9900;
                } else {
                    pagerAdapter.seekAddExpense.setMax(5000);
                    max = 5000;
                }
            }
        }
    }

    public void drawExpensesChart() {
        double[] values = new double[8];
        String[] categoriesForChart = categories.clone();
        int[] colorsForChart = colors.clone();

        float overAllTotal = getTotalExpensesWithDate(nowDate);

        if (overAllTotal > 0) {
            for (int i = 0; i < categories.length; i ++) {
                String whereClause = WearExpenseTrackerDatabase.EXPENSE_DELETED + "=0 ";
                whereClause += "AND ";
                whereClause += WearExpenseTrackerDatabase.EXPENSE_CATEGORY + "='" + categories[i] + "'";

                try {
                    database.open();
                    ArrayList<Expense> expenses = database.getExpenses(whereClause);

                    float total = 0;
                    for (Expense e : expenses) {
                        Date now = new Date();
                        if (dfDate.format(now).equals(e.date)) {
                            total += e.amount;
                        }
                    }
                    values[i] = (double) total;

                    database.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            categoriesForChart[0] = "---";
            colorsForChart[0] = Color.parseColor("#989a92");
            values[0]= 100;
        }

        DefaultRenderer todayRenderer = buildCategoryRenderer(colorsForChart);
        MultipleCategorySeries todayCategorySeries = new MultipleCategorySeries("HEADTRAUMA");
        todayCategorySeries.add(categoriesForChart, values);
        GraphicalView todayChartView = ChartFactory.getDoughnutChartView(context, todayCategorySeries, todayRenderer);

        pagerAdapter.frmExpenseChart.removeAllViews();
        pagerAdapter.frmExpenseChart.addView(todayChartView, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));

        updateTotalToday();
    }

    public void drawBudgetChart(String category, float expensesAmt, float remainingAmt, boolean isBudgetSet) {
        double[] values = new double[2];
        int[] budgetColors = new int[2];
        String[] categoriesForBalance = {"Expenses", "Balance"};

        if (isBudgetSet) {
            if (remainingAmt < 0) {
                remainingAmt = 0;
            }

            int categoryIndex = 0;
            for (int i=0; i<categories.length; i++) {
                if (category.equals(categories[i])) {
                    categoryIndex = i;
                    break;
                }
            }
            budgetColors[0] = colors[categoryIndex];
            budgetColors[1] = Color.parseColor("#989a92");
            values[0] = expensesAmt;
            values[1] = remainingAmt;
        } else {
            budgetColors[0] = Color.parseColor("#989a92");
            budgetColors[1] = Color.parseColor("#989a92");
            values[0] = 100;
            values[1] = 0;
        }

        DefaultRenderer renderer = buildCategoryRenderer(budgetColors);
        MultipleCategorySeries categorySeries = new MultipleCategorySeries("HEADTRAUMA");
        categorySeries.add(categoriesForBalance, values);

        GraphicalView mChartView = ChartFactory.getDoughnutChartView(context, categorySeries, renderer);
        pagerAdapter.frmBudgetChart.removeAllViews();
        pagerAdapter.frmBudgetChart.addView(mChartView, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));
    }

    public void drawReviewExpensesChart(String category) {
        ArrayList<Expense> expenses = getMonthlyExpenses(transactionYear, transactionMonth);
        double[] values = new double[8];

        float totalThisMonth = 0;
        if (expenses.size() > 0) {
            colors[0] = Color.parseColor("#29ABE2");
            for (int i = 0; i < categories.length; i++) {
                float totalForCategory = 0;
                for (Expense e : expenses) {
                    if (e.category.equals(categories[i])) {
                        totalForCategory += e.amount;
                    }
                }
                values[i] = totalForCategory;
                totalThisMonth += totalForCategory;
            }
        } else {
            values[0] = 100;
            colors[0] = Color.parseColor("#989a92");
        }

        DefaultRenderer renderer = buildCategoryRenderer(colors);
        MultipleCategorySeries categorySeries = new MultipleCategorySeries("HEADTRAUMA");
        categorySeries.add(categories, values);

        GraphicalView mChartView = ChartFactory.getDoughnutChartView(context, categorySeries, renderer);
        pagerAdapter.frmReviewExpenseChart.removeAllViews();
        pagerAdapter.frmReviewExpenseChart.addView(mChartView, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));

        updateReviewAmount(category, totalThisMonth, values);
    }

    protected DefaultRenderer buildCategoryRenderer(int[] colors) {
        DefaultRenderer renderer = new DefaultRenderer();
        for (int color : colors) {
            SimpleSeriesRenderer r = new SimpleSeriesRenderer();
            r.setColor(color);
            renderer.addSeriesRenderer(r);
        }
        renderer.setShowLabels(false);
        renderer.setShowLegend(false);
        renderer.setInScroll(true);
        renderer.setStartAngle(270);
        renderer.setPanEnabled(false);// Disable User Interaction
        renderer.setScale((float) 1.2);
        renderer.setApplyBackgroundColor(false);

        return renderer;
    }


    public void updateMonthAdapter(int selectedYear) {
        int month = (selectedYear == nowYear) ? nowMonth : 11;
        ArrayList list = new ArrayList<String>();
        for (int i = month; i >= 0; i--) {
            list.add(getMonthString(i));
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(WearExpenseTrackerActivity.this.context, android.R.layout.simple_spinner_item,list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        pagerAdapter.spinMM.setAdapter(dataAdapter);
    }

    public void updateDateAdapter(int selectedYear, int selectedMonth) {
        int maxDate = 0;
        Calendar c = Calendar.getInstance();
        if (selectedYear == nowYear && selectedMonth == nowMonth) {
            maxDate = c.get(Calendar.DAY_OF_MONTH);
        } else {
            c.set(Calendar.YEAR, selectedYear);
            c.set(Calendar.MONTH, selectedMonth);
            c.set(Calendar.DAY_OF_MONTH, 1);
            maxDate = c.getActualMaximum(Calendar.DATE);
        }

        ArrayList list = new ArrayList<String>();
        for (int i = maxDate; i > 0; i--) {
            list.add(Integer.toString(i));
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(WearExpenseTrackerActivity.this.context, android.R.layout.simple_spinner_item,list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        pagerAdapter.spinDD.setAdapter(dataAdapter);
    }

    public void updateDayDisplay() {
        int selectedYear = Integer.parseInt(pagerAdapter.spinYY.getSelectedItem().toString());
        int selectedMonth = getMonthInt(pagerAdapter.spinMM.getSelectedItem().toString());
        int selectedDate = Integer.parseInt(pagerAdapter.spinDD.getSelectedItem().toString());

        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, selectedYear);
        c.set(Calendar.MONTH, selectedMonth);
        c.set(Calendar.DAY_OF_MONTH, selectedDate);

        String day = dfDayComplete.format(c.getTime());
        pagerAdapter.txtExpenseDay.setText(day);
    }


    public void updateAllViewsOnDataChanged(int add) {
        // Set add = 1, previous month
        // Set add = 0, when month is not changed
        // Set add = 1, next month

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, transactionMonth);
        cal.set(Calendar.YEAR, transactionYear);
        cal.add(Calendar.MONTH, add);

        transactionMonth = cal.get(Calendar.MONTH);
        transactionYear = cal.get(Calendar.YEAR);

        // Views in Today page
        drawExpensesChart();

        String title = pagerAdapter.txtBudgetTitle.getText().toString();
        if (title.equals("BALANCE")) {
            updateBudgetView(pagerAdapter.btnGoToRemaining);
        } else if (title.equals("EXPENSES")) {
            updateBudgetView(pagerAdapter.btnGoToExpenses);
        } else if (title.equals("BUDGET")) {
            updateBudgetView(pagerAdapter.btnGoToBudget);
        }

        // Views in Transaction page
        updateMonthText(pagerAdapter.txtMonth);
        updateListTransactions();

        // Views in Review page
        updateMonthText(pagerAdapter.txtReviewMonth);
        drawReviewExpensesChart(DEFAULT_REVIEW_CATEGORY);
    }

    public void updateTotalToday() {
        float total = getTotalExpensesWithDate(nowDate);
        pagerAdapter.txtShowExpenses.setText(format.format(total));
    }

    void updateDefaultImageButtonAddCategories() {
        pagerAdapter.btnAddPersonal.setImageResource(R.drawable.expense_cat1_personal_0);
        pagerAdapter.btnAddHouse.setImageResource(R.drawable.expense_cat2_house_0);
        pagerAdapter.btnAddFood.setImageResource(R.drawable.expense_cat3_food_0);
        pagerAdapter.btnAddTranspo.setImageResource(R.drawable.expense_cat4_transpo_0);
        pagerAdapter.btnAddClothes.setImageResource(R.drawable.expense_cat5_clothes_0);
        pagerAdapter.btnAddFun.setImageResource(R.drawable.expense_cat6_fun_0);
        pagerAdapter.btnAddFamily.setImageResource(R.drawable.expense_cat7_family_0);
        pagerAdapter.btnAddMisc.setImageResource(R.drawable.expense_cat8_misc_0);
    }

    void updateDefaultImageButtonReviewCategories() {
        pagerAdapter.btnReviewTotal.setImageResource(R.drawable.expense_total_0);
        pagerAdapter.btnReviewPersonal.setImageResource(R.drawable.expense_cat1_personal_0);
        pagerAdapter.btnReviewHouse.setImageResource(R.drawable.expense_cat2_house_0);
        pagerAdapter.btnReviewFood.setImageResource(R.drawable.expense_cat3_food_0);
        pagerAdapter.btnReviewTranspo.setImageResource(R.drawable.expense_cat4_transpo_0);
        pagerAdapter.btnReviewClothes.setImageResource(R.drawable.expense_cat5_clothes_0);
        pagerAdapter.btnReviewFun.setImageResource(R.drawable.expense_cat6_fun_0);
        pagerAdapter.btnReviewFamily.setImageResource(R.drawable.expense_cat7_family_0);
        pagerAdapter.btnReviewMisc.setImageResource(R.drawable.expense_cat8_misc_0);
    }

    void updateDefaultImageButtonBudgetCategories() {
        pagerAdapter.btnBudgetPersonal.setImageResource(R.drawable.expense_cat1_personal_0);
        pagerAdapter.btnBudgetHouse.setImageResource(R.drawable.expense_cat2_house_0);
        pagerAdapter.btnBudgetFood.setImageResource(R.drawable.expense_cat3_food_0);
        pagerAdapter.btnBudgetTranspo.setImageResource(R.drawable.expense_cat4_transpo_0);
        pagerAdapter.btnBudgetClothes.setImageResource(R.drawable.expense_cat5_clothes_0);
        pagerAdapter.btnBudgetFun.setImageResource(R.drawable.expense_cat6_fun_0);
        pagerAdapter.btnBudgetFamily.setImageResource(R.drawable.expense_cat7_family_0);
        pagerAdapter.btnBudgetMisc.setImageResource(R.drawable.expense_cat8_misc_0);
    }

    public void updateMonthText(TextView txtMonth) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, transactionMonth);
        cal.set(Calendar.YEAR, transactionYear);
        String strMonth = dfMonth.format(cal.getTime());
        txtMonth.setText(strMonth.toUpperCase());
    }

    public void updateTodayView(View v) {
        // Display view showing Today's total expenses
        // btnExpenseAdd = user is finished adding an expense transaction
        // btnCancelExpenseAdd = user do not want to add
        if (v.getId() == R.id.btnExpenseAdd || v.getId() == R.id.btnCancelExpenseAdd || v.getId() == R.id.btnContinueAddNo || v.getId() == R.id.btnContinueAddYes) {
            pager.setPagingEnabled(true);

            pagerAdapter.frmShowExpense.setVisibility(View.VISIBLE);
            pagerAdapter.frmExpenseChart.setVisibility(View.VISIBLE);
            pagerAdapter.frmAddExpense.setVisibility(View.GONE);
            pagerAdapter.frmAddExpenseDate.setVisibility(View.GONE);
            pagerAdapter.frmAddExpenseCategories.setVisibility(View.GONE);
            pagerAdapter.frmExpensesOverbudget.setVisibility(View.GONE);

            updateDefaultImageButtonAddCategories();
        }

        // Display view for putting the expense amount
        // btnShowExpenseAdd = user clicks + while on page is showing Today's total, entering amount
        // btnBackToInputAmount = user clicks < while on Selecting Category
        if (v.getId() == R.id.btnShowExpenseAdd || v.getId() == R.id.btnBackToInputAmount
                || v.getId() == R.id.btnDateCancel || v.getId() == R.id.btnDateOk) {
            pager.setPagingEnabled(false);

            if (pagerAdapter.txtSelected != pagerAdapter.txtAddExpense
                    && pagerAdapter.txtSelected != pagerAdapter.txtAddExpenseCents
                    && pagerAdapter.txtSelected != pagerAdapter.txtAddExpenseHundredThousand
                    && pagerAdapter.txtSelected != pagerAdapter.txtAddExpenseThousand) {
                pagerAdapter.txtSelected = pagerAdapter.txtAddExpense;
            }
            pagerAdapter.txtSelected.setTextColor(0xFF000000);
            pagerAdapter.txtSelected.setBackgroundColor(0xAAFFFFFF);

            pagerAdapter.frmShowExpense.setVisibility(View.GONE);
            pagerAdapter.frmExpenseChart.setVisibility(View.GONE);
            pagerAdapter.frmAddExpense.setVisibility(View.VISIBLE);
            pagerAdapter.frmAddExpenseCategories.setVisibility(View.GONE);
            pagerAdapter.frmAddExpenseDate.setVisibility(View.GONE);
            pagerAdapter.frmExpensesOverbudget.setVisibility(View.GONE);
        }

        if (v.getId() == R.id.btnShowExpenseAddCategories) {
            pager.setPagingEnabled(false);

            pagerAdapter.frmShowExpense.setVisibility(View.GONE);
            pagerAdapter.frmExpenseChart.setVisibility(View.GONE);
            pagerAdapter.frmAddExpense.setVisibility(View.GONE);
            pagerAdapter.frmAddExpenseCategories.setVisibility(View.VISIBLE);
            pagerAdapter.frmAddExpenseDate.setVisibility(View.GONE);
            pagerAdapter.frmExpensesOverbudget.setVisibility(View.GONE);
        }

        if (v.getId() == R.id.txtAddExpenseDate || v.getId() == R.id.btnExpenseAddDate) {
            pager.setPagingEnabled(false);

            pagerAdapter.frmShowExpense.setVisibility(View.GONE);
            pagerAdapter.frmExpenseChart.setVisibility(View.GONE);
            pagerAdapter.frmAddExpense.setVisibility(View.GONE);
            pagerAdapter.frmAddExpenseCategories.setVisibility(View.GONE);
            pagerAdapter.frmAddExpenseDate.setVisibility(View.VISIBLE);
            pagerAdapter.frmExpensesOverbudget.setVisibility(View.GONE);
        }
    }

    public void updateAddExpenseDefault() {
        pagerAdapter.txtAddExpense.setText("00");
        pagerAdapter.txtAddExpenseThousand.setText("00");
        pagerAdapter.txtAddExpenseHundredThousand.setText("00");
        pagerAdapter.txtAddExpenseCents.setText("00");

        pagerAdapter.txtAddExpenseCategory.setText(DEFAULT_CATEGORY);
        pagerAdapter.txtAddExpenseDate.setText(dfToday.format(new Date()).toUpperCase());
    }

    public void updateListTransactions() {
        SparseArray<Group> groups = groupTransactionByDate(transactionMonth, transactionYear);

        MyExpandableListAdapter adapter = new MyExpandableListAdapter(WearExpenseTrackerActivity.this, groups);
        pagerAdapter.listTransactions.setAdapter(adapter);

        if (groups.size() == 0) {
            pagerAdapter.txtNoTransaction.setVisibility(View.VISIBLE);
        } else {
            pagerAdapter.txtNoTransaction.setVisibility(View.GONE);
        }
    }

    public void updateBudgetView(View v) {
        final String category = pagerAdapter.txtBudgetCategory.getText().toString();
        float amtShown = 0;
        boolean isBudgetSet = false;
        float remainingAmount = 0, budgetAmount = 0, expensesAmount = 0;
        String daysStr = "";

        try {
            database.open();

            String whereClause = WearExpenseTrackerDatabase.BUDGET_CATEGORY + "='" + category + "'";
            ArrayList<Budget> budgets = database.getBudget(whereClause);

            if (budgets.size() > 0) {
                isBudgetSet = true;
                Budget budget = budgets.get(0);

                // Update the budget
                Calendar c = Calendar.getInstance();
                c.setTime(dfDate.parse(budget.endDate));
                c.add(Calendar.DATE, 1);

                Date dateToUpdate = c.getTime();
                Date now = new Date();

                if (now.after(dateToUpdate)) {

                    String startDate = dfDate.format(new Date());
                    Calendar cal = Calendar.getInstance();
                    if (budget.period.equals("Weekly")) {
                        cal.add(Calendar.DATE, 6);
                    } else if (budget.period.equals("Monthly")) {
                        cal.add(Calendar.DATE, 29);
                    }
                    String endDate = dfDate.format(cal.getTime());

                    budget.startDate = startDate;
                    budget.endDate = endDate;

                    database.updateBudget(budget);
                }

                // Get the updated budget
                Date startDate = dfDate.parse(budget.startDate);
                c = Calendar.getInstance();
                c.add(Calendar.DATE, 1);
                now = c.getTime();

                long diff = (now.getTime() - startDate.getTime()) ;
                daysStr = String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
                daysStr += " out of ";

                if (budget.period.equals("Daily")) {
                    daysStr = "Daily";
                } else if (budget.period.equals("Weekly")) {
                    daysStr += "7 days";
                } else if (budget.period.equals("Monthly")) {
                    daysStr += "30 days";
                }
                budgetAmount = budget.amount;

                whereClause = WearExpenseTrackerDatabase.EXPENSE_DELETED + "=0 ";
                whereClause += "AND ";
                whereClause += WearExpenseTrackerDatabase.EXPENSE_DATE + "<='" + budget.endDate + "'";
                whereClause += "AND ";
                whereClause += WearExpenseTrackerDatabase.EXPENSE_DATE + ">='" + budget.startDate + "' ";
                whereClause += "AND ";
                whereClause += WearExpenseTrackerDatabase.EXPENSE_CATEGORY + "='" + category + "'";
                LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, whereClause);

                ArrayList<Expense> expenses = database.getExpenses(whereClause);
                for (Expense e : expenses) {
                    expensesAmount += e.amount;
                }

                remainingAmount = budgetAmount - expensesAmount;
            } else {
                // TO DO: No budget yet
            }
            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (v.getId() == R.id.btnGoToBudget) {
            amtShown = budgetAmount;
            pagerAdapter.txtBudgetTitle.setText("BUDGET");
            pagerAdapter.txtBudgetAmount.setText(format.format(budgetAmount));
        } else if (v.getId() == R.id.btnGoToExpenses) {
            amtShown = expensesAmount;
            pagerAdapter.txtBudgetTitle.setText("EXPENSES");
            pagerAdapter.txtBudgetAmount.setText(format.format(expensesAmount));
        } else if (v.getId() == R.id.btnGoToRemaining) {
            amtShown = remainingAmount;
            pagerAdapter.txtBudgetTitle.setText("BALANCE");
            pagerAdapter.txtBudgetAmount.setText(format.format(remainingAmount));
        }

        if (amtShown < 0) {
            pagerAdapter.txtBudgetAmount.setTextColor(Color.RED);
        } else {
            pagerAdapter.txtBudgetAmount.setTextColor(Color.parseColor("#6CA7C0"));
        }

        if (!isBudgetSet) {
            pagerAdapter.txtBudgetAmount.setText("- - - - -");
        }
        pagerAdapter.txtBudgetDays.setText(daysStr);
        drawBudgetChart(category, expensesAmount, remainingAmount, isBudgetSet);
    }

    public void updateReviewAmount(String category, double totalThisMonth, double[] values) {
        pagerAdapter.txtReviewCategory.setText(category);

        double amount = totalThisMonth;
        int percentage = 0;

        if (category.equals(DEFAULT_REVIEW_CATEGORY)) {
            pagerAdapter.txtReviewPercentage.setVisibility(View.GONE);

            pagerAdapter.txtReviewMonth.setTextColor(Color.parseColor("#6CA7C0"));
            pagerAdapter.txtReviewTotal.setTextColor(Color.parseColor("#6CA7C0"));
            pagerAdapter.txtReviewCategory.setTextColor(Color.parseColor("#6CA7C0"));
            pagerAdapter.viewLineReview.setBackgroundColor(Color.parseColor("#6CA7C0"));
            pagerAdapter.imgReviewBg.setColorFilter(null);
            pagerAdapter.imgReviewEditCategory.setColorFilter(Color.parseColor("#6CA7C0"));
        } else {
            int i;
            for (i = 0; i < categories.length; i++) {
                if (category.equals(categories[i])) {
                    break;
                }
            }
            amount = values[i];
            percentage = (int) Math.round((amount / totalThisMonth) * 100);

            pagerAdapter.txtReviewPercentage.setVisibility(View.VISIBLE);
            pagerAdapter.txtReviewPercentage.setText(percentage + "%");

            pagerAdapter.txtReviewMonth.setTextColor(Color.parseColor("#FFFFFF"));
            pagerAdapter.txtReviewTotal.setTextColor(Color.parseColor("#FFFFFF"));
            pagerAdapter.txtReviewCategory.setTextColor(Color.parseColor("#FFFFFF"));
            pagerAdapter.txtReviewPercentage.setTextColor(Color.parseColor("#FFFFFF"));
            pagerAdapter.viewLineReview.setBackgroundColor(Color.parseColor("#FFFFFF"));
            pagerAdapter.imgReviewBg.setColorFilter(colors[i]);
            pagerAdapter.imgReviewEditCategory.setColorFilter(Color.parseColor("#FFFFFF"));
        }
        pagerAdapter.txtReviewTotal.setText(format.format(amount));
    }

    /* ---- FUNCTIONS USED IN GETTING VALUES IN DATABASE --- */

    public SparseArray<Group> groupTransactionByDate(int month, int year) {
        SparseArray<Group> groups = new SparseArray<Group>();

        Calendar cal = Calendar.getInstance();
        cal.set(year, month, 1);
        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.set(year, month, maxDay);
        int myMonth = cal.get(Calendar.MONTH);
        int i = 0;

        try {
            while (myMonth == cal.get(Calendar.MONTH)) {
                String date = dfDate.format(cal.getTime());
                String whereClause = WearExpenseTrackerDatabase.EXPENSE_DELETED + "=0 ";
                whereClause += "AND ";
                whereClause += WearExpenseTrackerDatabase.EXPENSE_DATE + "='" + date + "'";

                database.open();
                ArrayList<Expense> expense = database.getExpenses(whereClause);
                database.close();

                if (expense.size() > 0) {
                    Group group = new Group(dfDay.format(cal.getTime()), getTotalExpensesWithDate(date), expense);
                    groups.append(i, group);
                    i++;
                }
                cal.add(Calendar.DAY_OF_MONTH, -1);
            }
        } catch (Exception e) {
            LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, e.getLocalizedMessage());
        }

        if (groups.size() > 0) {
            Group group = new Group("", 0, new ArrayList<Expense>());
            groups.append(i, group);
        }

        return groups;
    }

    public ArrayList<Expense> getMonthlyExpenses(int year, int month) {
        ArrayList<Expense> expenses = new ArrayList<Expense>();

        Calendar cal = Calendar.getInstance();
        cal.set(year, month, 1);
        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.set(year, month, maxDay);

        Calendar cal2 = Calendar.getInstance();
        cal2.set(year, month, 1);

        try {
            String date = dfDate.format(cal.getTime());
            String minDate = dfDate.format(cal2.getTime());
            String whereClause = WearExpenseTrackerDatabase.EXPENSE_DELETED + "=0 ";
            whereClause += "AND ";
            whereClause += WearExpenseTrackerDatabase.EXPENSE_DATE + "<='" + date + "'";
            whereClause += "AND ";
            whereClause += WearExpenseTrackerDatabase.EXPENSE_DATE + ">='" + minDate + "'";

            database.open();
            ArrayList<Expense> expenseWithDate = database.getExpenses(whereClause);
            database.close();

            for (Expense e : expenseWithDate) {
                expenses.add(e);
            }

        } catch (Exception e) {
            LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, e.getLocalizedMessage());
        }
        return expenses;
    }

    public float getTotalExpensesWithDate(String date) {
        ArrayList<Expense> expenses = new ArrayList<Expense>();
        float total = 0;

        try {
            database.open();
            String whereClause = WearExpenseTrackerDatabase.EXPENSE_DELETED + "=0 ";
            whereClause += "AND ";
            whereClause += WearExpenseTrackerDatabase.EXPENSE_DATE + "='" + date + "'";

            expenses = database.getExpenses(whereClause);

            for (Expense e : expenses) {
                total += e.amount;
            }

            database.close();
        } catch (Exception e) {
            LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, e.getLocalizedMessage());
        }
        return total;
    }

    public void insertExpense(View v) {
        String category = pagerAdapter.txtAddExpenseCategory.getText().toString();

        // Insert expense to database
        String strAmount = pagerAdapter.txtAddExpenseHundredThousand.getText().toString()
                + pagerAdapter.txtAddExpenseThousand.getText().toString()
                + pagerAdapter.txtAddExpense.getText().toString()
                + "."
                + pagerAdapter.txtAddExpenseCents.getText().toString();
        Date d = null;
        try {
            d = dfToday.parse(pagerAdapter.txtAddExpenseDate.getText().toString());

            String dateOnly = dfDate.format(d);
            String timeOnly = dfTimeOnly.format(new Date());
            d = dfTime.parse(dateOnly + " " + timeOnly);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String date = dfDate.format(d);
        String datetime = dfTime.format(d);
        float amount = Float.parseFloat(strAmount);
        LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, strAmount);

        try {
            database.open();
            database.insertExpense(new Expense(0, 0, 0, date, datetime, category, amount));
            database.close();
        } catch (Exception e) {
            LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, e.getLocalizedMessage());
        }

        updateTodayView(v);
        updateAllViewsOnDataChanged(0);
        autoSyncDatabase();
    }


    /* ---- FUNCTIONS USED IN WEAR-HANDHELD COMMUNICATIONS ---- */

    private ResultCallback<Status> resultCallback =  new ResultCallback<Status>() {
        @Override
        public void onResult(Status status) {
            LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, "Status: " + status.getStatus().isSuccess());

        }
    };

    @Override
    public void onConnected(Bundle bundle) {
        LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, "connected to Google Play Services on Wear!");
        Wearable.MessageApi.addListener(mGoogleApiClient, this).setResultCallback(resultCallback);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onMessageReceived(final MessageEvent messageEvent) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String handHeldMessage = messageEvent.getPath();
                String[] data = handHeldMessage.split("/");

                LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, handHeldMessage);


                if (data[0].equalsIgnoreCase("complete")) {
                    LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, "finished buying");
                    unlockFullVersion();
                }

                if (data[0].equalsIgnoreCase("autorestore")) {
                    LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, "autorestore");
                    unlockFullVersion();

                }

                if (data[0].equalsIgnoreCase("cancel")) {
                    LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, "cancelled buying");
                    prefs.edit().putBoolean("com.aeustech.wearexpensetracker.fullVersion", false).apply();
                }

                if (handHeldMessage.equals("sync/database")) {
                    sendMessageToSyncDatabase();
                }

                if (handHeldMessage.equals("sync/finish")) {
                    updateSyncExpenses();

                    try {

                        database.open();
                        String whereClause = WearExpenseTrackerDatabase.EXPENSE_SYNC + "=0";
                        ArrayList<Expense> expenses = database.getExpenses(whereClause);
                        database.close();

                        if (expenses.size() > 0) {
                            sendMessageToSyncDatabase();
                        } else {
                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... params) {
                                    sendHandheldMessage("sync/complete/", "");
                                    return null;
                                }
                            }.execute();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (handHeldMessage.equals("budget/database")) {
                    String string = new String(messageEvent.getData());
                    LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, string);
                    JSONArray array = null;

                    try {
                        array = new JSONArray(string);

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);

                            int id = object.getInt(WearExpenseTrackerDatabase.BUDGET_ID);
                            String category = object.getString(WearExpenseTrackerDatabase.BUDGET_CATEGORY);
                            String period = object.getString(WearExpenseTrackerDatabase.BUDGET_PERIOD);
                            String startDate = object.getString(WearExpenseTrackerDatabase.BUDGET_START_DATE);
                            String endDate = object.getString(WearExpenseTrackerDatabase.BUDGET_END_DATE);
                            float amount = (float) object.getDouble(WearExpenseTrackerDatabase.BUDGET_AMOUNT);

                            Budget budget = new Budget(id, category, period, startDate, endDate, amount);
                            String whereClause = WearExpenseTrackerDatabase.BUDGET_CATEGORY + "='" + category + "'";

                            try {
                                database.open();
                                ArrayList<Budget> arrayList = database.getBudget(whereClause);

                                if (arrayList.size() > 0) {
                                    database.updateBudget(budget);
                                } else {
                                    database.insertBudget(budget);
                                }

                                database.close();
                                updateBudgetView(pagerAdapter.btnGoToRemaining);

                                new AsyncTask<Void, Void, Void>() {
                                    @Override
                                    protected Void doInBackground(Void... params) {
                                        sendHandheldMessage("budget/complete/", "");
                                        return null;
                                    }
                                }.execute();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                }
            }
        });
    }

    private void unlockFullVersion() {
        prefs.edit().putBoolean("com.aeustech.wearexpensetracker.fullVersion", true).apply();
        pagerAdapter.frmLocked.setVisibility(View.GONE);
    }

    private void sendHandheldMessage(String path, String message){
        LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, "send handheld message = " + message);

        NodeApi.GetConnectedNodesResult rawNodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();

        for (final Node node : rawNodes.getNodes()) {
            LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, "Node: " + node.getId());
            PendingResult<MessageApi.SendMessageResult> result = Wearable.MessageApi.sendMessage(
                    mGoogleApiClient,
                    node.getId(),
                    path,
                    message.getBytes()
            );

            result.setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                @Override
                public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                    //  The message is done sending.
                    //  This doesn't mean it worked, though.
                    LogHelper.log(LogHelper.TAG_WearExpenseTrackerActivity, "Our callback is done.");
                    peerNode = node;    //  Save the node that worked so we don't have to loop again.
                }
            });
            break;
        }
    }

    public void sendMessageToSyncDatabase() {
        try {
            database.open();
            final JSONArray object = database.getExpensesToBeSync();
            database.close();

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    sendHandheldMessage("sync/database/", object.toString());
                    return null;
                }
            }.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void autoSyncDatabase() {
        ArrayList<Expense> expenses = null;

        try {
            String whereClause = WearExpenseTrackerDatabase.EXPENSE_SYNC + "=0";

            database.open();
            expenses = database.getExpenses(whereClause);
            database.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (expenses.size() >= DATABASE_SYNC_LIMIT) {
            Toast.makeText(this, "Data sync is in progress", Toast.LENGTH_LONG).show();
            sendMessageToSyncDatabase();
        }
        prefs.edit().putBoolean("com.aeustech.wearexpensetracker.fullVersion", false).apply();
    }

    public void updateSyncExpenses() {
        try {
            database.open();
            String whereClause = WearExpenseTrackerDatabase.EXPENSE_SYNC + "=0";
            ArrayList<Expense> expenses = database.getExpenses(whereClause);

            int limit = expenses.size();
            if (expenses.size() > WearExpenseTrackerActivity.DATABASE_SYNC_LIMIT) {
                limit = WearExpenseTrackerActivity.DATABASE_SYNC_LIMIT;
            }

            for (int i = 0; i < limit; i++) {
                Expense e = expenses.get(i);
                if (e.deleted == 1) {
                    database.deleteExpense(e);
                } else {
                    e.sync = 1;

                    Date eDate = dfDate.parse(e.date);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(eDate);

                    if (cal.get(Calendar.MONTH) == nowMonth && cal.get(Calendar.YEAR) == nowYear) {
                        database.updateExpense(e);
                    } else {
                        // Delete expenses if not in the current month
                        // for less storage memory
                        database.deleteExpense(e);
                    }
                }
            }
            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* ---- OTHER HELPER METHODS ---- */

    public String getMonthString(int month) {
        return MONTHS_FOR_SELECTION[month];
    }

    public int getMonthInt(String monthStr) {
        int i;
        for (i = 0; i < MONTHS_FOR_SELECTION.length; i++) {
            if (MONTHS_FOR_SELECTION[i].equals(monthStr)) {
                break;
            }
        }
        return i;
    }

    public String zeroPad(int value) {
        String temp = value < 10 ? "0" + value : value + "";
        return temp;
    }

    public String oneDigitPad(int value) {
        String temp = value + "";
        return temp;
    }

    public int screenHeight() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        return metrics.heightPixels;
    }
 }

   /*public void updateMonthText(TextView txtMonth, ImageButton btnNextMonth) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, transactionMonth);
        cal.set(Calendar.YEAR, transactionYear);
        String strMonth = dfMonth.format(cal.getTime());
        txtMonth.setText(strMonth.toUpperCase());

        if (nowMonth == transactionMonth && nowYear == transactionYear) {
            btnNextMonth.setClickable(false);
            btnNextMonth.setVisibility(View.INVISIBLE);
        } else {
            btnNextMonth.setClickable(true);
            btnNextMonth.setVisibility(View.VISIBLE);
        }
    }*/


