package com.aeustech.wearexpensetracker;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyExpandableListAdapter extends BaseExpandableListAdapter {

    private final SparseArray<Group> groups;
    public LayoutInflater inflater;
    public Activity activity;

    public MyExpandableListAdapter(Activity act, SparseArray<Group> groups) {
        activity = act;
        this.groups = groups;
        inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groups.get(groupPosition).children.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat df2 = new SimpleDateFormat("hh:mm a");

        final Expense child = (Expense) getChild(groupPosition, childPosition);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.wear_transactions_details, null);
        }

        ImageView imgCategory = (ImageView) convertView.findViewById(R.id.imgCategory);
        imgCategory.setImageResource(getDrawableCategory(child.category));

        TextView txtTransactionAmount = (TextView) convertView.findViewById(R.id.txtTransactionAmount);
        txtTransactionAmount.setText(formatDecimal(child.amount));

        TextView txtTransactionTime = (TextView) convertView.findViewById(R.id.txtTransactionTime);
        try {
            Date d = df1.parse(child.datetime);
            txtTransactionTime.setText(df2.format(d));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ImageView imgDelete = (ImageView) convertView.findViewById(R.id.imgDelete);
        imgDelete.setVisibility(View.VISIBLE);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((WearExpenseTrackerActivity) activity).pagerAdapter.layoutDeleteBg.setVisibility(View.VISIBLE);

                DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                DateFormat df2 = new SimpleDateFormat("hh:mm a");
                DateFormat df3 = new SimpleDateFormat("dd MMM yyyy");

                View contentView = inflater.inflate(R.layout.wear_transactions_delete, null);
                ImageView delImgCat = (ImageView) contentView.findViewById(R.id.imgCategory);
                delImgCat.setImageResource(getDrawableCategory(child.category));

                TextView delTxtTransactionAmount = (TextView) contentView.findViewById(R.id.txtTransactionAmount);
                delTxtTransactionAmount.setText(formatDecimal(child.amount));

                TextView delTxtTransactionTime = (TextView) contentView.findViewById(R.id.txtTransactionTime);
                try {
                    Date d = df1.parse(child.datetime);
                    delTxtTransactionTime.setText(df2.format(d));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                TextView delTxtTransactionDate = (TextView) contentView.findViewById(R.id.txtTransactionDate);
                try {
                    Date d = df1.parse(child.datetime);
                    delTxtTransactionDate.setText(df3.format(d));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                CustomDialog.Builder customBuilder = new CustomDialog.Builder(activity);
                customBuilder.setTitle("EXPENSES");
                customBuilder.setMessage("Remove?");
                customBuilder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ((WearExpenseTrackerActivity) activity).pagerAdapter.layoutDeleteBg.setVisibility(View.GONE);

                                Group group = (Group) getGroup(groupPosition);
                                group.children.remove(childPosition);
                                notifyDataSetChanged();
                                dialog.cancel();

                                // database.deleteExpense(child);
                                // temp delete, update deleted=1
                                WearExpenseTrackerDatabase database = new WearExpenseTrackerDatabase(activity);
                                try {
                                    database.open();
                                    child.deleted = 1;
                                    child.sync = 0;
                                    database.updateExpense(child);
                                    database.close();
                                } catch (Exception e) {
                                    LogHelper.log(LogHelper.TAG_MyExpandableListAdapter, e.getLocalizedMessage());
                                }
                                ((WearExpenseTrackerActivity) activity).updateAllViewsOnDataChanged(0);
                                ((WearExpenseTrackerActivity) activity).autoSyncDatabase();

                                group = (Group) getGroup(groupPosition);
                                if (group != null) {
                                    ((WearExpenseTrackerActivity) activity).pagerAdapter.listTransactions.expandGroup(groupPosition);
                                }
                            }
                        });
                customBuilder.setContentView(contentView);
                customBuilder.setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ((WearExpenseTrackerActivity) activity).pagerAdapter.layoutDeleteBg.setVisibility(View.GONE);
                                dialog.cancel();
                            }
                        });

                Dialog dialog = customBuilder.create();
                dialog.show();
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (groups.get(groupPosition) != null) {
            return groups.get(groupPosition).children.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        if (groups != null) {
            return groups.get(groupPosition);
        } else {
            return null;
        }
    }

    @Override
    public int getGroupCount() {
        if (groups != null) {
            return groups.size();
        } else {
            return 0;
        }
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        int childrenCount = getChildrenCount(groupPosition);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.wear_transactions_group, null);
        }

        final int position = groupPosition;
        Group group = (Group) getGroup(groupPosition);

        TextView txtDay = (TextView) convertView.findViewById(R.id.txtDay);
        TextView txtAmount = (TextView) convertView.findViewById(R.id.txtAmount);
        ImageView imgIndicator = (ImageView) convertView.findViewById(R.id.imgIndicator);

        txtDay.setText(group.date);
        txtAmount.setText(formatDecimal(group.amount));
        if (isExpanded) {
            imgIndicator.setImageResource(R.drawable.transaction_arrow2);
        } else {
            imgIndicator.setImageResource(R.drawable.transaction_arrow1);
        }

        View viewLine = (View) convertView.findViewById(R.id.viewLine);

        if (txtDay.getText().toString().equals("")) {
            txtAmount.setText("");
            imgIndicator.setVisibility(View.INVISIBLE);
            viewLine.setVisibility(View.INVISIBLE);
        } else {
            imgIndicator.setVisibility(View.VISIBLE);
            viewLine.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    String formatDecimal(float number) {
        DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance();
        format.setMaximumFractionDigits(2);
        format.setMinimumFractionDigits(2);
        return format.format(number);
    }

    int getDrawableCategory(String category) {
        if (category.equalsIgnoreCase(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_PERSONAL)) {
            return R.drawable.expense_cat1_personal;
        } else if (category.equalsIgnoreCase(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_HOUSE)) {
            return R.drawable.expense_cat2_house;
        } else if (category.equalsIgnoreCase(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FOOD)) {
            return R.drawable.expense_cat3_food;
        } else if (category.equalsIgnoreCase(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_TRANSPORTATION)) {
            return R.drawable.expense_cat4_transpo;
        } else if (category.equalsIgnoreCase(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_CLOTHING)) {
            return R.drawable.expense_cat5_clothes;
        } else if (category.equalsIgnoreCase(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FUN)) {
            return R.drawable.expense_cat6_fun;
        } else if (category.equalsIgnoreCase(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_FAMILY)) {
            return R.drawable.expense_cat7_family;
        } else if (category.equalsIgnoreCase(WearExpenseTrackerDatabase.EXPENSE_CATEGORY_MISCELLANEOUS)) {
            return R.drawable.expense_cat8_misc;
        } else {
            return 0;
        }
    }
}

