package com.aeustech.wearexpensetracker;

/**
 * Created by lnz on 1/28/15.
 */
public class Expense {
    public int id;
    public int sync;
    public int deleted;
    public String date;
    public String datetime;
    public String category;
    public float amount;

    public Expense(int id, int sync, int deleted, String date, String datetime, String category, float amount) {
        this.id = id;
        this.sync = sync;
        this.deleted = deleted;
        this.date = date;
        this.datetime = datetime;
        this.category = category;
        this.amount = amount;
    }
}
