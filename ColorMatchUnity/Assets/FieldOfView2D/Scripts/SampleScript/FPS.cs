﻿using UnityEngine;
using System.Collections;

public class FPS : MonoBehaviour {

	public GUIText debugText;
	public float updateRate = 1;
	float uR;

	void Start () 
	{
		uR = updateRate;
		Application.targetFrameRate = 60;
	}
	

	void Update () 
	{
		if(uR>0)
		{
			uR-=Time.deltaTime;
		}
		else
		{
			uR = updateRate;
			debugText.text = (1f/Time.deltaTime).ToString("0.00");
		}
	}
}
