#import "UnityAppController.h"
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <StoreKit/StoreKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <Twitter/Twitter.h>
#import <GameKit/GameKit.h>
#import <iAd/iAd.h>

#import "Reachability.h"
#import "Flurry.h"
#import "Chartboost.h"


@interface CustomController :  UIViewController <UIActionSheetDelegate, MFMailComposeViewControllerDelegate,SKProductsRequestDelegate,SKPaymentTransactionObserver,SKPaymentTransactionObserver, ChartboostDelegate, GKGameCenterControllerDelegate, ADBannerViewDelegate>
{
    SKProductsRequest *productRequest;
    Chartboost *cb;
    ADBannerView *adView;
}

@property (strong, nonatomic) NSString* productType;
@property (readwrite, nonatomic) BOOL gameCenterEnabled;
@property (readwrite, nonatomic) BOOL gamePlayStarted;
@property (readwrite, nonatomic) BOOL bannerIsVisible;

- (void)completeTransaction:(SKPaymentTransaction *)transaction;
- (void)restoreTransaction:(SKPaymentTransaction *)transaction;
- (void)failedTransaction:(SKPaymentTransaction *)transaction;
@end

@implementation CustomController

#pragma mark - In-app purchase methods

-(void)buyProduct:(NSString *)productName
{
    if (! [self hasInternetConnection]) {
        [self echoTest:@"noInternetConnection"];
    } else {
        
        // check if restoring previous purchases
        if([productName isEqualToString:@"restore"]) {
            [self restorePurchases];
            return;
        }
        
        // products: moonrox20, moonrox100, moonrox999
        NSString *productNameType = [NSString stringWithFormat:@"com.aeustech.matchblox.%@",productName];
        NSLog(@"product to buy: %@", productNameType);
        self.productType = productName;
        
        productRequest = [[SKProductsRequest alloc] initWithProductIdentifiers: [NSSet setWithObject: productNameType]];
        productRequest.delegate = self;
        [productRequest start];
    }
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    
    NSArray *products = response.products;
    if([products count] == 0) {
        // TODO: call unity to hide loading screen
        // TODO: call unity to display product unavailable
        NSLog(@"no products returned");
        [self echoTest:@"failedBuyingProduct"];
    }
    else {
        NSLog(@"user requested to buy: %@",[[products objectAtIndex:0] productIdentifier]);
        //[self showAlert:[NSString stringWithFormat:@"received response, product id = %@",[[products objectAtIndex:0] productIdentifier]]];
        
        //Since only one product, we do not need to choose from the array. Proceed directly to payment.
        SKPayment *newPayment = [SKPayment paymentWithProduct:[products objectAtIndex:0]];
        [[SKPaymentQueue defaultQueue] addPayment:newPayment];
    }
    
    [request autorelease];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    NSLog(@"updated transactions");
    
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    }
}

- (void)completeTransaction: (SKPaymentTransaction *)transaction
{
    NSLog(@"Transaction Completed");
    //    [self showAlert:@"transaction completed"];
    
    UnitySendMessage("NativeCodeCallbacks", "CallFromiOS", (const char *) [self.productType UTF8String]);
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    
    // TODO: tell Unity to add more moonrox
    // hide loading screen and inform user that more moonrox were added
    [self echoTest:@"success"];
}

- (void)restoreTransaction: (SKPaymentTransaction *)transaction
{
    NSLog(@"Transaction Restored");
    NSString *prodId = [[transaction payment] productIdentifier];
    
    // TODO: parse prodid and add more moonrox based on prodid being restored
    // need to research how this is handled for consumables or it may not be needed at all
    
    //    NSLog(@"product restored = %@", prodId);
    
    //    NSString *chapterToUnlock = [NSString stringWithFormat:@"Chapter %@",[[prodId componentsSeparatedByString:@"_"] objectAtIndex:1]];
    //    NSMutableArray *unlocked = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"unlocked"]];
    //
    //    if ([unlocked containsObject:chapterToUnlock]) {
    //        [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    //
    //        [DataManager logEvent:@"PURCHASES_ALREADY_RESTORED" param1:prodId param2:@""];
    //
    //    } else {
    //
    //        [chaptersToRestore addObject:chapterToUnlock];
    //
    //        [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    //
    //        [DataManager logEvent:@"PURCHASES_RESTORED" param1:prodId param2:@""];
    //    }
}

- (void)failedTransaction: (SKPaymentTransaction *)transaction
{
    NSLog(@"Transaction Failed");
    [self echoTest:@"failedBuyingProduct"];
    
    // TODO: tell Unity to hide loading screen
    
    if (transaction.error.code != SKErrorPaymentCancelled) {
        // TODO: tell Unity to inform the user that an error has occurred
        //[self showAlert:@"an error occurred"];
        
        
    } else {
        // TODO: tell Unity to inform the user that transaction was cancelled
        //[self showAlert:@"transaction cancelled"];
        
    }
    
    // Finally, remove the transaction from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    
    
}

-(void)restorePurchases
{
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    // TODO: tell Unity to inform the user that the purchases were restored
    NSLog(@"purchases restored");
    [self echoTest:@"success"];
    //    [self showAlert:@"purchase restored"];
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    NSLog(@"error restoring purchases = %@", [error description]);
    [self echoTest:@"failedBuyingProduct"];
    
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    // TODO: tell Unity to inform the user that the restore failed
    
}


#pragma mark - Misc

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    NSLog(@"alert button = %@", buttonTitle);
}

-(void)emailXMLFile:(NSString *)xmlFile
{
    
	NSString *fileToSend = [xmlFile lastPathComponent];
	
	NSLog(@"--- sending %@", fileToSend);
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    
    picker.mailComposeDelegate = self;
    [picker setSubject:[@"JR Level File - " stringByAppendingFormat:@"%@", fileToSend]];
    
    NSArray *toRecipients = [NSArray arrayWithObjects:@"lito@aeustech.com",nil];
    [picker setToRecipients:toRecipients];
    
    NSString *emailBody = @"Attached is an updated level file";
    [picker setMessageBody:emailBody isHTML:NO];
    
    NSData *attachment = [NSData dataWithContentsOfFile:xmlFile];
    [picker addAttachmentData:attachment mimeType:@"text/xml" fileName:fileToSend];
    
	extern UIViewController* UnityGetGLViewController();
    [UnityGetGLViewController() presentViewController:picker animated:YES completion:nil];
    
    [picker release];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Email cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Email saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Email sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Email failed");
            break;
        default:
            NSLog(@"Email default result");
            break;
    }
    
    extern UIViewController* UnityGetGLViewController();
    [UnityGetGLViewController() dismissViewControllerAnimated:YES completion:nil];
}

-(void)echoTest:(NSString *)message
{
    if ([message isEqualToString:@"noInternetConnection"]) {
        UnitySendMessage("NativeCodeCallbacks", "CallFromiOS", "noInternetConnection");
    } else if ([message isEqualToString:@"problemConnectingToFb"]) {
        UnitySendMessage("NativeCodeCallbacks", "CallFromiOS", "problemConnectingToFb");
    } else if ([message isEqualToString:@"success"]) {
        UnitySendMessage("NativeCodeCallbacks", "CallFromiOS", "success");
    } else if ([message isEqualToString:@"fbNotAllowed"]) {
        UnitySendMessage("NativeCodeCallbacks", "CallFromiOS", "fbNotAllowed");
    } else if ([message isEqualToString:@"problemConnectionToTwitter"]) {
        UnitySendMessage("NativeCodeCallbacks", "CallFromiOS", "problemConnectionToTwitter");
    } else if ([message isEqualToString:@"failedBuyingProduct"]) {
        UnitySendMessage("NativeCodeCallbacks", "CallFromiOS", "failedBuyingProduct");
    } else if ([message isEqualToString:@"successBuyingProduct"]) {
        UnitySendMessage("NativeCodeCallbacks", "CallFromiOS", "successBuyingProduct");
    } else {
        UnitySendMessage("NativeCodeCallbacks", "CallFromiOS", "Hello from iOS!");
        
        UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"OMG"
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil] autorelease];
        [alert show];
    }
}

-(void)showAlert:(NSString *)message
{
	UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"OMG" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] autorelease];
    [alert show];
}

#pragma mark - Facebook
-(BOOL)hasInternetConnection
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

-(void)fbCreateNewSession {
    FBSession* session = [[FBSession alloc] init];
    [FBSession setActiveSession: session];
}

-(void)fbLogin:(void (^)(void)) action {
    NSArray *permissions = [[NSArray alloc] initWithObjects: @"email", nil];
    
    // Attempt to open the session. If the session is not open, show the user the Facebook login UX
    [FBSession openActiveSessionWithReadPermissions:permissions
                                       allowLoginUI:true
                                  completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                      if (error) {
                                          // Error in FB
                                          NSString *valueError = [error.userInfo objectForKey:FBErrorLoginFailedReason];
                                          if ([valueError compare:FBErrorLoginFailedReasonSystemDisallowedWithoutErrorValue] == NSOrderedSame) {
                                              NSLog(@"To use your Facebook account with this app, open Settings > Facebook and make sure this app is turned on.");
                                              [self echoTest:@"fbNotAllowed"];
                                          } else {
                                              [self echoTest:@"problemConnectingToFb"];
                                          }
                                      } else {
                                          // Did something go wrong during login? I.e. did the user cancel?
                                          if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateCreatedOpening) {
                                              // If so, just send them round the loop again
                                              [[FBSession activeSession] closeAndClearTokenInformation];
                                              [FBSession setActiveSession:nil];
                                              [self fbCreateNewSession];
                                              [self echoTest:@"problemConnectingToFb"];
                                          }
                                          else {
                                              //logged in
                                              action();
                                          }
                                      }
                                  }];
}

-(void)fbShare:(NSString *)message
{
    if ([self hasInternetConnection]) {
        [self fbLogin:^ {
            // This function will invoke the Feed Dialog to post to a user's Timeline and News Feed
            // It will attemnt to use the Facebook Native Share dialog
            // If that's not supported we'll fall back to the web based dialog.
            
            NSString *appID = @"838303028";
            NSString *linkURL = [NSString stringWithFormat:@"http://itunes.apple.com/app/id%@", appID];
            NSString *pictureURL = @"https://fbcdn-sphotos-e-a.akamaihd.net/hphotos-ak-frc1/t1/10013503_437600733009014_1860489985_n.png";
            
            // Prepare the native share dialog parameters
            FBShareDialogParams *shareParams = [[FBShareDialogParams alloc] init];
            shareParams.link = [NSURL URLWithString:linkURL];
            shareParams.name = @"Match Blox";
            shareParams.caption= @"Match Blox";
            shareParams.picture= [NSURL URLWithString:pictureURL];
            shareParams.description = message;
            
            if ([FBDialogs canPresentShareDialogWithParams:shareParams]){
                [FBDialogs presentShareDialogWithParams:shareParams
                                            clientState:nil
                                                handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                                    if(error) {
                                                        NSLog(@"Error publishing story.");
                                                        [self echoTest:@"problemConnectingToFb"];
                                                    } else if (results[@"completionGesture"] && [results[@"completionGesture"] isEqualToString:@"cancel"]) {
                                                        NSLog(@"User canceled story publishing.");
                                                        [self echoTest:@"problemConnectingToFb"];
                                                    } else {
                                                        NSLog(@"Story published.");
                                                        [self echoTest:@"success"];
                                                    }
                                                }];
            } else {
                // Prepare the web dialog parameters
                NSDictionary *params = @{
                                         @"name" : shareParams.name,
                                         @"caption" : shareParams.caption,
                                         @"description" : shareParams.description,
                                         @"picture" : pictureURL,
                                         @"link" : linkURL
                                         };
                // Invoke the dialog
                [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                                       parameters:params
                                                          handler:
                 ^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                     if (error) {
                         NSLog(@"Error publishing story.");
                         [self echoTest:@"problemConnectingToFb"];
                     } else {
                         if (result == FBWebDialogResultDialogNotCompleted) {
                             NSLog(@"User canceled story publishing.");
                             [self echoTest:@"problemConnectingToFb"];
                         } else {
                             NSLog(@"Story published.");
                             [self echoTest:@"success"];
                         }
                     }}];
            }
        }];
    } else {
        [self echoTest:@"noInternetConnection"];
    }
}

#pragma mark - Rate application
-(void)rateApp
{
    NSString *appID = @"838303028";
    NSString *url = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@", appID];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < 7.0f) {
        url = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@", appID];
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

#pragma mark - Twitter
- (void)tweet:(NSString *)message
{
    if ([self hasInternetConnection]) {
        if ([TWTweetComposeViewController canSendTweet]) {
            TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
            [tweetSheet setInitialText:message];
            NSString *appID = @"838303028";
            NSString *url = [NSString stringWithFormat:@"http://itunes.apple.com/app/id%@", appID];
            [tweetSheet addURL:[NSURL URLWithString:url]]; //change to app store link
            
            NSString *photoURL = @"https://fbcdn-sphotos-e-a.akamaihd.net/hphotos-ak-frc1/t1/10013503_437600733009014_1860489985_n.png";
            NSURL *aURL = [NSURL URLWithString:photoURL];
            NSData *data = [[NSData alloc] initWithContentsOfURL:aURL];
            [tweetSheet addImage:[UIImage imageWithData:data]];
            
            extern UIViewController* UnityGetGLViewController();
            [UnityGetGLViewController() presentViewController:tweetSheet animated:YES completion:nil];
            
            //[self presentModalViewController:tweetSheet animated:YES];
        } else {
            [self echoTest:@"problemConnectionToTwitter"];
        }
    } else {
        [self echoTest:@"noInternetConnection"];
    }
}

#pragma mark - Apple iAd

-(void)initIAds
{
    NSLog(@"--- init iAds");
    extern UIViewController* UnityGetGLViewController();
    
    
    self.bannerIsVisible = NO;
    
    int adViewCount = [[UnityGetGLViewController().view subviews] count];
    
    NSLog(@"--- unity subviews count = %d", adViewCount);
    
    if(adViewCount == 0) {
        adView = [[ADBannerView alloc] initWithFrame:CGRectZero];
        adView.requiredContentSizeIdentifiers = [NSSet setWithObject:ADBannerContentSizeIdentifierLandscape];
        adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
        adView.delegate = self;
        
        CGRect viewFrame = UnityGetGLViewController().view.frame;
        
        adView.frame = CGRectMake(0, viewFrame.size.width, adView.frame.size.width, adView.frame.size.height);
        [UnityGetGLViewController().view addSubview:adView];
        
    } else {
        adView = [[UnityGetGLViewController().view subviews] objectAtIndex:0];
    }
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    NSLog(@"--- Banner view is beginning an ad action");
    
    BOOL shouldExecuteAction = YES;
    
    if (!willLeave && shouldExecuteAction)
    {
        UnitySendMessage("NativeCodeCallbacks", "PauseOnAd", "");
    }
    return shouldExecuteAction;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
    NSLog(@"--- close iAd view");
}

- (void)showIADBanner:(BOOL) show
{
    if (show && [adView isBannerLoaded]) {
        adView.hidden = NO;
        NSLog(@"--- ad banner shown");
        
    } else {
        adView.hidden = YES;
        NSLog(@"--- ad banner hidden");
        
        //        [adView setNeedsDisplay];
        //        [CATransaction flush];
    }
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    if (!self.bannerIsVisible)
    {
        [UIView beginAnimations:@"animateAdBannerOn" context:NULL];
        // Assumes the banner view is just off the bottom of the screen.
        adView.frame = CGRectOffset(adView.frame, 0, -adView.frame.size.height);
        [UIView commitAnimations];
        self.bannerIsVisible = YES;
    }
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    if (self.bannerIsVisible)
    {
        [UIView beginAnimations:@"animateAdBannerOff" context:NULL];
        // Assumes the banner view is placed at the bottom of the screen.
        adView.frame = CGRectOffset(adView.frame, 0, adView.frame.size.height);
        [UIView commitAnimations];
        self.bannerIsVisible = NO;
    }
}


#pragma mark - Game Center

- (void) updatePlayerAuthStatus {
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    if (localPlayer.isAuthenticated)
    {
        NSLog(@"--- authenticated local player");
        self.gameCenterEnabled = YES;
        
        // notify unity that game center is enabled
        UnitySendMessage("NativeCodeCallbacks", "CallFromiOS", "gameCenterEnabled");
        
    }
    else
    {
        NSLog(@"--- local player has not authenticated");
        self.gameCenterEnabled = NO;
        
        // notify unity that game center is enabled
        UnitySendMessage("NativeCodeCallbacks", "CallFromiOS", "gameCenterDisabled");
    }
}

- (void) authenticateLocalPlayer
{
    extern UIViewController* UnityGetGLViewController();
    
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        if (viewController != nil)
        {
            NSLog(@"--- presenting game center login view");
            [UnityGetGLViewController() presentViewController:viewController animated:YES completion:nil];
            
        }
        else if (localPlayer.isAuthenticated)
        {
            NSLog(@"--- local player has authenticated");
            self.gameCenterEnabled = YES;
            
            // notify unity that game center is enabled
            UnitySendMessage("NativeCodeCallbacks", "CallFromiOS", "gameCenterEnabled");
            
        }
        else
        {
            NSLog(@"--- disable game center functions");
            self.gameCenterEnabled = NO;
            
            // notify unity that game center is enabled
            UnitySendMessage("NativeCodeCallbacks", "CallFromiOS", "gameCenterDisabled");
            
            if(error != nil) {
                NSLog(@"error = %@", [error description]);
            }
            
        }
    };
    
}

- (void) submitBestScore:(NSString *)score leaderboard:(NSString *) lbID
{
    //    if(! self.gameCenterEnabled) return;
    
    GKScore *scoreReporter = [[GKScore alloc] initWithCategory:lbID];
    scoreReporter.value = [score intValue];
    scoreReporter.context = 0;
    
    [scoreReporter reportScoreWithCompletionHandler:^(NSError *error) {
        NSLog(@"--- submitted best score = %@", score);
    }];
}

- (void) showLeaderBoard:(NSString *) lbID
{
    //    if(! self.gameCenterEnabled) return;
    
    GKGameCenterViewController *gameCenterController = [[GKGameCenterViewController alloc] init];
    if (gameCenterController != nil)
    {
        gameCenterController.gameCenterDelegate = self;
        gameCenterController.viewState = GKGameCenterViewControllerStateLeaderboards;
        gameCenterController.leaderboardTimeScope = GKLeaderboardTimeScopeToday;
        gameCenterController.leaderboardCategory = lbID;
        //        [self presentViewController: gameCenterController animated: YES completion:nil];
        
        extern UIViewController* UnityGetGLViewController();
        [UnityGetGLViewController() presentViewController:gameCenterController animated:YES completion:nil];
        
    }
}

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    extern UIViewController* UnityGetGLViewController();
    [UnityGetGLViewController() dismissViewControllerAnimated:YES completion:nil];
}

- (void)reportAchievement: (NSString*) achievementID percentComplete: (NSString*) percent
{
    //    if(! self.gameCenterEnabled) return;
    
    NSLog(@"--- report achievement = %@ complete = %@",achievementID, percent);
    
    GKAchievement *achievement = [[GKAchievement alloc] initWithIdentifier: achievementID];
    if (achievement)
    {
        achievement.percentComplete = [percent floatValue];
        achievement.showsCompletionBanner = YES;
        
        [achievement reportAchievementWithCompletionHandler:^(NSError *error)
         {
             if (error != nil)
             {
                 NSLog(@"Error in reporting achievements: %@", error);
             }
         }];
    }
}

- (void) showAchievements
{
    //    if(! self.gameCenterEnabled) return;
    
    GKGameCenterViewController *gameCenterController = [[GKGameCenterViewController alloc] init];
    if (gameCenterController != nil)
    {
        gameCenterController.gameCenterDelegate = self;
        gameCenterController.viewState = GKGameCenterViewControllerStateAchievements;
        
        extern UIViewController* UnityGetGLViewController();
        [UnityGetGLViewController() presentViewController:gameCenterController animated:YES completion:nil];
        
    }
}

#pragma mark - Flurry

- (void)logEvent:(NSString *)event param1:(NSString *)param1 param2:(NSString *)param2
{
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:@"uuid"];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:uuid, @"userid", param1, @"param1", param2, @"param2", nil];
    NSString *logData = [NSString stringWithFormat:@"%@", event];
    [Flurry logEvent:logData withParameters:params];
    NSLog(@"Flurry event = %@ - %@", event, params);
}

- (void)initFlurry
{
    [Flurry setCrashReportingEnabled:YES];
    [Flurry startSession:@"79WVGCRJNX7TTMKQ2W2H"];
}

#pragma mark - Ads

- (void)didDismissInterstitial:(NSString *)location {
    NSLog(@"--- caching ads");
    [cb cacheInterstitial:location];
}

- (BOOL)shouldDisplayInterstitial:(NSString *)location {
    NSLog(@"--- should display ads");
    // do not display ads when gameplay has started
    return !self.gamePlayStarted;
}

-(void)gamePlayStarted:(NSString *)message
{
    if([message isEqualToString:@"YES"]) {
        NSLog(@"--- game play started");
        self.gamePlayStarted = YES;
    } else {
        NSLog(@"--- game play stopped");
        self.gamePlayStarted = NO;
    }
}

-(void)showAds:(NSString *)location
{
    //    [self showAlert:@"show ads"];
    if([cb hasCachedInterstitial:location]) {
        NSLog(@"--- show ads - %@",location);
        [cb showInterstitial:location];
    }
}

-(void)initAds
{
    //    [self showAlert:@"init ads"];
    NSLog(@"--- init ads");
    
    cb = [Chartboost sharedChartboost];
    
    //    cb.appId = @"530e1e412d42da48fbf3e4ca";
    //    cb.appSignature = @"695fb1ca33542fb481450ef2cbf31e1140404981";
    
    // Required for use of delegate methods. See "Advanced Topics" section below.
    cb.delegate = self;
    
    // Begin a user session. Must not be dependent on user actions or any prior network requests.
    // Must be called every time your app becomes active.
    //    [cb startSession];
    //
    //    [cb cacheInterstitial:@"Main Menu"];
    //    [cb cacheInterstitial:@"Game Over"];
    //    [cb cacheInterstitial:@"Pause"];
}

@end


NSString* CreateNSString (const char* string)
{
	if (string)
		return [NSString stringWithUTF8String: string];
	else
		return [NSString stringWithUTF8String: ""];
}

extern "C" {
    CustomController *controller;
    
    void init()
    {
        controller = [[CustomController alloc] init];
    }
    
    void sendEmail(const char* xmlFile)
    {
        [controller emailXMLFile:CreateNSString(xmlFile)];
    }
    
    void echoTest(const char* message)
    {
        [controller echoTest:CreateNSString(message)];
    }
    
    void buyProduct(const char* productName)
    {
        [controller buyProduct:CreateNSString(productName)];
    }
    
    void fbShare(const char* message) {
        [controller fbShare:CreateNSString(message)];
    }
    
    void tweet(const char* message) {
        [controller tweet:CreateNSString(message)];
    }
    
    void rateApp() {
        [controller rateApp];
    }
    
    void initGameCenter() {
        controller = [[CustomController alloc] init];
        controller.gamePlayStarted = NO;
        controller.gameCenterEnabled = NO;
        
        // authenticate only on iOS 6 and up, disable GC otherwise
        float gcSupportedVersion = 6.0;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= gcSupportedVersion)
        {
            [controller authenticateLocalPlayer];
        }
    }
    
    void submitBestScore(const char* score, const char* lbID) {
        [controller submitBestScore:CreateNSString(score) leaderboard:CreateNSString(lbID)];
    }
    
    void showLeaderBoard(const char* lbId) {
        [controller showLeaderBoard:CreateNSString(lbId)];
    }
    
    void submitAchievement(const char* aID, const char* percentComplete) {
        [controller reportAchievement:CreateNSString(aID) percentComplete:CreateNSString(percentComplete)];
    }
    
    void showAchievements() {
        [controller showAchievements];
    }
    
    void updatePlayerAuthStatus() {
        [controller updatePlayerAuthStatus];
    }
    
    void initFlurry() {
        [controller initFlurry];
    }
    
    void logFlurryEvent(const char* event) {
        [controller logEvent:CreateNSString(event) param1:@"" param2:@""];
    }
    void showAds(const char* location) {
        [controller showAds:CreateNSString(location)];
    }
    
    void initAds() {
        [controller initAds];
    }
    
    void gamePlayStarted(const char* message) {
        [controller gamePlayStarted:CreateNSString(message)];
    }
    
    void initIAds() {
        [controller initIAds];
    }
    
    void showIADBanner(const char* show) {
        if ([CreateNSString(show) isEqualToString:@"YES"] ) {
            [controller showIADBanner:YES];
        } else {
            [controller showIADBanner:NO];
        }
    }
    
    void restorePurchases()
    {
        [controller restorePurchases];
    }
}
