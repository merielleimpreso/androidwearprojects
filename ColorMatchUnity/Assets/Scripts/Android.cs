﻿using UnityEngine;
using System.Collections;

public class Android : MonoBehaviour {

	AndroidJavaClass androidJavaClass;
	AndroidJavaObject javaObj;
	AndroidJavaClass javaClass;


	void Start () 
	{
		androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		javaObj = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
		//javaClass = new AndroidJavaClass("com.aeustech.colormatch.UnityPlayerNativeActivity");
	
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKey (KeyCode.Alpha1))
		{
			UnlockAllModes();
		}
		if(Input.GetKey(KeyCode.Alpha2))
		{
			LockAllModes();
		}

		if(Input.GetKey(KeyCode.Alpha3))
		{
			PlayerPrefs.SetInt("Classic_unlocked", 0);
		}

		if(Input.GetKey(KeyCode.Alpha4))
		{
			PlayerPrefs.SetInt("Classic_unlocked", 1);
		}
	
	}

	public void CallFromAndroid(string message) {

		if (message == "runningOnRoundScreen") {
			Screen.orientation = ScreenOrientation.LandscapeLeft;
		} else {
			Screen.orientation = ScreenOrientation.Portrait;
		}
	}

	public string GiveClassicScore()
	{
		return PlayerPrefs.GetInt("ClassicHighScore").ToString();
	}

	public string GiveTimedScore()
	{
		return PlayerPrefs.GetInt("TimedHighScore").ToString();
	}

	public string GiveMemoryScore()
	{
		return PlayerPrefs.GetInt("MemoryHighScore").ToString();
	}

	public string GiveAdvancedScore()
	{
		return PlayerPrefs.GetInt("AdvancedHighScore").ToString();
	}

	public void UpdateAllScores()
	{
		string path = "AllScores/";
		path += GiveClassicScore()+",";
		path += GiveTimedScore() +",";
		path += GiveMemoryScore()+",";
		path += GiveAdvancedScore();

		string [] args = {path};
		Debug.Log ("Called UpdateAllScores");
		javaObj.Call("updateScores", args);

	}

	public void GoToBuyScreen()
	{
		string [] args = {""};
		javaObj.Call("goToBuyScreen", args);
		Debug.Log ("Called UpdateAllScores");
	}

	public void UnlockAllModes()
	{
		PlayerPrefs.SetInt("Timed_unlocked", 1);
		PlayerPrefs.SetInt("Memory_unlocked", 1);
		PlayerPrefs.SetInt("Advanced_unlocked", 1);
		GameObject.FindObjectOfType<Master>().DisableUnlockButton();
		Debug.Log ("Called Unlock all modes");
	}

	public void LockAllModes()
	{
		PlayerPrefs.SetInt("Timed_unlocked", 0);
		PlayerPrefs.SetInt("Memory_unlocked", 0);
		PlayerPrefs.SetInt("Advanced_unlocked", 0);
		GameObject.FindObjectOfType<Master>().EnableUnlockButton();
		Debug.Log ("Called lock all modes");
	}

	public void UpdateScore(string path)
	{
		string[] args = {path};
		javaObj.Call("updateScores", args);
		Debug.Log ("Called update score");
	}

}
