﻿using UnityEngine;
using System.Collections;

public class ColorButton : MonoBehaviour {

	Master master;
	public SpriteRenderer myRenderer;
	public Sprite mySprite;
	public GameObject gloss;
	Animator myAnimator;
	float glossWait = 0f;


	void Start () 
	{
		GetMySprite();
	}
	

	void Update () 
	{
		if(glossWait <= 0 && gloss.activeSelf)
		{
			//gloss.SetActive(false);
		}
		else if(gloss && gloss.activeSelf)
		{
			glossWait-= Time.deltaTime;
		}

		//CheckMouse();
	}



	public void Activate()
	{
		if(!master.isEnded && master.TimerActual() && !master.isPaused)
		{
			master.GuessColor(mySprite);
			Debug.Log (master.TimerActual().ToString());
			//PlayMemoryAnimation();
		}

		//Debug.Log ("Pressed: "+ mySprite.name);
	}

	public void SetSprite(Sprite newSprite)
	{
		mySprite = myRenderer.sprite = newSprite;
	}

	void GetMySprite()
	{
		mySprite = myRenderer.sprite;

		if(gloss)
			myAnimator = gloss.GetComponent<Animator>();
	}

	public void SetMaster(Master x)
	{
		master = x;
	}

	public void Gloss(bool enable)
	{
		if(gloss)
			gloss.SetActive(enable);
	}

	public void PlayMemoryAnimation()
	{

		myAnimator.SetTrigger("Memory");
		myAnimator.SetBool("isHolding", false);
		Gloss(true);
		glossWait = 0.31f;
	}

	public void SetHoldAnimation(bool isHolding)
	{
		if(master.TimerActual())
			myAnimator.SetBool("isHolding", isHolding);
	}

	public void DisableGloss()
	{
		gloss.SetActive(false);
	}

	public void FinishedAnim()
	{
		//Gloss(false);
		master.ButtonFinishedAnim();
	

	}
}
