﻿using UnityEngine;
using System.Collections;

public class iOSCallbacks : MonoBehaviour {
	public GameObject loadingScreen;
	public GameObject messageScreen;
	public SpriteRenderer messageDisplay;
	public Sprite[] messageStrings;
	
	public GameObject[] objectsToShowHide;
	
	//public PlayerController controller;
	//public AdController adController;
	
	
	public void Start() 
	{
		#if UNITY_ANDROID
		
		// TODO - call back from Android to set this value, hardcode for now
		PlayerPrefs.SetInt("GameCenterEnabled", 1);
		
		#endif
		
		UpdateGameCenterButtons();
	}
	
	public void UpdateGameCenterButtons()
	{
		if (PlayerPrefs.GetInt("GameCenterEnabled") == 1) 
		{
			//			print("--- unity - game center enabled");
			ShowObjects();
			
		} else {
			//			print("--- unity - game center disabled");
			HideObjects();
			
		}
	}
	
	void ShowObjects()
	{
		foreach(GameObject go in objectsToShowHide)
		{
			if(go)
				go.SetActive(true);
		}
	}
	
	void HideObjects()
	{
		foreach(GameObject go in objectsToShowHide)
		{
			if(go)
				go.SetActive(false);
		}
	}
	
	void RestoredPurchases(string message) {
		//		print("--- unity - restore purchase message = " + message);
		
		if(message == "restored") {
			PlayerPrefs.SetInt("NoAds",1);
		}
		
		loadingScreen.SetActive(false);
	}
	
	void PauseOnAd(string message) {
		//		print("--- unity - iAd banner clicked - pause game"); 
		//		adController.PauseOnAd();
	}
	
	void CallFromAndroid(string message) {
		//		print("--- unity - call from android, message = " + message);
		
		if (message == "runningOnRoundScreen") {
			Screen.orientation = ScreenOrientation.LandscapeLeft;
		} else {
			Screen.orientation = ScreenOrientation.Portrait;
		}
		
		//		int roxBought = PlayerPrefs.GetInt("SpaceRoxBought");
		//
		//		if (message == "moonrox20") {
		//			controller.spaceRox += 20;
		//			roxBought += 20;
		//			PlayerPrefs.SetInt("SpaceRox",controller.spaceRox);
		//			
		//		} else if (message == "moonrox100") {
		//			controller.spaceRox += 100;
		//			roxBought += 100;
		//			PlayerPrefs.SetInt("SpaceRox",controller.spaceRox);
		//			
		//		} else if (message == "moonrox999") {
		//			controller.spaceRox += 999;
		//			roxBought += 999;
		//			PlayerPrefs.SetInt("SpaceRox",controller.spaceRox);
		//			
		//		} else if (message == "noads") {
		//			PlayerPrefs.SetInt("NoAds",1);
		//		}
		//		
		//		PlayerPrefs.SetInt("SpaceRoxBought", roxBought);
	}
	
	void CallFromiOS(string message) {
		
		// print("--- unity - call from ios, message = " + message);
		
		if (message == "cancelledAction") {
			messageScreen.SetActive(false);
			loadingScreen.SetActive(false);
			
			// print("--- unity - hide screens");
			
			return;
		}
		
		int roxBought = PlayerPrefs.GetInt("SpaceRoxBought");
		
		
		/*if (message == "moonrox20") {
			controller.spaceRox += 20;
			roxBought += 20;
			PlayerPrefs.SetInt("SpaceRox",controller.spaceRox);
			
		} else if (message == "moonrox100") {
			controller.spaceRox += 100;
			roxBought += 100;
			PlayerPrefs.SetInt("SpaceRox",controller.spaceRox);
			
		} else if (message == "moonrox999") {
			controller.spaceRox += 999;
			roxBought += 999;
			PlayerPrefs.SetInt("SpaceRox",controller.spaceRox);
			
		} else if (message == "noads") {
			PlayerPrefs.SetInt("NoAds",1);
		}
		
		PlayerPrefs.SetInt("SpaceRoxBought", roxBought);
		
		if(PlayerPrefs.GetInt("AchievedFinancialAid") == 0 && PlayerPrefs.GetInt("SpaceRoxBought") >= 20)
		{
			AchievementsiOSToAndroidMap.SubmitAchievement("com.aeustech.ohmygravity.omg_bought20moonrox", "100");
			PlayerPrefs.SetInt("AchievedFinancialAid", 1);
		}
		
		if(PlayerPrefs.GetInt("AchievedBenefactor") == 0 && PlayerPrefs.GetInt("SpaceRoxBought") >= 100)
		{
			AchievementsiOSToAndroidMap.SubmitAchievement("com.aeustech.ohmygravity.omg_bought100moonrox", "100");
			PlayerPrefs.SetInt("AchievedBenefactor", 1);
		}
		
		if(PlayerPrefs.GetInt("AchievedBigSpender") == 0 && PlayerPrefs.GetInt("SpaceRoxBought") >= 999)
		{
			AchievementsiOSToAndroidMap.SubmitAchievement("com.aeustech.ohmygravity.omg_bought999moonrox", "100");
			PlayerPrefs.SetInt("AchievedBigSpender", 1);
		}
		
		if(PlayerPrefs.GetInt("AchievedSpaceWhale") == 0 && PlayerPrefs.GetInt("SpaceRoxBought") >= 10000)
		{
			AchievementsiOSToAndroidMap.SubmitAchievement("com.aeustech.ohmygravity.omg_bought10000moonrox", "100");
			PlayerPrefs.SetInt("AchievedSpaceWhale", 1);
		}*/
		
		
		if (message == "noInternetConnection") {
			messageDisplay.sprite = messageStrings[0];
		} else if (message == "problemConnectingToFb") {
			messageDisplay.sprite = messageStrings[1];
		} else if (message == "successSharingToFb") {
			messageDisplay.sprite = messageStrings[2];
		} else if (message == "successBuyingProduct") {
			messageDisplay.sprite = messageStrings[2];
		} else if (message == "failedBuyingProduct") {
			messageDisplay.sprite = messageStrings[3];
		} else if (message == "fbNotAllowed") {
			messageDisplay.sprite = messageStrings[4];
		}
		
		if (message == "gameCenterEnabled") {
			PlayerPrefs.SetInt("GameCenterEnabled", 1);
			UpdateGameCenterButtons();
			
		} else if (message == "gameCenterDisabled") {
			PlayerPrefs.SetInt("GameCenterEnabled", 0);
			UpdateGameCenterButtons();
			
		}
		
		if (! (message == "gameCenterEnabled" || message == "gameCenterDisabled")) {
			messageScreen.SetActive(true);
			loadingScreen.SetActive(false);
		}
	}
}

