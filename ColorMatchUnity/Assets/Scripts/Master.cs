using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum GameType
{
	Classic,
	Timed,
	Memory,
	Advanced
}

public class Master : MonoBehaviour {

	public TextMesh scoreText;
	public float timer = 20f;
	public float timerActual;
	float fpsWait;
	public float gameOverWait = 0;
	public GameType gameType;

	public Sprite[] colorCollection;//Where we get colors. Indexes must match with colorCollection2
	public Sprite[] colorCollection2;//Center Choice. Indexes must match with colorCollection
	public ColorButton[] colorButtons;//color choices
	public SpriteRenderer colorIndicator;//Color to match
	public FOV2DEyes timerMaterial;

	public GameObject timerParent;
	public List<Sprite> choicePool = new List<Sprite>();
	List<int> colorIndexesToMemorize = new List<int>();//For Memory

	public int currentColorIndex = 0;
	public float interval = 0f;//private
	public Sprite currentColor;
	public GameObject mainMenu;

	public GameObject gameOver;
	public TextMesh highScore;

	int score = 0;
	int index = 0;
	int tries = 0;

	GameObject container;//GameObject.Find
	bool clockWise = true;
	GameObject currentObject;
	GameObject gameOverScreen;
	GameObject modeMenuScreen;
	GameObject notice;

	public GameObject unlockButton;
	Android android;

	GenericButton pauseButton;

	public bool isStarted;
	public bool isEnded;
	public bool showSequence;
	public bool lastInSequence;
	public bool isPaused;
	public bool debug;

	void Start () 
	{
		timerActual = timer+3.8f;
		InitializeChoices();
		scoreText.text = "";
		container = GameObject.Find("Container");
		gameOverScreen = GameObject.Find ("GameOver");
		modeMenuScreen = GameObject.Find ("ModeMenu");
		unlockButton = GameObject.Find("UnlockButton");
		notice = GameObject.Find ("Notice");

		GameObject[] genericButtons = GameObject.FindGameObjectsWithTag("GenericButton");

		foreach(GameObject genericButton in genericButtons)
		{
			genericButton.GetComponent<GenericButton>().Initialize(this);

		}

		if(PlayerPrefs.GetInt("Classic_unlocked") == 0)
		{
			PlayerPrefs.SetInt("Classic_unlocked", 1);
			PlayerPrefs.SetInt("Timed_unlocked", 0);
			PlayerPrefs.SetInt("Memory_unlocked", 0);
			PlayerPrefs.SetInt("Advanced_unlocked", 0);
		}

		notice.SetActive(false);
		gameOverScreen.SetActive(false);
		modeMenuScreen.SetActive(false);
		pauseButton = GameObject.Find ("Pause").GetComponent<GenericButton>();
		pauseButton.gameObject.SetActive(false);
		android = GameObject.FindObjectOfType<Android>();
		//gameOver = GameObject.Find ("GameOver");
		//highScore = gameOver.GetComponentInChildren<TextMesh>();

	}

	void Update () 
	{
		if(!isPaused)
		{
			if(!isEnded && isStarted)
			{
				Timers();
				
				//if(gameType!= GameType.Memory)
					HandleTimerBar();
				
				if(gameType == GameType.Memory && timerActual <= timer)
					HandleMemory();
				
				if(gameType == GameType.Advanced && timerActual <= timer)
					HandleAdvanced();
			}
			else
			{
				if(gameOverWait > 0)
				{
					gameOverWait-= Time.deltaTime;
				}
			}
		}

		if(SystemInfo.deviceType == DeviceType.Desktop)
			CheckMouse();
		else
			CheckTouch();


	}

	void CheckMouse()
	{

		if(Input.GetMouseButtonDown(0) /*&& (!showSequence || (showSequence && timerActual>timer))*/)
		{

			Vector3 camPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			RaycastHit hit;
			
			if(Physics.Raycast(camPos, Camera.main.transform.forward, out hit,Camera.main.transform.position.y +100))
			{
				hit.collider.gameObject.SendMessage("Activate", SendMessageOptions.DontRequireReceiver);

				if(!isPaused)
					hit.collider.gameObject.SendMessage("SetHoldAnimation", true,SendMessageOptions.DontRequireReceiver);

				if(currentObject != hit.collider.gameObject)
				{
					if(currentObject)
						currentObject.SendMessage("SetHoldAnimation", false, SendMessageOptions.DontRequireReceiver);
					currentObject = hit.collider.gameObject;
				}
			}
		}

		if(!isPaused)
		{
			if(Input.GetMouseButtonUp(0))
			{
				Vector3 camPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				RaycastHit hit;
				
				if(Physics.Raycast(camPos, Camera.main.transform.forward, out hit,Camera.main.transform.position.y +100))
				{
					hit.collider.gameObject.SendMessage("SetHoldAnimation", false,SendMessageOptions.DontRequireReceiver);
					
					if(currentObject != hit.collider.gameObject)
					{
						if(currentObject)
							currentObject.SendMessage("SetHoldAnimation", false, SendMessageOptions.DontRequireReceiver);
						currentObject = hit.collider.gameObject;
					}
				}
			}
		}

		
	}
	
	void CheckTouch()
	{
		
		if(Input.touchCount > 0 /*&& !showSequence*/)
		{

			if(Input.touches[0].phase == TouchPhase.Began)
			{
				Vector3 camPos = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
				RaycastHit hit;
				
				if(Physics.Raycast(camPos, Camera.main.transform.forward, out hit, Camera.main.transform.position.y +100))
				{
					hit.collider.gameObject.SendMessage("Activate", SendMessageOptions.DontRequireReceiver);
					hit.collider.gameObject.SendMessage("SetHoldAnimation", true,SendMessageOptions.DontRequireReceiver);

					if(currentObject != hit.collider.gameObject)
					{
						if(currentObject)
							currentObject.SendMessage("SetHoldAnimation", false, SendMessageOptions.DontRequireReceiver);
						currentObject = hit.collider.gameObject;
					}
				}
				
			}

			if(Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
			{
				Vector3 camPos = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
				RaycastHit hit;

				if(Physics.Raycast(camPos, Camera.main.transform.forward, out hit, Camera.main.transform.position.y +100))
				{
					hit.collider.gameObject.SendMessage("SetHoldAnimation", false,SendMessageOptions.DontRequireReceiver);
				}
			}
		}
		else if(!showSequence)
		{
			foreach(ColorButton colorButton in colorButtons)
			{
				colorButton.SetHoldAnimation(false);
			}
		}

	}

	void LateUpdate()
	{
		if(debug )
		{
			if(fpsWait<=0)
				scoreText.text = (1/Time.deltaTime).ToString("0");
			else
				fpsWait-= Time.deltaTime;
		}

		if(timerActual >= timer && isStarted)
		{
			scoreText.text = (timerActual-timer).ToString("0");
			timerMaterial.fovAngle = 360;
		}

		if(isPaused)
		{
			scoreText.text = "";
		}
		/*
		else if(isStarted && !isPaused && scoreText.text != score.ToString())
		{
			scoreText.text = score.ToString();
		}
		*/

//		Debug.Log (Time.timeScale.ToString());

	}

	public void GuessColor(Sprite guessedColor)
	{
		//Debug.Log (guessedColor.name + " Current: "+currentColor.name);
		if(!isEnded)
		{

			if(isStarted)
			{
			
				if(gameType != GameType.Memory)
					tries++;

				if( guessedColor == currentColor)
				{

					if(Random.Range(0,100) <= 40)
						clockWise = !clockWise;
				
					if(gameType == GameType.Timed || gameType == GameType.Advanced)
					{
						timerActual+= 0.5f;
					}

					if(gameType == GameType.Memory)
					{

						if(currentColorIndex < colorIndexesToMemorize.Count -1)
						{
							currentColorIndex++;
							currentColor = colorButtons[colorIndexesToMemorize[currentColorIndex]].mySprite;
							scoreText.text = currentColorIndex.ToString() + "/" + colorIndexesToMemorize.Count.ToString();
						}
						else
						{
							currentColorIndex = 0;
							showSequence = true;
							interval = 1f;
							MakeNewRound();
							score++;
							scoreText.text = (colorIndexesToMemorize.Count -1).ToString() + "/" + (colorIndexesToMemorize.Count -1).ToString();
						}

					}
					else
					{
						score++;
						scoreText.text = score.ToString();
					}
					
					//timerParent.transform.rotation = Quaternion.Euler(0, 0, 359);

				}
				else
				{
					if(gameType == GameType.Timed || gameType == GameType.Advanced)
					{
						timerActual-=2f;
					}
					else if(gameType == GameType.Classic)
					{
						MakeNewRound();
					}
					else 
					{
						EndGame();
					}
				}
			}
			else if(!isStarted)
			{

				StartGame();

			}

			if(gameType != GameType.Memory)
			{
				//scoreText.text = score.ToString()+ "/"+tries.ToString();
				MakeNewRound();
			}
			else
			{
				//scoreText.text = currentColorIndex.ToString() + "/" + colorIndexesToMemorize.Count.ToString();
			}
			
		
				
		}


	

	}

	void MakeNewRound()
	{

		MakeNewColorToGuess();
		PopulateChoices();


	}

	void MakeNewColorToGuess()
	{
		index = 0;

		if(gameType != GameType.Memory)
		{
			do{
				index = Random.Range(0, colorCollection.Length);
				
			}while(currentColor == colorCollection[index]);

			currentColor = colorCollection[index];
			colorIndicator.sprite = colorCollection2[index];
		}
		else
		{
			int tempIndex = Random.Range(0, colorButtons.Length);
			colorIndexesToMemorize.Add (tempIndex);
			currentColor = colorButtons[tempIndex].mySprite;
			currentColorIndex = 0;

		}
		

	}

	void PopulateChoices()
	{

		if(gameType != GameType.Memory || !isStarted)
		{
			choicePool.Clear();
			
			int chosenIndex = Random.Range(0, colorButtons.Length);//index of the correct choice
			Sprite tempSprite;
			
			foreach(Sprite col in colorCollection)
			{
				choicePool.Add(col);
			}

			colorButtons[chosenIndex].GetComponent<ColorButton>().SetSprite(currentColor);
			choicePool.Remove(currentColor);
			
			foreach(ColorButton colorButton in colorButtons)
			{
				if(System.Array.IndexOf(colorButtons, colorButton) != chosenIndex && choicePool.Count > 0)
				{
					tempSprite = choicePool[Random.Range(0, choicePool.Count)];
					colorButton.SetSprite(tempSprite);
					choicePool.Remove(tempSprite);
				}
				
			}
		}

	}

	void InitializeChoices()
	{
		foreach(ColorButton col in colorButtons)
		{
			col.SetMaster(this);
		}
	}

	void Timers()
	{
		if(gameType != GameType.Memory)
		{
			if(timerActual > 0)
				timerActual -= Time.deltaTime;
			else if(!isEnded)
			{
				timerActual =0;
				HandleTimerBar();
				EndGame();

			}
		}

		if(timerActual > timer)
		{
			timerActual -= Time.deltaTime;


		}


	}

	void HandleTimerBar()
	{
		//timerMaterial.SetFloat("_Cutoff", Mathf.Clamp(Mathf.InverseLerp(0, timer, timerActual), 0.000001f, 1));
		if(timerParent)
		{
			float newRot = Mathf.Lerp(0, 360, timerActual/timer);
			if(gameType == GameType.Memory)
			{
				newRot = 360;
			}

				timerMaterial.fovAngle = 360- (int)newRot;

			//Debug.Log ("Entered");
			//Debug.Log ((timerActual/timer).ToString("0.0"));
		}
	}
	
	public void StartGame()
	{
		if((isEnded || !isStarted) && gameOverWait<=0)
		{
			timerActual = timer + 3.5f;
			Time.timeScale = 1;
			score = 0;
			tries = 0;
			MakeNewRound();
			pauseButton.gameObject.SetActive(true);
			scoreText.text = "0";


			isStarted = true;
			isEnded = false;
			mainMenu.SetActive(false);

			if(gameType == GameType.Memory)
			{
				showSequence = true;

				//colorIndexesToMemorize.Clear();
			}


		}

	}

	void ResetTimerBar()
	{
		timerParent.transform.rotation = Quaternion.Euler(0, 0, 359f);
	
	}

	void HandleMemory()
	{
		if(interval <= 0f && showSequence)
		{
			if(currentColorIndex != 0)
			{
				//colorButtons[colorIndexesToMemorize[currentColorIndex]].DisableGloss();

			}

			colorButtons[colorIndexesToMemorize[currentColorIndex]].PlayMemoryAnimation();
		
			scoreText.text = score.ToString();

			if(currentColorIndex < colorIndexesToMemorize.Count-1)
			{
				currentColorIndex++;
				lastInSequence = false;
			}
			else
			{
				showSequence = false;
				currentColorIndex = 0;
				currentColor = colorButtons[colorIndexesToMemorize[0]].mySprite;
				lastInSequence = true;
				//scoreText.text = currentColorIndex.ToString() + "/" + colorIndexesToMemorize.Count.ToString();
			}

			interval = 0.3f;
		}
		else if(showSequence)
		{
			interval-= Time.deltaTime;

		}

	}

	void HandleAdvanced()
	{

		/*
		if(interval <= 0)
		{
			int x = 0;
			List<Sprite> spriteList = new List<Sprite>();

			foreach(ColorButton colorButton in colorButtons)
			{
				spriteList.Add(colorButton.mySprite);
				Debug.Log (colorButton.mySprite.name);
			}

			while(x < colorButtons.Length)
			{
				int nextIndex = x-1;


				if(x == 0)
					nextIndex = 3;

				Debug.Log ("Button "+ colorButtons[x].mySprite.name + " Sprite replaced with: " + spriteList[nextIndex].name);
				colorButtons[x].SetSprite(spriteList[nextIndex]);
				x++;
			}

			interval = 2f;

		}
		else
		{
			interval-= Time.deltaTime;
		}
		*/

		if(container)
		{
			float rotSpeed = 10f + (score);

			if(!clockWise)
				rotSpeed*= -1;

			container.transform.rotation = Quaternion.Euler(0, 0, container.transform.rotation.eulerAngles.z + (rotSpeed * Time.deltaTime));
		}


	}

	void EndGame()
	{
		foreach(ColorButton colorButton in colorButtons)
		{
			colorButton.SetHoldAnimation(false);
		}

		isEnded = true;
		pauseButton.gameOver.SetActive(false);
		ResetTimerBar();
		colorIndexesToMemorize.Clear();
		ShowGameOver();
		gameOverWait = 1.5f;

		if(score > PlayerPrefs.GetInt(gameType.ToString()+"HighScore"))
		{
			PlayerPrefs.SetInt(gameType.ToString()+"HighScore", score);
			android.UpdateScore(gameType.ToString()+"/"+score.ToString());
		}


		container.transform.rotation = Quaternion.Euler(0, 0, 0);




	}

	public void ButtonFinishedAnim()
	{
		if(!isEnded && isStarted && timerActual < timer)
		{

			if(!lastInSequence && !showSequence)
				scoreText.text = score.ToString();
			else if (!showSequence && lastInSequence && gameType == GameType.Memory)
			{

				scoreText.text = currentColorIndex.ToString() + "/" + colorIndexesToMemorize.Count.ToString();
			}
				


		}

		if(!showSequence)
			interval = 0f;
	}

	void ShowGameOver()
	{
		highScore.text = PlayerPrefs.GetInt(gameType.ToString()+"HighScore").ToString();
		gameOver.SetActive(true);
	}

	public void ClearScore()
	{
		scoreText.text = "";
	}

	public bool TimerActual()
	{

		return timerActual < timer;

	}

	public int Score()
	{
		return score;
	}

	public int ColorIndexesToMemorizeLength()
	{
		return colorIndexesToMemorize.Count;
	}

	public int CurrentColorIndex()
	{
		return currentColorIndex;
	}
	
	void UnlockMode(string mode)
	{
		PlayerPrefs.SetInt(mode+"_unlocked", 1);

	}

	public void Restart()
	{
		score = 0;
		isStarted = false;
		isEnded = false;
		isPaused = false;
		CleanTimerBar();
		ResetTimerBar();
		StartGame();
	}

	public void CleanTimerBar()
	{
		timerActual = timer+3.5f;
		HandleTimerBar();
	}

	public void DisableUnlockButton()
	{
		unlockButton.SetActive(false);
	}

	public void CompanionGoToBuy()
	{
		notice.SetActive(true);
		android.GoToBuyScreen();

	}

	public void EnableUnlockButton()
	{
		unlockButton.SetActive(true);
	}


}
