package com.aeustech.colormatch;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.ArrayList;
import java.util.List;

public class ColorMatchListeningService extends WearableListenerService {
    private static String TAG = "ColorMatch_S";

    // Variables used for watch-device communication
    private GoogleApiClient mGoogleApiClient;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: service created");

        //  Needed for communication between watch and device.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        Log.d(TAG, "onConnected: " + connectionHint);
                        tellWatchConnectedState("CONNECTED");
                        //  "onConnected: null" is normal.
                        //  There's nothing in our bundle.
                    }
                    @Override
                    public void onConnectionSuspended(int cause) {
                        Log.d(TAG, "onConnectionSuspended: " + cause);
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        Log.d(TAG, "onConnectionFailed: " + result);
                    }
                })
                .addApi(Wearable.API)
                .build();

        mGoogleApiClient.connect();
    }



    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: service destroyed");
    }

    /**
     * Here, the device actually receives the message that the phone sent, as a path.
     * We simply check that path's last segment and act accordingly.
     * @param messageEvent
     */

    @Override
    public void onMessageReceived(MessageEvent messageEvent)
    {
        // Check for classic score update

        if(messageEvent.getPath().indexOf("Classic") != -1 ||
           messageEvent.getPath().indexOf("Timed") != -1 ||
           messageEvent.getPath().indexOf("Memory") != -1 ||
           messageEvent.getPath().indexOf("Advanced") != -1 ||
           messageEvent.getPath().indexOf("AllScores") != -1 ||
           messageEvent.getPath().indexOf("goToBuyScreen") != -1 ||
           messageEvent.getPath().indexOf("showScores") != -1
        )
            {


                if(messageEvent.getPath().indexOf("goToBuyScreen") != -1 || messageEvent.getPath().indexOf("showScores") != -1)
                {
                    Intent LaunchIntent = getPackageManager().getLaunchIntentForPackage("com.aeustech.colormatch");
                    startActivity(LaunchIntent);
                }

                String[] data = messageEvent.getPath().split("/");
                SharedPreferences sharedPref = this.getSharedPreferences("com.aeustech.colormatch", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();


                if (data[0].equalsIgnoreCase("Classic"))
                {
                    int score = Integer.parseInt(data[1]);
                    editor.putInt("Classic", score);
                    editor.commit();
                }

                if (data[0].equalsIgnoreCase("Timed"))
                {
                    int score = Integer.parseInt(data[1]);
                    editor.putInt("Timed", score);
                    editor.commit();
                }

                if (data[0].equalsIgnoreCase("Memory"))
                {
                    int score = Integer.parseInt(data[1]);
                    editor.putInt("Memory", score);
                    editor.commit();
                }

                if (data[0].equalsIgnoreCase("Advanced"))
                {
                    int score = Integer.parseInt(data[1]);
                    editor.putInt("Advanced", score);
                    editor.commit();
                }


            Intent broadcastIntent = new Intent("com.aeustech.colormatch.broadcast");
            broadcastIntent.putExtra("data", messageEvent.getPath());
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        }



    }

    private void tellWatchConnectedState(final String state){

        new AsyncTask<Void, Void, List<Node>>(){

            @Override
            protected List<Node> doInBackground(Void... params) {
                return getNodes();
            }

            @Override
            protected void onPostExecute(List<Node> nodeList) {
                for(Node node : nodeList) {
                    Log.v(TAG, "Phone telling " + node.getId() + " i am " + state);

                    PendingResult<MessageApi.SendMessageResult> result = Wearable.MessageApi.sendMessage(
                            mGoogleApiClient,
                            node.getId(),
                            "sendScores",
                            null
                    );

                    result.setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                            Log.v(TAG, "Phone: " + sendMessageResult.getStatus().getStatusMessage());
                        }
                    });
                }
            }
        }.execute();

    }

    private List<Node> getNodes() {
        List<Node> nodes = new ArrayList<Node>();
        NodeApi.GetConnectedNodesResult rawNodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
        for (Node node : rawNodes.getNodes()) {
            nodes.add(node);
        }
        return nodes;
    }

    public void log(String msg) {
        Log.d("COLORMATCH_S", msg);
    }
}