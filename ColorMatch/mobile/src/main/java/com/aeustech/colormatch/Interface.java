package com.aeustech.colormatch;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;

import com.android.vending.billing.util.IabHelper;
import com.android.vending.billing.util.IabResult;
import com.android.vending.billing.util.Inventory;
import com.android.vending.billing.util.Purchase;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;

import java.util.List;

public class Interface {

    public Activity context;
    public IabHelper iabHelper;
    public GameHelper gameHelper;

    String purchasedItem;
    boolean iabSetupDone = false;
    IabHelper mHelper;
    static int logCounter = 0;

    public Interface() {
    }

    public static void log(String message) {
        Log.d("ColorMatchActivity_H", (logCounter++) + " - " + message);
    }

    public void gplayRestorePurchase() {
        iabHelper.queryInventoryAsync(mGotInventoryListener);
    }

    public IabHelper gplayInitIAB() {

        if (!gplayServicesAvailable()) return null;

        String a = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArM7pnfLuJkcKMLDYWP2lZojCu64IkQfbdGkQsCT9y3uLfQqi6xtkaTHndh6+Ft9P+788d6c+5USX";
        String b = "o0xG56GLdy865SA/Dg28PIvEfD40SYVbgJxqsOZkLCT9hrTa9KTf9zRQh0v6zAUPnvcT+hb0bZbmYt403CAoFoBMXy0fsJHe3gUQWxaoK6c9WeHwJXcIom8g";
        String c = "5w6mcbrI1QMI6oVfoLqhGbtrMC+zmP1W3DJId6ZhhH8IRXmPrd2/B3pEUd9MUEgPZHdf3T5ZXTSIevKmjij2y6GUNm/nqkj1hwnaCcptHIR1Jac0hOhDfIS6RRcf1/xKlIzvkGn/aAzNyOQ0kQIDAQAB";

        String base64EncodedPublicKey = a + b + c;

        // compute your public key and store it in base64EncodedPublicKey
        mHelper = new IabHelper(context, base64EncodedPublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    log("IAB setup error: " + result);
                    iabSetupDone = false;
                    return;
                } else {
                    log("IAB setup success: " + result);
                    iabSetupDone = true;
                    ((ColorMatchActivity)context).checkingPrevPurchases = true;
                    iabHelper.queryInventoryAsync(mGotInventoryListener);
                }
                if (mHelper == null) return;
            }
        });
        iabHelper = mHelper;

        return mHelper;
    }

    public void gplayDestroyIAB() {
        if (iabHelper != null) iabHelper.dispose();
        iabHelper = null;
    }

    public void gplayIABBuyItem(String itemID) {

        if(! gplayServicesAvailable() || ! iabSetupDone) {
            // if bought from watch, tell watch user cancelled on phone
            ((ColorMatchActivity) context).cancelBuy();
            showAlert("Please try buying from your watch again.");

            return;
        }

        purchasedItem = "com.aeustech.colormatch." + itemID;
        log("about to purchase item = " + purchasedItem);
        itemID = purchasedItem;

        // TO DO: Delete when actual purchase should be done
        // Testing only
        //itemID = "android.test.purchased";

        try {
            iabHelper.launchPurchaseFlow(context, itemID, 1000,
                    mPurchaseFinishedListener, "bought_" + itemID);

        } catch (Exception e) {
            ((ColorMatchActivity)context).cancelBuy();
            log("GAME SERVICES ERROR: " + e.getMessage());
            showAlert("Please log on to Google Play services to use this feature.");

        }
    }

    public boolean gplayServicesAvailable() {
        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(context);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Activity Recognition",
                    "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason
        } else {
            showAlert("Google Play services is not available on this device.");
            return false;
        }
    }

    // Google Play services
    public GameHelper gplayInitGameHelper() {
        if(! gplayServicesAvailable()) {
            log("google play services not available");

            return null;
        }

        logCounter = 1;
        gameHelper = new GameHelper(context, GameHelper.CLIENT_ALL);

        GameHelper.GameHelperListener listener = new GameHelper.GameHelperListener() {
            @Override
            public void onSignInSucceeded() {
                // handle sign-in success
                log("sign in success");
            }
            @Override
            public void onSignInFailed() {
                // handle sign-in failure (e.g. show Sign In button)
                log("sign in failed");
                gameHelper.setMaxAutoSignInAttempts(0);
            }

        };
        gameHelper.setup(listener);

        log("init game helper");
        return gameHelper;
    }

    public void gplayLeaderboardShow(String lbID) {
        if(! gplayServicesAvailable()) return;

        try {
            context.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(StaticObjects.gameHelper.getApiClient(), lbID), 0);
        } catch (Exception e) {
            log("GAME SERVICES ERROR: " + e.getMessage());
            showAlert("Please log on to Google Play game services to use this feature.");
        }
    }


    public void gplayLeaderboardSubmitScore(String lbID, int score) {
        if(! gplayServicesAvailable()) return;

        try {
            Games.Leaderboards.submitScore(StaticObjects.gameHelper.getApiClient(), lbID, score);
        } catch (Exception e) {
            log("GAME SERVICES ERROR: " + e.getMessage());
        }
    }

    public void showAlert(String message) {
        log("show alert");

        final String msg = message;
        new AlertDialog.Builder(context)
                .setTitle("Color Match")
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .show();
    }

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            log("Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
//            if (StaticObjects.iabHelper == null) return;

            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item to the game
                String sku = purchase.getSku();
                log("Consumption successful. Provisioning " + sku);
            }
            else {
                log("Error while consuming: " + result);
            }

            log("End consumption flow.");
        }
    };

    void unlockItemPurchased(Purchase purchase) {
        String boughtItem = purchase.getSku().replace(".", "/");
        String[] data = boughtItem.split("/");

        // TO DO: Remove comment braces
        // Code for actual purchase
        /*
        if(data.length >= 4) {
            if(data[3].equalsIgnoreCase("allmodes")) {

                log("unlock product = " + data[3]);
                ((ColorMatchActivity) context).finishedBuy();
            }
        }*/

        // TO DO: Remove the following lines
        // Code for testing
        ((ColorMatchActivity) context).finishedBuy();
        log("test purchase successful");


    }

    // Google Play Billing service
    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            log("Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (iabHelper == null) return;

            if (result.isFailure()) {
                log("Error purchasing: " + result.getResponse());

                // already owned
                if(result.getResponse() == 7) {
                    log("item is already owned, unlocking");
                    showAlert("You already own this item.\nPlease press the Restore button to restore your previous purchases.");
                } else {
                    // if bought from watch, tell watch user cancelled on phone
                    ((ColorMatchActivity) context).cancelBuy();
                }
                return;
            }

            log("Purchase successful.");

            log("purchased " + purchase.getSku());

            unlockItemPurchased(purchase);
        }
    };

    // Listener that's called when we finish querying the items we own
    final IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            log("Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                log("Failed to query inventory: " + result);
                return;
            }

            log("Query inventory was successful: " + inventory.getAllPurchases().toString());

            List<Purchase> purchases = inventory.getAllPurchases();

            // if we have owned but not consumed item, use it
//    	            if (inventory.hasPurchase("android.test.purchased")) {
            if (purchases.size() > 0) {
//    	                mHelper.consumeAsync(inventory.getPurchase("android.test.purchased"), mConsumeFinishedListener);
//                    purchasedItem = purchases.get(0).getSku();
//    	            log("prev purchase sku = " + purchasedItem);
                // unlock items for non-consumables

                // comment out for non-consumables, do not consume
//                    mHelper.consumeAsync(inventory.getPurchase(purchasedItem), mConsumeFinishedListener);

                // consume all, for testing only
                for(Purchase p : purchases) {
                    log("prev sku = " + p.getSku());
                    ((ColorMatchActivity)context).prevPurchasesFound = true;
                    unlockItemPurchased(p);

                    // TESTING: enable to buy non-consumable again
                    if(((ColorMatchActivity)context).consumeNC) {
                        mHelper.consumeAsync(inventory.getPurchase(p.getSku()), mConsumeFinishedListener);
                    }
                }
            } else {
                if(! ((ColorMatchActivity)context).checkingPrevPurchases) {
                    showAlert("No previous purchases found.");
                }
            }
        }
    };
}

