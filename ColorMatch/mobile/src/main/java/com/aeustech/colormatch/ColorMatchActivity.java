package com.aeustech.colormatch;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.Image;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.example.games.basegameutils.GameHelper;
import com.flurry.android.FlurryAgent;

import java.util.ArrayList;
import java.util.List;


public class ColorMatchActivity extends Activity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    FrameLayout windowSettings;
    ImageView img;
    TextView textView;
    ImageView lockImg;
    Image img2;

    Context context;
    LayoutInflater inflater;
    ColorMatchPagerAdapter pagerAdapter;
    ViewPager pager;
    int currentPage = 0;
    ViewGroup viewGroupLayout;
    Button visitButton;

    // For google services
    private GoogleApiClient mGoogleApiClient;// mGoogleApiClientGames;
    private boolean mResolvingConnectionFailure = false;
    private boolean mAutoStartSignInflow = true;
    private boolean mSignInClicked = false;
    private static int RC_SIGN_IN = 9001;
    private GameHelper gameHelper;

    BroadcastReceiver resultReceiver;

    // Variables used for buying products
    Interface apiBindings;
    String product = "0";
    SharedPreferences prefs;

    public boolean boughtFromWatch = false;
    public boolean checkingPrevPurchases = false;
    public boolean consumeNC = false;
    public boolean prevPurchasesFound = false;
    private static String TAG = "COLORMATCH_H";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_match);


        context = this;

        // Instantiate a ViewPager and a PagerAdapter
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewGroupLayout = (ViewGroup) findViewById(android.R.id.content);
        visitButton = (Button) findViewById(R.id.btn_otherapps);

        pagerAdapter = new ColorMatchPagerAdapter();
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);
        pagerAdapter.preloadPacks();

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

                if(currentPage == 3) {
                    visitButton.setText("How To Use");
                } else {
                    visitButton.setText("Other Apps");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Wearable.API)
                .build();

        mGoogleApiClient.connect();

        /*mGoogleApiClientGames = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN)
                .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                .build();*/


        // For buying product
        resultReceiver = createBroadcastReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(
                resultReceiver, new IntentFilter("com.aeustech.colormatch.broadcast"));

        apiBindings = new Interface();
        apiBindings.context = this;
        StaticObjects.iabHelper = apiBindings.gplayInitIAB();
        StaticObjects.gameHelper = apiBindings.gplayInitGameHelper();

        SharedPreferences sharedPref = context.getSharedPreferences("com.aeustech.colormatch", Context.MODE_PRIVATE);
        pagerAdapter.txtClassic.setText(sharedPref.getInt("Classic",0 ) +"");
        pagerAdapter.txtTimed.setText(sharedPref.getInt("Timed", 0) + "");
        pagerAdapter.txtMemory.setText(sharedPref.getInt("Memory", 0) + "");
        pagerAdapter.txtAdvanced.setText(sharedPref.getInt("Advanced", 0) + "");

        // FOR TESTING ONLY!
        //tellWatchConnectedState("complete/allmodes");
        //
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        log("onConnected: " + connectionHint);
        tellWatchConnectedState("askForScores");
    }

    @Override
    public void onConnectionSuspended(int cause) {
        log("onConnectionSuspended: " + cause);
        // Attempt to reconnect
        mGoogleApiClient.connect();
        //\mGoogleApiClientGames.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        log("onConnectionFailed: " + connectionResult);

        if (mResolvingConnectionFailure) {
            // already resolving
            return;
        }

        // if the sign-in button was clicked or if auto sign-in is enabled,
        // launch the sign-in flow
        /*if (mSignInClicked || mAutoStartSignInflow) {
            mAutoStartSignInflow = false;
            mSignInClicked = false;
            mResolvingConnectionFailure = true;

            // Attempt to resolve the connection failure using BaseGameUtils.
            // The R.string.signin_other_error value should reference a generic
            // error string in your strings.xml file, such as "There was
            // an issue with sign-in, please try again later."
            if (!BaseGameUtils.resolveConnectionFailure(this,
                    mGoogleApiClientGames, connectionResult,
                    RC_SIGN_IN, R.string.signin_other_error)) {
                mResolvingConnectionFailure = false;
            }
        }*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        log("app on start");
        flurryStart();

        product = null;
        if(product != null && product.equalsIgnoreCase("allmodes")) {
            boughtFromWatch = true;
            showBuyPopup();
        }

        try {
            if(StaticObjects.gameHelper  != null)
                StaticObjects.gameHelper.onStart(this);
        } catch(Exception e) {
            showAlert("Google Play services is not available on this device.");
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        flurryStop();

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

        try {
            if(StaticObjects.gameHelper  != null)
                StaticObjects.gameHelper.onStop();
        } catch(Exception e) {
            showAlert("Google Play services is not available on this device.");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (resultReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(resultReceiver);
        }
        apiBindings.gplayDestroyIAB();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(apiBindings.iabHelper == null) {
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }

        if (!apiBindings.iabHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);

            if (apiBindings.gameHelper == null) return;

            apiBindings.gameHelper.onActivityResult(requestCode, resultCode, data);
        }
/*
        if (requestCode == RC_SIGN_IN) {
            mSignInClicked = false;
            mResolvingConnectionFailure = false;

            if (resultCode == RESULT_OK) {
                mGoogleApiClient.connect();
                //mGoogleApiClientGames.connect();
            } else {
                // Bring up an error dialog to alert the user that sign-in
                // failed. The R.string.signin_failure should reference an error
                // string in your strings.xml file that tells the user they
                // could not be signed in, such as "Unable to sign in."
                BaseGameUtils.showActivityResultError(this,
                        requestCode, resultCode, R.string.signin_failure);
            }
        }*/
    }

    private void tellWatchConnectedState(final String state){

        new AsyncTask<Void, Void, List<Node>>(){

            @Override
            protected List<Node> doInBackground(Void... params) {
                return getNodes();
            }

            @Override
            protected void onPostExecute(List<Node> nodeList) {
                for(Node node : nodeList) {
                    Log.v(TAG, "Phone telling " + node.getId() + " i am " + state);

                    PendingResult<MessageApi.SendMessageResult> result = Wearable.MessageApi.sendMessage(
                            mGoogleApiClient,
                            node.getId(),
                            state,
                            null
                    );

                    result.setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                            Log.v(TAG, "Phone: " + sendMessageResult.getStatus().getStatusMessage());
                        }
                    });
                }
            }
        }.execute();

    }

    private List<Node> getNodes() {
        List<Node> nodes = new ArrayList<Node>();
        NodeApi.GetConnectedNodesResult rawNodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
        for (Node node : rawNodes.getNodes()) {
            nodes.add(node);
        }
        return nodes;
    }

    private BroadcastReceiver createBroadcastReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                log("broadcast received = " + intent.getStringExtra("data"));

                String[] data = intent.getStringExtra("data").split("/");
                SharedPreferences sharedPref = context.getSharedPreferences("com.aeustech.colormatch", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();


                if (data[0].equalsIgnoreCase("Classic")) {
                    int score = Integer.parseInt(data[1]);
                    pagerAdapter.txtClassic.setText(score + "");
                    editor.putInt("Classic", score);
                    editor.commit();

                    apiBindings.gplayLeaderboardSubmitScore(getString(R.string.classic_leaderboard), score);



                    /*if (mGoogleApiClientGames != null && mGoogleApiClientGames.isConnected()) {
                        Games.Leaderboards.submitScore(mGoogleApiClientGames, String.valueOf(R.string.classic_leaderboard), score);
                    }*/
                }

                if (data[0].equalsIgnoreCase("Timed")) {
                    int score = Integer.parseInt(data[1]);
                    pagerAdapter.txtTimed.setText(score + "");
                    editor.putInt("Timed", score);
                    editor.commit();
                    apiBindings.gplayLeaderboardSubmitScore(getString(R.string.time_leaderboard), score);


                    /*if (mGoogleApiClientGames != null && mGoogleApiClientGames.isConnected()) {
                        Games.Leaderboards.submitScore(mGoogleApiClientGames, String.valueOf(R.string.time_leaderboard), score);
                    }*/
                }

                if (data[0].equalsIgnoreCase("Memory")) {
                    int score = Integer.parseInt(data[1]);
                    pagerAdapter.txtMemory.setText(score + "");
                    editor.putInt("Memory", score);
                    editor.commit();

                    apiBindings.gplayLeaderboardSubmitScore(getString(R.string.memory_leaderboard), score);


                    /*if (mGoogleApiClientGames != null && mGoogleApiClientGames.isConnected()) {
                        Games.Leaderboards.submitScore(mGoogleApiClientGames, String.valueOf(R.string.memory_leaderboard), score);
                    }*/
                }

                if (data[0].equalsIgnoreCase("Advanced")) {
                    int score = Integer.parseInt(data[1]);
                    pagerAdapter.txtAdvanced.setText(score + "");
                    editor.putInt("Advanced", score);
                    editor.commit();

                    apiBindings.gplayLeaderboardSubmitScore(getString(R.string.advance_leaderboard), score);


                    /*if (mGoogleApiClientGames != null && mGoogleApiClientGames.isConnected()) {
                        Games.Leaderboards.submitScore(mGoogleApiClientGames, String.valueOf(R.string.advance_leaderboard), score);
                    }*/
                }

                if(data[0].equalsIgnoreCase("AllScores"))
                {
                    String[] scores = data[1].split(",");
                    pagerAdapter.txtClassic.setText(scores[0]);
                    editor.putInt("Classic", Integer.parseInt(scores[0]));
                    editor.commit();

                    if(scores.length > 1) {
                        pagerAdapter.txtTimed.setText(scores[1]);
                        editor.putInt("Timed", Integer.parseInt(scores[1]));
                        editor.commit();
                        pagerAdapter.txtMemory.setText(scores[2]);
                        editor.putInt("Timed", Integer.parseInt(scores[2]));
                        editor.commit();
                        pagerAdapter.txtAdvanced.setText(scores[3]);
                        editor.putInt("Timed", Integer.parseInt(scores[3]));
                        editor.commit();
                    }

                }

                if(data[0].equalsIgnoreCase("goToBuyScreen")) {
                    pager.setCurrentItem(1);

                    ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 75);
                    toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 1000);

                }

                if(data[0].equalsIgnoreCase("showScores")) {
                    pager.setCurrentItem(2);

                    ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 75);
                    toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 1000);

                }
            }
        };
    }

    public void showBuyPopup() {
        if(pagerAdapter.views.size() > 0) {
            pager.setCurrentItem(2);
            boughtFromWatch = true;

            if(prevPurchasesFound) {
                showAlert("You already own this item.\nPlease press the Restore button to unlock all modes.");
            }

            flurryLogEvent("SHOW_BUY_CONFIRM_POPUP");
        }
    }

    public void finishedBuy() {
        log("complete buy");

        tellWatchConnectedState("complete/allmodes");

        pagerAdapter.buyButton.setVisibility(View.GONE);
        //txtUnlockMessage.setText("Thank you for your support!");
        //txtUnlockMessage.setVisibility(View.VISIBLE);

        if(!checkingPrevPurchases) {
            showAlert("All modes are unlocked.\nPlease check your watch.");
        }

    }

    public void cancelBuy() {
        log("cancel buy");
        tellWatchConnectedState("cancel/allmodes/");
    }

    public void buttonClicked(View v) {
        Intent appStoreIntent;

        switch (v.getId()) {
            case R.id.btn_wrc:
//                consumeNC = true;

                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.wearrotarycalculator"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_WTC_PLAY_STORE");
                break;

            case R.id.btn_wtc:
//              consumeNC = true;

                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.weartipcalculator"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_WTC_PLAY_STORE");
                break;

            case R.id.btn_wit:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.wearintervaltimer"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_WIT_PLAY_STORE");
                break;

            case R.id.btn_omgwe:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.ohmygravitywear"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_OMGW_PLAY_STORE");
                break;

            case R.id.btn_htc:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.hitthecan"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_WTC_PLAY_STORE");
                break;

            case R.id.btn_hm:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.hydrateme"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_WTC_PLAY_STORE");
                break;

            case R.id.btn_mb:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.matchblox"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_MB_PLAY_STORE");
                break;

            case R.id.btn_omg:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.ohmygravity"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_OMGH_PLAY_STORE");
                break;

            case R.id.btn_shapopo:
                appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.shapopo"));
                appStoreIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appStoreIntent);

                flurryLogEvent("LAUNCH_SPP_PLAY_STORE");
                break;

            case R.id.btn_classic:

                apiBindings.gplayLeaderboardShow(getString(R.string.classic_leaderboard));

                /*if (mGoogleApiClientGames != null && mGoogleApiClientGames.isConnected()) {

                        startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mGoogleApiClientGames,
                                String.valueOf(R.string.classic_leaderboard)), 0);


                }*/
                break;

            case R.id.btn_timed:

                apiBindings.gplayLeaderboardShow(getString(R.string.time_leaderboard));


                /*if (mGoogleApiClientGames != null && mGoogleApiClientGames.isConnected()) {
                    startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mGoogleApiClientGames,
                            String.valueOf(R.string.time_leaderboard)), 1);
                }*/
                break;

            case R.id.btn_memory:

                apiBindings.gplayLeaderboardShow(getString(R.string.memory_leaderboard));


                /*if (mGoogleApiClientGames != null && mGoogleApiClientGames.isConnected()) {
                    startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mGoogleApiClientGames,
                            String.valueOf(R.string.memory_leaderboard)), 2);
                }*/
                break;

            case R.id.btn_advanced:

                apiBindings.gplayLeaderboardShow(getString(R.string.advance_leaderboard));


                /*if (mGoogleApiClientGames != null && mGoogleApiClientGames.isConnected()) {
                    startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mGoogleApiClientGames,
                            String.valueOf(R.string.advance_leaderboard)), 3);
                }*/
                break;

            case R.id.btn_buy:
                checkingPrevPurchases = false;
                log("buy 3 game modes");

                boughtFromWatch = true;
                checkingPrevPurchases = false;

                apiBindings.gplayIABBuyItem("allmodes");
                break;

            case R.id.btn_otherapps:
                if(pager.getCurrentItem() != 3)
                {
                    pager.setCurrentItem(3);
                    visitButton.setText("How To Play");
                }

                else
                {
                    pager.setCurrentItem(0);
                    visitButton.setText("Other Apps");
                }
                break;


            case R.id.btn_rate:
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.aeustech.colormatch"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;

            case R.id.btn_restore:

                if(prevPurchasesFound)
                {
                    tellWatchConnectedState("complete/allmodes");
                    showAlert("Full Game Unlocked. Please check your watch.");
                }
                else
                {
                    showAlert("You have not yet bought the full game. Press Buy to unlock all modes.");
                }



                break;

        }

    }

    public void showAlert(String message) {
        log("show alert");
        final String msg = message;
        new AlertDialog.Builder(context)
                .setTitle("Color Match")
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                }).show();
    }

    // Helper method for logging
    public void log(String msg) {
        Log.d(TAG, msg);
    }


    private class ColorMatchPagerAdapter extends PagerAdapter {
        ArrayList<View> views = new ArrayList<View>();

        TextView txtClassic;
        TextView txtTimed;
        TextView txtMemory;
        TextView txtAdvanced;
        ImageButton buyButton;

        public void preloadPacks() {
            views.clear();

            views.add((View) inflater.inflate(R.layout.page1_tutorial_layout, null));
            views.add((View) inflater.inflate(R.layout.page2_buy_layout, null));
            views.add((View) inflater.inflate(R.layout.page3_leaderboards_layout, null));
            views.add((View) inflater.inflate(R.layout.page4_otherapps_layout, null));

            Log.d("TAG", "PRELOADED PACKS");
            buyButton = (ImageButton) views.get(1).findViewById(R.id.btn_buy);
            //restoreButton = (Button) views.get(2).findViewById(R.id.btn_restore);
            //txtUnlockMessage = (TextView) views.get(2).findViewById(R.id.txt_message);

            txtClassic =(TextView) views.get(2).findViewById(R.id.txtClassic);
            txtTimed = (TextView)views.get(2).findViewById(R.id.txtTimed);
            txtMemory = (TextView)views.get(2).findViewById(R.id.txtMemory);
            txtAdvanced = (TextView)views.get(2).findViewById(R.id.txtAdvanced);


            //TO DO: get score from wear
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view==object);
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            View v = views.get(position);
            collection.addView(v, 0);

            return v;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }



    }

    public boolean gplayServicesAvailable() {
        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(context);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Activity Recognition",
                    "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason
        } else {
            showAlert("Google Play services is not available on this device.");
            return false;
        }
    }

    public void flurryStart() {
        FlurryAgent.onStartSession(this, "DCNQ8ZHDWRCQC94QQ478");
        flurryLogEvent("APP_HANDHELD_STARTED");
    }

    public void flurryStop() {
        flurryLogEvent("APP_HANDHELD_STOPPED");
        FlurryAgent.onEndSession(this);
    }

    public void flurryLogEvent(String event) {
        //log("Flurry event = " + event);
        //FlurryAgent.logEvent(event);
    }

}
