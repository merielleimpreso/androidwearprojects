package com.aeustech.colormatch;

import android.app.Activity;

import com.unity3d.player.UnityPlayer;

public class Interface {

    public Activity context;
    String purchasedItem;

    static int logCounter = 0;

    private static String TAG = "COLORMATCH_W";


    public static void log(String message) {
//		Log.d(TAG, (logCounter++) + " - " + message);
    }

    public void downloadFullVersion() {
        StaticObjects.activity.sendMessage("");
        log("in android, download full version");
    }

    public void quitGame() {
        log("in android, quit game");
        System.exit(0);
    }

    public void GetScores(int index)
    {
        UnityPlayer.UnitySendMessage("Android", "GiveClassicScore","");
        UnityPlayer.UnitySendMessage("Android", "GiveTimedScore","");
        UnityPlayer.UnitySendMessage("Android", "GiveMemoryScore","");
        UnityPlayer.UnitySendMessage("Android", "GiveAdvancedScore","");

    }

}