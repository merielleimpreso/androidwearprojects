package com.aeustech.colormatch;

import android.app.ActivityManager;
import android.app.NativeActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.wearable.view.WatchViewStub;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.unity3d.player.UnityPlayer;

public class UnityPlayerNativeActivity extends NativeActivity implements MessageApi.MessageListener, GoogleApiClient.ConnectionCallbacks
{
    Interface unityBinding;

    protected UnityPlayer mUnityPlayer;		// don't change the name of this variable; referenced from native code

    GoogleApiClient mGoogleApiClient = null;

    Node node; // the connected device to send the message to
    MessageApi.MessageListener listener = null;

    public static final String OMG_PATH = "/download/app";
    private Node peerNode;
    Interface unity = null;

    private static String TAG = "COLORMATCH_W";

    public void log(String message) {
        Log.d(TAG, message);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        return;
    }

    private void servicesRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            log("service=" + service.service.getClassName());
//            if(service.service.getClassName().equalsIgnoreCase("com.npi.wearminilauncher.service.DrawerService")) {
            if(service.service.getClassName().equalsIgnoreCase("aeustech")) {
                manager.killBackgroundProcesses(service.service.getPackageName());
                log("stopping service=" + service.service.getPackageName());
            }
        }
    }

    public void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
//		log("show toast");
    }

    public int screenHeight() {
                Display display = getWindowManager().getDefaultDisplay();
                DisplayMetrics metrics = new DisplayMetrics();
                display.getMetrics(metrics);
                log("screen height = " + metrics.heightPixels);
                return metrics.heightPixels;
            }
    // UnityPlayer.init() should be called before attaching the view to a layout - it will load the native code.
    // UnityPlayer.quit() should be the last thing called - it will unload the native code.
    protected void onCreate (Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);
//        servicesRunning();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        getWindow().takeSurface(null);
        setTheme(android.R.style.Theme_NoTitleBar_Fullscreen);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);

        mUnityPlayer = new UnityPlayer(this);
        if (mUnityPlayer.getSettings ().getBoolean ("hide_status_bar", true))
            getWindow ().setFlags (WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        int glesMode = mUnityPlayer.getSettings().getInt("gles_mode", 1);
        boolean trueColor8888 = false;
        mUnityPlayer.init(glesMode, trueColor8888);

        View playerView = mUnityPlayer.getView();
        setContentView(playerView);
        playerView.requestFocus();

        //---
        WatchViewStub stub = new WatchViewStub(this);
        stub.setRoundLayout(R.layout.round_activity_color_match);
        stub.setRectLayout(R.layout.rect_activity_color_match);
        addContentView(stub, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));


        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                log("onLayoutInflated()");
                FrameLayout frmMain;

                frmMain = (FrameLayout) findViewById(R.id.frmMainRect);

                if (frmMain == null) {
                    log("round screen detected");
//                    toast("round screen");;
                    frmMain = (FrameLayout) findViewById(R.id.frmMainRound);
//                   // LG G Watch R will have the same screen orientation as square faces
                        if(screenHeight() == 320) {
                                UnityPlayer.UnitySendMessage("Android","CallFromAndroid","runningOnSquareScreen");
                                      }
                                  // Moto 360 screen orientation
                       else {
                                UnityPlayer.UnitySendMessage("Android", "CallFromAndroid", "runningOnRoundScreen");
                                        }
            } else {
                    log("rectangular screen detected");
//                    toast("rectangular screen");
//                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

                      UnityPlayer.UnitySendMessage("Android","CallFromAndroid","runningOnSquareScreen");

//                    frmMain.addView(mDismissOverlayView, new RelativeLayout.LayoutParams(
//                            RelativeLayout.LayoutParams.MATCH_PARENT,
//                            RelativeLayout.LayoutParams.MATCH_PARENT));

                }

//                layoutInflated = true;

            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        Log.d(TAG, "onConnectionFailed: " + result);
                    }
                })
                .addApi(Wearable.API)
                .build();

        mGoogleApiClient.connect();

        StaticObjects.activity = this;

//		Looper.prepare();
    }

    protected void onDestroy ()
    {
        mUnityPlayer.quit();
        Wearable.MessageApi.removeListener(mGoogleApiClient, listener);
        Log.d(TAG, "listener removed");
        super.onDestroy();
    }

    // onPause()/onResume() must be sent to UnityPlayer to enable pause and resource recreation on resume.
    protected void onPause()
    {
        super.onPause();
        mUnityPlayer.pause();
    }
    protected void onResume()
    {
        super.onResume();
//        servicesRunning();
        mUnityPlayer.resume();
    }
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mUnityPlayer.configurationChanged(newConfig);
    }
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        mUnityPlayer.windowFocusChanged(hasFocus);
    }
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
            return mUnityPlayer.onKeyMultiple(event.getKeyCode(), event.getRepeatCount(), event);
        return super.dispatchKeyEvent(event);
    }

    @Override
    protected void onStart() {
        super.onStart();
//        servicesRunning();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onMessageReceived(final MessageEvent messageEvent) {
        Log.v(TAG, "Message received on wear: " + messageEvent.getPath()
        );



        //sendStartMessage();

        if(messageEvent.getPath().equals("askForScores"))
        {
            UnityPlayer.UnitySendMessage("Android", "UpdateAllScores", "");
            //sendStartMessage("Send Scores");
        }

        // For buying
        log("handheld message: " + messageEvent.getPath());

       runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String handHeldMessage = messageEvent.getPath();
                String[] data = handHeldMessage.split("/");

                if (data.length > 1) {

                    if (data[0].equalsIgnoreCase("complete")) {
                        log("finished buying");

                        // TO DO: Connect to Unity to unlock the other game modes
                        UnityPlayer.UnitySendMessage("Android", "UnlockAllModes", "");


                    } else if (data[0].equalsIgnoreCase("cancel")) {
                        log("cancelled buying");
                        UnityPlayer.UnitySendMessage("Android", "LockAllModes", "");
                    }
                }
            }
        });
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.v(TAG, "connected to Google Play Services on Wear!");
        Wearable.MessageApi.addListener(mGoogleApiClient, this).setResultCallback(resultCallback);
    }

    private void sendStartMessage(String msg){

        NodeApi.GetConnectedNodesResult rawNodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();

        for (final Node node : rawNodes.getNodes()) {
            Log.v(TAG, "Node: " + node.getId());
            PendingResult<MessageApi.SendMessageResult> result = Wearable.MessageApi.sendMessage(
                    mGoogleApiClient,
                    node.getId(),
                    msg,
                    null
            );
            Log.d("WEAR", "Sent "+ msg);
            result.setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                @Override
                public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                    //  The message is done sending.
                    //  This doesn't mean it worked, though.
                    Log.v(TAG, "Our callback is done.");
                    peerNode = node;    //  Save the node that worked so we don't have to loop again.
                }
            });
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    private ResultCallback<Status> resultCallback =  new ResultCallback<Status>() {
        @Override
        public void onResult(Status status) {
            Log.v(TAG, "Status: " + status.getStatus().isSuccess());
            new AsyncTask<Void, Void, Void>(){
                @Override
                protected Void doInBackground(Void... params) {
//                    sendStartMessage();
                    return null;
                }
            }.execute();
        }
    };

    public void sendMessage(final String msg) {
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                sendStartMessage(msg);
                return null;
            }
        }.execute();
    }

    // Function in sending scores to mobile
    // Called by Android.cs in Unity project
    public void updateScores(String gamePath) {
        sendMessage(gamePath);
        log("Android W received: "+gamePath);
    }

    //Called by Unity to force Companion App to go to buy screen
    public void goToBuyScreen(String gamePath)
    {
        sendMessage("goToBuyScreen");
    }

    public void vibrate(String index)
    {
        final Vibrator vibe = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        long pattern[] = {0, 20, 10, 20};



        if (vibe.hasVibrator()) {
            vibe.vibrate(pattern, -1);
        }
    }

    public void showScores(String index)
    {
        sendMessage("showScores");
    }



}

